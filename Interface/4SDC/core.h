/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Interface file for the 4SDC
 *
 * The class contained in this file has the responsibility of controlling the
 * 4SDC stack's main thread, initialising the stack, and running its message
 * loop.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef CORE_H
#define CORE_H

#include "FSYS/basemsg.h"
#include <string>

namespace fsdc
{

/**
 * @brief 4SDC Core class
 *
 * The core class of 4SDC, contains the interfaces that should eventually
 * exsist in the presentation layer
 */
class Core
{

public:

  ///@todo Fixme: Document me
  class RegisterDataType : public FSYS::BaseMsg {
  public:     
      uint16_t dataType;
  };

  ///@todo Fixme: Document me
  class UnRegisterDataType : public FSYS::BaseMsg {
  public:
      uint16_t dataType;
  };

  ///@todo Fixme: Document me
  class DataAvailable : public FSYS::BaseMsg {
  public:
      // The device type the data is comming from
      uint16_t dataType;
      // A JSON string with the data
      std::string data;
  };

};

} // End namespace


#endif // CORE_H


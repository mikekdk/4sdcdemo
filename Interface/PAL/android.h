/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Handles to the Java VM running our Android service.
 *
 * This component bridges the C++ and Java worlds of the Android service by
 * providing the handles into the Android/Java world needed by the C++
 * components of the PAL layer.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2015
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef ANDROID_H
#define ANDROID_H

#include <jni.h>

/**
 * @brief The minimal required JNI/JRE version.
 */
#define JNIVER JNI_VERSION_1_6


namespace PAL {

/**
 * @brief Handles to the Java VM running our Android service.
 *
 * This class bridges the C++ and Java worlds of the Android service by
 * providing the handles into the Android/Java world needed by the C++
 * components of the PAL layer.
 */
class Android {
public:
    /**
     * @brief Get a pointer to the Java VM instance used to connect to Android.
     *
     * This method will return a pointer to the Java VM instance running
     * the Android service.
     *
     * @return A pointer to the Java VM.
     */
    static JavaVM *getJavaVM();


    /**
     * @brief Get a global reference to the Java ClassLoader.
     *
     * Acquire a global reference to a Java ClassLoader that will be capable of
     * loading PAL-layer classes (the reference will be valid until the
     * application terminates).
     *
     * @return The java.lang.ClassLoader global reference.
     */
    static jobject getClassLoader();


    /**
     * @brief Get a global reference to the Android Context.
     *
     * Acquire a global reference to the Android Context (the reference will be
     * valid until the application terminates).
     *
     * @see setContext()
     * @return The android.content.Context global reference.
     */
    static jobject getContext();


    /**
     * @brief Provide a reference to the Android Context.
     *
     * Provide a reference to the Android Context object which provides the
     * access rights to the system, such as Bluetooth or USB access. The
     * android.content.Context object reference provided may be of any scope,
     * it will be copied into a global reference, which can be accessed later
     * using getContext().
     *
     * This function must be called exactly once during initialization of the
     * system *before* the PAL layer modules are initialized, as the modules
     * in the PAL layer may depend on this reference being available during
     * initialization.
     *
     * @see getContext()
     * @param contextRef A reference of any scope to an android.content.Context
     *                   object.
     */
    static void setContext(jobject contextRef);
};

} // namespace PAL

#endif // ANDROID_H

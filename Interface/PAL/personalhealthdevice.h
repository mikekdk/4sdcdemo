/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief PAL layer interface for personal health device communication.
 *
 * The communication between transport components in the PAL layer and protocol
 * handlers in the session layer is defined by the classes in this file.
 *
 * The central interface (pure virtual class) is the
 * PersonalHealthDeviceConnector, which, conceptually, may be thought of as an
 * multiconductor electrical connector with signals flowing in both directions;
 * with each method of the interface representing one of those uni-directional
 * signals.
 *
 * Two abstract classes, PersonalHealthDeviceHandler and
 * PersonalHealthDeviceProviderBase implements the two "ends" of this connector
 * -- conceptually like a male and female electrical connector. These abstract
 * classes will manage all the communication across the FSYS message system,
 * leaving subclasses to focus only on handling (and generating) the messages.
 * A component in the session layer must derive from the
 * PersonalHealthDeviceHandler and implement the handling of upward signals,
 * while a component in the PAL layer must derive from one of the subclasses of
 * PersonalHealthDeviceProviderBase and implement the downward signals.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

// TODO: Revisit all the documentation
#ifndef PERSONALHEALTHDEVICE_H
#define PERSONALHEALTHDEVICE_H

#include "FSYS/basemsg.h"
#include "FSYS/handle.h"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

// FIXME: redefine/refactor errortype to the actual error type when chosen...
typedef uint32_t errortype;

namespace PAL {




/* ************************************************************ */
/*   Messages communicated between the PAL and session layers   */
/* ************************************************************ */


class PHDRegisterDatatypeMsg : public FSYS::BaseMsg {
public:
    uint16_t datatype;
};

class PHDUnregisterDatatypeMsg : public FSYS::BaseMsg {
public:
    uint16_t datatype;
};

class PHDConnectIndicationMsg : public FSYS::BaseMsg {
public:
    FSYS::Handle virtualDevice;
    FSYS::Handle physicalDevice;
    uint16_t datatype;
    std::string virtualDisplayname;
    std::string physicalDisplayname;
};

class PHDDisconnectIndicationMsg : public FSYS::BaseMsg {
public:
    FSYS::Handle virtualDevice;
    errortype error;
};

class PHDDataTransferMsg : public FSYS::BaseMsg {
public:
    FSYS::Handle virtualDevice;
    std::vector<uint8_t> apdu;
    bool reliable;
};

class PHDDisconnectMsg : public FSYS::BaseMsg {
public:
    FSYS::Handle virtualDevice;
};

class PHDConnectIndicationBluetoothMsg : public PHDConnectIndicationMsg {
public:
    std::string bluetoothAddress;
    std::string bluetoothName;
};

class PHDConnectIndicationUSBMsg : public PHDConnectIndicationMsg {
public:

};

class PHDConnectIndicationZigBeeMsg : public PHDConnectIndicationMsg {
public:

};




/* ************************************************************ */
/*                       General Interface                      */
/* ************************************************************ */


class PhysicalPHD : public FSYS::Handle {
public:

    /**
     * @brief Default constructor
     *
     * The default constructor will create a PhysicalPHD object with a new
     * unique FSYS::Handle.
     */
    PhysicalPHD() {}

    /**
     * @brief Constructor used to clone the handle
     *
     * This alternative constructor will create a PhysicalPHD object with a
     * cloned FSYS::Handle.
     */
    PhysicalPHD(FSYS::Handle &h) : FSYS::Handle(h) {}

    /**
     * @brief Virtual destructor
     */
    virtual ~PhysicalPHD() {}

    /**
     * @brief A user-friendly name of the device.
     *
     * This getter will return a user-friendly (or at least user-recognizable)
     * name of the physical device, suitable for use in the user interface.
     * Implementing classes should enforce consistent naming across other PAL
     * layer interfaces (e.g. Bluetooth pairing, ZigBee network monitors and the
     * like).
     *
     * Bluetooth devices may (should?) grab the *Service Name* field of the SDP
     * record; while USB devices may (should?) grab the name from the device
     * descriptor's *iProduct* field.
     *
     * @return The device name to be used in user interfaces.
     */
    virtual std::string getDisplayName() const noexcept =0;
};




/**
 * @brief The base interface of a personal health device seen from the
 *        perspective of a transport module.
 *
 * This class is implemented by the transport components in the PAL layer
 * to model a connected device. Each transport type (USB, Bluetooth, ZigBee)
 * implements different subclasses of this class, each adding the
 * characteristics of that particular transport type.
 *
 * Objects deriving from this class are always created by components in the PAL
 * layer and managed by smart references (std::shared_ptr). The objects are
 * then communicated back and forth between the PAL and session layers using
 * the PersonalHealthDeviceConnector interfaces. Notice that the object
 * appearing at the session layer will be a by-value copy (a proxy) of the
 * original object.
 *
 * This class derives from FSYS::Handle so that upper layers and applications
 * may compare these device objects (and proxies) with objects/proxies obtained
 * from other PAL layer interfaces (e.g. Bluetooth pairing, ZigBee network
 * monitors and the like). Hence, two (different) proxy-objects representing
 * the same device will always turn out to be equal when compared (==) as they
 * will be clones of the same FSYS::Handle.
 */
class VirtualPHD : public FSYS::Handle {
public:

    /**
     * @brief Default constructor
     *
     * The default constructor will create a PersonalHealthDevice object with
     * a new unique FSYS::Handle.
     */
    VirtualPHD() {}

    /**
     * @brief Constructor used to clone the handle
     *
     * This alternative constructor will create a PersonalHealthDevice object
     * with a cloned FSYS::Handle.
     */
    VirtualPHD(FSYS::Handle &h) : FSYS::Handle(h) {}

    /**
     * @brief Virtual destructor
     */
    virtual ~VirtualPHD() {}

    // The returned reference will be valid as long as this VirtualPHD object is alive
    virtual PhysicalPHD &getPhysicalDevice() noexcept =0;
    virtual const PhysicalPHD &getPhysicalDevice() const noexcept =0;

    /**
     * @brief The IEEE \c MDC_DEV_SPEC_PROFILE_ datatype implemented by this
     *        device.
     *
     * This getter will return the datatype implemented by this device. The
     * datatype is a 16-bit unsigned integer -- one of the
     * \c MDC_DEV_SPEC_PROFILE_ codes belonging to the \c MDC_PART_INFRA
     * partition.
     *
     * @return The datatype implemented by this device.
     */
    virtual uint16_t getDatatype() const noexcept =0;

    /**
     * @brief A user-friendly name of the device.
     *
     * This getter will return a user-friendly (or at least user-recognizable)
     * name of the virtual device, suitable for use in the user interface.
     * Implementing classes should enforce consistent naming across other PAL
     * layer interfaces (e.g. Bluetooth pairing, ZigBee network monitors and the
     * like).
     *
     * Bluetooth devices may (should?) grab the name from the optional *MDEP
     * Description* field of the SDP record, if available; while USB devices
     * may (should?) grab the name from the interface descriptor's *iInterface*
     * field. If no name is available (including the case when the PAL component
     * is not allowed to read these names), this name should be set to the empty
     * string, and the user interface should be able to generate a default
     * text based on the getDatatype() value.
     *
     * @return The device name to be used in user interfaces, or "".
     */
    virtual std::string getDisplayName() const noexcept =0;
};




/**
 * @brief The PAL interface for communication with personal health devices.
 *
 * The communication between transport components in the PAL layer and
 * session-layer components is defined by this class.
 *
 * All possible signals communicated across the PAL interface are realized as
 * methods of this class. Some are implemented by the session layer and called
 * from the PAL layer, while others travels in the opposite direction. Two
 * abstract subclasses of this class, PersonalHealthDeviceProviderBase and
 * PersonalHealthDeviceHandler manages the magic of wrapping these method
 * calls in messages and sending them back and forth between the modules.
 * The PersonalHealthDeviceHandler will be extended by a class in the
 * session layer, while the PersonalHealthDeviceProviderBase is extended by a
 * class in the PAL layer (a component of the appropriate transport).
 *
 * The PersonalHealthDeviceProviderBase and PersonalHealthDeviceHandler classes
 * will keep track of which PAL- and session-layer components handles which
 * PersonalHealthDevice, and make sure the messages are forwarded appropriately.
 * A few messages does not relate to particular devices, and these messages
 * will instead be broadcast to all possible peers.
 *
 * @see PersonalHealthDevice
 * @see PersonalHealthDeviceProviderBase
 * @see PersonalHealthDeviceHandler
 */
class PersonalHealthDeviceConnector {
protected:

    /**
     * @brief Registers a session-layer component as handler of a datatype.
     *
     * A protocol-to-PAL message. This message is issued by the session-layer
     * component, when it is ready to handle a given datatype. When the
     * component no longer handles the datatype, the unregisterDatatype() is
     * used to stop new connections of that particular datatype.
     *
     * The datatype must be one of the IEEE \c MDC_DEV_SPEC_PROFILE_ codes
     * belonging to the \c MDC_PART_INFRA partition.
     *
     * The PersonalHealthDeviceProviderBase and PersonalHealthDeviceHandler
     * classes will keep track of which session-layer components handles which
     * datatypes, and will broadcast the registerDatatype() and
     * unregisterDatatype() messages to all PAL-layer components when a
     * previously unhandled datatype is registered for the first time and when
     * a datatype is no longer being handled.
     *
     * Except in the unlikely event of memory exhaustion, this method should
     * never throw any exceptions.
     *
     * @see unregisterDatatype()
     * @param datatype The datatype handled by this component.
     * @exception std::bad_alloc in case of memory exhaustion.
     */
    virtual void registerDatatype(uint16_t datatype) =0;

    /**
     * @brief Unregisters a session-layer component as handler of a datatype.
     *
     * A protocol-to-PAL message. This message is issued by the session-layer
     * component, when it no longer wishes to handle the given datatype. For
     * more details, see the documentation of the registerDatatype() method.
     *
     * @see registerDatatype()
     * @param datatype The datatype no longer handled by this component.
     */
    virtual void unregisterDatatype(uint16_t datatype) noexcept =0;

    /**
     * @brief Indicates the connection of a new device.
     *
     * A PAL-to-protocol message. A new device has been connected at the PAL
     * layer and is now available for the session layer.
     *
     * The message magic implemented by PersonalHealthDeviceProviderBase and
     * PersonalHealthDeviceHandler will choose a session-layer component which
     * has previously registered itself for the datatype matching this device
     * using registerDatatype() and establish a one-to-one connection. If no
     * session-layer component is currently registered, the message will be
     * dropped.
     *
     * @param device The device that was just connected.
     */
    virtual void connectIndication(std::shared_ptr<VirtualPHD> device)
                                                                    noexcept =0;

    /**
     * @brief Indicates a device disconnection along with error details.
     *
     * A PAL-to-protocol message. A device (which was previously connected using
     * connectIndication()) has been disconnected. It is no longer available for
     * the session layer. No more messageReceived() signals will ever be
     * emitted for this object, and sendMessage() attempts will be discarded.
     * The session layer should forget all about this object and delete all
     * references.
     *
     * Notice that this message also cuts the link between the PAL and session
     * layer components, so no more messages regarding this device can be
     * communicated until the PAL layer emits a new connectionIndication()
     * (which, by-the-way, may choose to connect the device to a different
     * protocol component, if more are available).
     *
     * The optional error argument carries detailed information of an error in
     * the PAL-layer component that caused this disconnection. The usage of
     * this argument is completely optional for the PAL component -- it is
     * always allowed to use the non-error variant instead. The session-layer
     * component may also choose to ignore the error argument; however, it is
     * recommended that the session layer should forward the error message to
     * the application layer in some way, to allow the error message to be
     * presented to the user, if relevant.
     *
     * @todo Fix doc when the actual errortype has been chosen...
     *
     * @param device The device object, which will no longer be in use.
     * @param error  An error code (optional - defaults to "no error").
     */
    virtual void disconnectIndication(std::shared_ptr<VirtualPHD> device,
                                      errortype error=0) noexcept =0;

    /**
     * @brief Receive a message from this device.
     *
     * A PAL-to-protocol message. A message arrived from a connected device.
     * This device was previously announced by the connectIndication() signal
     * and has not yet been disconnected.
     *
     * @param device            The source of the message.
     * @param apdu              The buffer of bytes received from the device.
     * @param reliableTransport The message arrived on a reliable transport
     *                          channel.
     */
    virtual void apduReceived(std::shared_ptr<VirtualPHD> device,
                              std::shared_ptr<std::vector<uint8_t> > apdu,
                              bool reliableTransport) noexcept =0;

    /**
     * @brief Send a message to this device on the primary virtual channel.
     *
     * A protocol-to-PAL message. If the device is currently connected, this
     * method will (attempt to) send a message to the device on the primary
     * virtual channel (which by definition is a reliable transport service).
     * There will be no indication of whether this attempt was succesful or not.
     * In particular, if this device object is currently not connected, the
     * method invocation will be ignored.
     *
     * @param device  The device, we are sending to.
     * @param apdu    The buffer of bytes to transmit to the device.
     */
    virtual void sendApduPrimary(std::shared_ptr<VirtualPHD> device,
                                 std::shared_ptr<std::vector<uint8_t> > apdu)
                                                                    noexcept =0;

    /**
     * @brief Disconnect the device.
     *
     * A protocol-to-PAL message. If the device is currently connected, it will
     * be disconnected immediately (and a disconnectIndication() will be
     * returned from the PAL shortly), otherwise nothing will happen.
     *
     * @param device The device, we wish to disconnect.
     */
    virtual void disconnect(std::shared_ptr<VirtualPHD> device) noexcept =0;

};




/* ************************************************************ */
/*                      Bluetooth "classic"                     */
/* ************************************************************ */


/**
 * @brief Bluetooth specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying bluetooth Health Device Profile (HDP) device, namely the
 * bluetooth address and name.
 *
 * @todo Describe how the HDP standard maps to this interface
 *
 * @see PersonalHealthDevice
 * @see PHDBluetoothConnector
 */
class PHDBluetoothDevice : public PhysicalPHD {
public:

    /**
     * @brief Default constructor
     *
     * The default constructor will create a PersonalHealthDevice object with
     * a new unique FSYS::Handle.
     */
    PHDBluetoothDevice() {}

    /**
     * @brief Constructor used to clone the handle
     *
     * This alternative constructor will create a PersonalHealthDevice object
     * with a cloned FSYS::Handle.
     */
    PHDBluetoothDevice(FSYS::Handle &h) : PhysicalPHD(h) {}

    /**
     * @brief The bluetooth address of this device.
     *
     * This getter will return the bluetooth address (a.k.a. MAC address)
     * of this device. The format will be 6 double-digit hex values separated
     * by colons, e.g.: "01:23:45:67:89:AB", so the string will always be
     * 17 characters long.
     *
     * @return The bluetooth address of this device.
     */
    virtual std::string getBTAddress() const noexcept =0;

    /**
     * @brief The bluetooth name of this device.
     *
     * This getter will return the bluetooth name of this device. This may or
     * may not be identical to the name returned by getDisplayName().
     *
     * @return The bluetooth name of this device.
     */
    virtual std::string getBTName() const noexcept =0;
};




/**
 * @brief The PersonalHealthDeviceConnector class specialization for
 *        communication with bluetooth devices (PHDBluetoothDevice).
 *
 * This class defines the communication between the bluetooth HDP transport
 * component in the PAL layer and session-layer components.
 *
 * In addition to the common communication primitives, bluetooth offers a mode
 * of transport more suited for streaming data. In this mode, data will be
 * transferred faster and/or with less energy consumption (best-effort), but
 * with reduced reliability.
 *
 * @see PersonalHealthDeviceConnector
 * @see PHDBluetoothDevice
 */
class PHDBluetoothConnector : virtual protected PersonalHealthDeviceConnector {

protected:

    /**
     * @brief Send a message to this device on a streaming virtual channel.
     *
     * A protocol-to-PAL message. If the device is currently connected, this
     * method will (attempt to) send a message to the device on a streaming
     * virtual channel. If such a channel is not available, it will attempt the
     * primary virtual channel instead. There will be no indication of whether
     * this attempt was succesful or not. In particular, if this device object
     * is currently not connected, the method invocation will simply be ignored.
     *
     * @param device  The bluetooth device to send to. The shared pointer must
     *                point to a PHDBluetoothDevice, otherwise the method will
     *                use the primary virtual channel.
     * @param apdu    The buffer of bytes to transmit to the device.
     */
    virtual void sendApduStreaming(std::shared_ptr<VirtualPHD> device,
                                   std::shared_ptr<std::vector<uint8_t> > apdu)
                                                                    noexcept =0;
};




/* ************************************************************ */
/*                              USB                             */
/* ************************************************************ */

/**
 * @brief A USB specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying USB Personal Healthcare Device Class (PHDC) device.
 *
 * @todo This specialization must be defined when the first USB device is
 *       integrated in this framework.
 */
class PHDUSBDevice : public PhysicalPHD {
public:
    PHDUSBDevice() {}
    PHDUSBDevice(FSYS::Handle &h) : PhysicalPHD(h) {}


};


/**
 * @brief The PersonalHealthDeviceConnector class specialization for
 *        communication with USB devices (PHDUSBDevice).
 *
 * This class defines the communication between the USB Personal Healthcare
 * Device Class (PHDC) component in the PAL layer and session-layer components.
 *
 * @todo This specialization must be defined when the first USB device is
 *       integrated in this framework.
 *
 * @see PersonalHealthDeviceConnector
 * @see PHDUSBDevice
 */
class PHDUSBConnector : virtual protected PersonalHealthDeviceConnector {
protected:


};




/* ************************************************************ */
/*                             ZigBee                           */
/* ************************************************************ */

/**
 * @brief A ZigBee specialization of the PersonalHealthDevice.
 *
 * This abstract class extends the PersonalHealthDevice with properties of the
 * underlying ZigBee Health Care (ZHC) device.
 *
 * @todo This specialization must be defined when the first ZigBee device is
 *       integrated in this framework.
 */
class PHDZigBeeDevice : public PhysicalPHD {
public:
    PHDZigBeeDevice() {}
    PHDZigBeeDevice(FSYS::Handle &h) : PhysicalPHD(h) {}


};


/**
 * @brief The PersonalHealthDeviceConnector class specialization for
 *        communication with ZigBee devices (PHDZigBeeDevice).
 *
 * This class defines the communication between the ZigBee Health Care transport
 * component in the PAL layer and session-layer components.
 *
 * @todo This specialization must be defined when the first ZigBee device is
 *       integrated in this framework.
 *
 * @see PersonalHealthDeviceConnector
 * @see PHDZigBeeDevice
 */
class PHDZigBeeConnector : virtual protected PersonalHealthDeviceConnector {
protected:


};




/* ************************************************************ */
/*             Abstract Provider and Handler classes            */
/* ************************************************************ */

/**
 * @brief The abstract class, session-layer components must implement to
 *        handle communication with personal health devices from any
 *        transport class.
 *
 *
 * @see PersonalHealthDevice
 * @see PersonalHealthDeviceConnector
 * @see PersonalHealthDeviceProviderBase
 */
class PersonalHealthDeviceHandler :
        protected PHDBluetoothConnector,
        protected PHDUSBConnector,
        protected PHDZigBeeConnector {

public:
    /**
     * @brief Cleanup and make sure all devices are disconnected.
     *
     * The destructor will make sure that any connection to the PAL-layer
     * components that was left open will be disconnected, and that any
     * registered datatype that the session-layer component (child class)
     * forgot to unregister will be properly unregistered.
     */
    virtual ~PersonalHealthDeviceHandler();

protected:
    PersonalHealthDeviceHandler();
    void registerDatatype(uint16_t datatype);
    void unregisterDatatype(uint16_t datatype) noexcept;
    void sendApduPrimary(std::shared_ptr<VirtualPHD> device,
                       std::shared_ptr<std::vector<uint8_t> > apdu) noexcept;
    void disconnect(std::shared_ptr<VirtualPHD> device) noexcept;

    void sendApduStreaming(std::shared_ptr<VirtualPHD> device,
                       std::shared_ptr<std::vector<uint8_t> > apdu) noexcept;

private:
    // Opaque class will wrap the private parts
    class PrivatePHDHandler;
    PrivatePHDHandler *privateParts;
};




/**
 * @brief The abstract class, PAL-layer components must implement to provide
 *        communication with personal health devices.
 *
 *
 * @see PersonalHealthDevice
 * @see PersonalHealthDeviceConnector
 * @see PersonalHealthDeviceHandler
 */
class PersonalHealthDeviceProviderBase :
                               virtual protected PersonalHealthDeviceConnector {

public:
    /**
     * @brief Cleanup and make sure all devices are disconnected.
     *
     * The destructor will make sure that any connection to the session-layer
     * components that was left open will be disconnected.
     */
    virtual ~PersonalHealthDeviceProviderBase();

protected:
    PersonalHealthDeviceProviderBase();
    void connectIndication(std::shared_ptr<VirtualPHD> device) noexcept;

    void disconnectIndication(std::shared_ptr<VirtualPHD> device,
                              errortype error) noexcept;

    void apduReceived(std::shared_ptr<VirtualPHD> device,
                      std::shared_ptr<std::vector<uint8_t> > apdu,
                      bool reliableTransport) noexcept;

    // Opaque class will wrap the private parts
    class PrivatePHDProvider;
    PrivatePHDProvider *privateParts;
};




/**
 * @brief Bluetooth specialisation of the PersonalHealthDeviceProviderBase
 */
class PHDBluetoothProvider :
        protected PHDBluetoothConnector,
        protected PersonalHealthDeviceProviderBase {
    friend PrivatePHDProvider;
};





/*
class PlatformProviderLauncherBase {
public:
    PlatformProviderLauncherBase();
    virtual ~PlatformProviderLauncherBase();
    // Skal der være private copy/assignment ctor og operator?
protected:
    virtual Terminator makeBluetoothProvider();
    virtual Terminator makeUSBProvider();
    virtual Terminator makeZigBeeProvider();

};


class PlatformProviderLauncher : public PlatformProviderLauncherBase {
private:
    Terminator makeBluetoothProvider();
    Terminator makeUSBProvider();
    Terminator makeZigBeeProvider();
};

*/

} // End of namespace


#endif // PERSONALHEALTHDEVICE_H

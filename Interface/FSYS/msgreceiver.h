/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface declaration for the FSYS::MsgReceiver class
 *
 * /see FSYS::MsgReceiver
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#ifndef MSGRECEIVER_H
#define MSGRECEIVER_H

#include "msgaddrgenerator.h"
#include "msgcallback.h"
#include "msgqueue.h"

namespace FSYS
{
  /**
   * @brief Template class allowing other classes to receive messages
   *
   * This is the class that other classes can inherit from in order to receive
   * specific messages.
   *
   * When inheriting from this class you give the parent class and the class
   * of the message that you want to receive as template parameters.
   *
   * In order to receive a message, the class must inherit from this class and
   * implement a receive function that takes the type of message as parameter.
   *
   * @code
   *
   * class MyClass : public MsgReceiver<MyClass, MyMessage>
   * {
   * public:
   *  void receive(MyMessage &message);
   * }
   *
   * @endcode
   *
   * Remember to take a copy of message if you want to use its content later, or
   * want to respond to it.
   *
   * The receive function will be called in the thread context where the object
   * was initialised, and it is not possible to move it to another thead.
   *
   * @note The thread where the object is instantiatet must be a thread that is
   * created in a way that is compatible with the C++11 thread system.
   *
   * @note A class can only count on receiving a message if the class is fully
   * constructed and registered with the queue/message system at the time where
   * the message is sent.
   *
   * It is possible to declare the receive function as virtual if that is
   * desired.
   */
  template<class TReceiverClass, class TMsg> class MsgReceiver : private MsgCallBackT<TMsg>,
                                                                 public virtual MsgAddrGenerator<TReceiverClass>
  {
  public:
    /**
     * @brief Constructor for MsgReceiver class
     *
     * The constructor will register this class with the message system, so it
     * is possible for it to receive messages of the specific type.
     */
    MsgReceiver( void )
    {
      // Register this object on the listener queue of this thread
      MsgAddr destination = *this;
      MsgQueue::addListenerToQueue(destination,
                                   typeid(TMsg),
                                   this);
    }

    /**
     * @brief Destructor for MsgReceiver class
     *
     * The destructor of the MsgReceiver class unregisters this class from the
     * message system, so it won't attempt to call it again.
     */
    ~MsgReceiver()
    {
      // Remove this object for the listener queue
      MsgQueue::removeListenerFromQueue(this);
    }

  private:
    /**
     * @brief Function to generate a pointer to the parent class
     *
     * This class uses the pointer to the parent in order to call the receive
     * function on the parent.
     *
     * @return A pointer to the parent class
     */
    TReceiverClass* parentClassPointer()
    {
      return static_cast<TReceiverClass*>(this);
    }

    /**
     * @brief Function that does the actually call into the listening class
     *
     * This function takes the pointer to the listening class and calls the
     * receive function on that class
     *
     * @param msg A reference to the message that is being received
     */
    void callBack(TMsg &msg)
    {
      parentClassPointer()->receive(msg);
    }
  };
}

#endif // MSGRECEIVER_H

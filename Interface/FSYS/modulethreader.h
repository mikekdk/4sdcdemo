/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
#ifndef MODULETHREADER_H
#define MODULETHREADER_H

/**
 * @file
 * @brief Interface file for the module threader class
 */




namespace FSYS
{

  namespace ModulePrivate
  {
    /**
     * @brief The ModuleThreader class
     *
     * The module threader class is the class that starts all the threads in
     * the system.
     *
     * The functions in this class must only be called from the main thread
     */
    class ModuleThreader
    {
    private:

    public:
      /**
       * @brief Function that spawns all threads and starts their message loops
       *
       * Call this function to start the system.
       *
       * @note Calling this function will not start the message loop in the
       * calling thread
       */

      void spawnAll( void );

      /**
       * @brief Function that terminates all threads
       *
       * Call this function to stop all message loops and terminate the threads.
       *
       * The function will not return until all threads are terminated.
       *
       * The message loop of the main thread will not run while the other
       * threads are being terminated.
       *
       * Alternatively you can first broadcast FSYS::MsgGoingDown wait a set
       * time while you let the main message loop continue, and then broadcast
       * FSYS::MsgDestroy.
       *
       * In any case when the other threads are terminated, a
       * FSYS::MsgAllDestroyed message will be broadcasted
       */
      void terminateAll( void );
    };
  }
}

#endif // MODULETHREADER_H

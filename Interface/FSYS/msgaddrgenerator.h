/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains interface for a message address generator class
 *
 * /see FSYS::MsgAddrGenerator
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


#ifndef MSGADDRGENERATOR_H
#define MSGADDRGENERATOR_H

#include "handle.h"
#include "msgaddr.h"
#include "msgqueue.h"

namespace FSYS
{
/**
 * @brief Constructor to MsgAddrGenerator object
 *
 * The MsgAddrGenerator object generates a unique address for an instance
 * of a class.
 *
 * @todo Make a base class of this to handle the MsgAddr
 */
template<class T> class MsgAddrGenerator : private virtual Handle
{
private:
  /**
   * @brief The address stored in this handle
   */
  MsgAddr msgAddr;

  /**
   * @brief Generates the magic key for T
   *
   * @return The magic key for T
   */
  inline void *generateMagicKey( void )
  {
    //return static_cast<T*>(this);
    return this;
  }

  /**
   * @brief Function ensuring that the address is generated
   */
  inline void ensureInit( void )
  {
    msgAddr.magicKey = generateMagicKey();
  }

  inline MsgAddr &GetAddr( void )
  {
    ensureInit();
    return msgAddr;
  }

public:
  /**
   * @brief Constructor that creates an address
   */
  MsgAddrGenerator( void )
    : msgAddr(*this, MsgQueue::getHandle(), nullptr)
  {

  }

  /**
   * @brief MsgAddr operator for type conversion of MsgAddrGenerator to a MsgAddr
   *
   * This operator allows for conversion of a MsgAddrGenerator objects to a
   * MsgAddr object
   */
//  inline operator MsgAddr()
//  {
//    return GetAddr();
//  }

  /**
   * @brief MsgAddr operator for type conversion of MsgAddrGenerator to a MsgAddr
   *
   * This operator allows for conversion of a MsgAddrGenerator objects to a
   * MsgAddr object
   */
  inline operator MsgAddr&()
  {
    return GetAddr();
  }

};

}

#endif // MSGADDRGENERATOR_H

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
#ifndef MODULERUNNER_H
#define MODULERUNNER_H

/**
 * @file
 * @brief Interface file for the module runner class
 */

#include "FSYS/moduledeclare.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"

#include <initializer_list>
#include <list>
#include <string>

namespace FSYS
{

  namespace ModulePrivate
  {
    class LauncherBase;
    class Runner : public MsgSender<Runner>,
                   public MsgReceiver<Runner, MsgSystemReady>,
                   public MsgReceiver<Runner, MsgGoingDown>,
                   public MsgReceiver<Runner, MsgDestroy>
    {
    private:
      std::string groupName;
      std::list<LauncherBase*> groupModuleList;
      bool terminateMsgLoop = {false};

    public:
      /**
       * @brief Constructor for the Runner class
       *
       * This class keeps track of the user classes in a given group, by keeping
       * a list of launchers for the user classes.
       *
       * Ownership of the items in the module list is transfered to the Runner
       * class by calling this function, and they will be deleted with delete.
       *
       * @param groupName  The name for the group that is being initialised.
       *
       * @param moduleList A list of the launchers for modules that belog to the
       *                   group.
       */
      Runner(std::string groupName,
             std::initializer_list<LauncherBase*> moduleList);

      /**
       * @brief Function to start all static modules
       *
       * Calling this function will initialize and start all static modules
       */
      void startStaticModules( void );

      /**
       * @brief Calling this function will run the message loop for this thread
       *
       * This function will run the message loop for this thread until a
       * MsgDestroy is received by the runner class
       */
      void runMsgLoop( void );

      /**
       * @brief Function that stops and delete all modules owned by this runner
       *
       * Calling this function will stop and delete all modules from this runner
       */
      void shutdown( void );

      /**
       * @brief Receiver function for the MsgSystemReady broadcast
       *
       * The runner will call the msgSystemReady an all modules when this
       * message is received.
       */
      void receive(MsgSystemReady);

      /**
       * @brief Receiver function for the MsgGoingDown broadcast
       *
       * The runner will call msgGoingDown on all modules when this message
       * is received.
       */
      void receive(MsgGoingDown);

      /**
       * @brief Receiver function for the MsgDestroy broadcast
       *
       * After this message is received the message loop will be terminated and
       * the runner will destroy all of its modules
       */
      void receive(MsgDestroy);

    };
  }
}

#endif // MODULERUNNER_H


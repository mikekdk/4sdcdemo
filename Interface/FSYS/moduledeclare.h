/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains the declaration of the module defining macros and classes
 *
 * The macros and classes are used for defining the modules in the system.
 *
 * A module is in this context defined as an object that needs to be
 * instantiated (potentially in its own thread) and isn't instantiated
 * dynamically or as a static member of or in a different object.
 *
 * A module must inherit from the FSYS::Module class.
 *
 * *****************************************************************************
 *
 * A number of different module types exists:
 *
 * A "Static" module is one that will be created when the program is launched.
 * A static module can't and won't be deleted/destroyed until the program is
 * terminating.
 *
 * A "Delayed" module will be instantiated when a message of the type given in
 * its declaration macro is broadcasted in the system.
 *
 * A "Delayed" module can destroy it self by calling its deleteMe() function.
 *
 * If a "Delayed" module has destroyed it self it will be instantiated again
 * the next time a message of the type given in the template parameter is
 * broadcasted.  (Beware of raceconditions here)
 *
 * A "Dynamic" and "DynamicThread" module will be instantiated each time a
 * message of MsgType is broadcasted in the system.  A DynamicThread module will
 * be spawned in its own thread where a dynamic module will be created in the
 * thread defined by its module group.
 *
 * "Dynamic" and "DynamicThread" modules must destroy them self when they are
 * done, by calling their deleteMe() function, to avoid resource leaking and
 * CPU cycle leaking.
 *
 *******************************************************************************
 *
 * A Module is defined in the module definition file by using the MODULE*
 * macros.
 *
 * MODULE_GROUP(GroupName)
 *   MODULE_STATIC(ModuleClassName)
 *   MODULE_DELAYED(ModuleClassName)
 * MODULE_GROUP_END
 *
 * MODULE_GROUP(GroupName)
 *   MODULE_DYNAMIC(ModuleClassName)
 *   MODULE_DYNAMIC_THREAD(ModuleClassName)
 * MODULE_GROUP_END
 *
 * All modules in a group are guaranteed to run in the same thread, modules in
 * different groups, might or might not run in the same thread, depending on the
 * configuration of the platform that the code is run on (With
 * MODULE_DYNAMIC_THREAD as an obvious exception to this rule)
 *
 * Currently only MODULE_STATIC is implemented
 *
 *******************************************************************************
 *
 * The ModuleX baseclass will implement and call the following virtual functions
 * that can be used by the module to act, depending on where in its life cycle
 * it is, by overriding the virual functions
 *
 * void msgReady( void )
 * ---------------------
 * This function is called  when the module can start to use the message system.
 * It is guarantied that this function is called before any messages are
 * received by the module.
 *
 * void msgGroupReady( void )
 * --------------------------
 * This function is called when all the static modules in the group are ready
 * to use the message system (this means that the module can interact through
 * the message system with the other static modules in the group).
 *
 * void msgSystemReady( void )
 * ---------------------------
 * This function is called when all static modules in all groups are ready to
 * use the message system (this means that the module can interact through the
 * message system with all other static modules in the system (in all groups))
 *
 * void msgGoingDown( void )
 * -------------------------
 * This function is called to inform the module that the shutdown sequence of
 * the system has been initiated and that it should release all its system
 * resources.
 *
 * void msgDestroy( void )
 * -----------------------
 * After this function is called the module can no longer rely on the message
 * system and should prepare for imminent destruction.
 *
 *******************************************************************************
 *
 * The msgSystemReady() and msgGoingDown() function calls will also be
 * broadcasted as standard messages
 *
 * FSYS::MsgSystemReady
 * --------------------
 * Broadcasted when all static modules are started.  If a module is created
 * after this message is broadcasted, then it won't be able to receive this
 * message, but it will still have its void msgSystemReady( void ) function
 * called.
 *
 * FSYS::MsgGoingDown
 * ------------------
 * Broadcasted when the system is shutting down.  If a module is created after
 * this message is broadcasted, then it won't be able to receive this message,
 * but it will still have its void msgGointDown( void ) function called.
 *
 *******************************************************************************
 *
 * The following messages are being used internally.
 *
 * FSYS::MsgGroupReady
 * -------------------
 * Broadcasted by a group when all of its static modules have launched
 *
 * FSYS::MsgDestroy
 * ----------------
 * Broadcasted when all the threads should terminate
 *
 * FSYS::MsgAllDestroyed
 * ---------------------
 * Broadcasted when all threads are destroyed
 *
 *******************************************************************************
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MODULEDECLARE_H
#define MODULEDECLARE_H

#include "FSYS/basemsg.h"
#include "FSYS/log.h"

#include <assert.h>

namespace FSYS
{
  /**
   * @brief Base class for all modules
   *
   * The Module class is the base class for all modules, except for the ModuleX
   * classes, no other classes should ever need to inherit directly from this
   * class.
   */
  class Module
  {
  public:
    virtual void msgReady( void ) {}
    virtual void msgGroupReady( void ) {}
    virtual void msgSystemReady( void ) {}
    virtual void msgGoingDown( void ) {}
    virtual void msgDestroy( void ) {}
    virtual ~Module( void ) {}
  };

  /**
   * @brief Message broadcasted when the system is initialised
   *
   * Broadcasted when all static modules are started.  If a module is created
   * after this message is broadcasted, then it won't be able to receive this
   * message, but it will still have its void msgSystemReady( void ) function
   * called.
   */
  class MsgSystemReady : public BaseMsg {};

  /**
   * @brief Message broadcasted when the system is being terminated
   *
   * Broadcasted when the system is shutting down.  If a module is created after
   * this message is broadcasted, then it won't be able to receive this message,
   * but it will still have its void msgGointDown( void ) function called.
   */
  class MsgGoingDown : public BaseMsg {};

  /**
   * @brief Message broadcasted when all static modules in a group are ready
   *
   * This message is sent by each of the runners when their static modules
   * have started and they are about to enter their message loop
   */
  class MsgGroupReady : public BaseMsg {};

  /**
   * @brief Last message broadcasted to threads
   *
   * After this message has been received the message loops in the system will
   * terminate and the program shut down
   */
  class MsgDestroy : public BaseMsg {};

  /**
   * @brief Last message broadcasted in system
   *
   * When this message is broadcasted, the main threads message loop will
   * terminate.
   */
  class MsgAllDestroyed : public BaseMsg {};

namespace ModulePrivate
{
  /**
   * @brief The LauncherBase class is the non-template class for other Launchers
   *
   * The Launcher class is responsible for the user class (a user class is a
   * user written class that is not part of the FSYS framework).
   *
   * The Launcher class is a member of the Macro class (the class that is
   * generated by Macros in the module group macro definitions).
   */
  class LauncherBase
  {
  private:
    /**
     * @brief Calls the msgReady function on the user class
     */
    void sendMsgReady( void );

    /**
     * @brief Calls the msgDestroy function on the user class
     */
    void sendMsgDestroy( void );

    /**
     * @brief Boolean telling if the contained object is initialized
     */
    bool isCreated = {false};

  protected:
    Module *userObject{nullptr};   // The pointer to the real object

    /**
     * @brief Virtual function, called when the user object should be created
     *
     * The specialisations to the LaunchBase class needs to implement this
     * function, so this class can call this function to have the object
     * (the user class) instantiated.
     */
    virtual void create( void ) = 0;

    /**
     * @brief Virtual function, called when the user object should be destroyed
     *
     * The specialisations to the LauncherBase class needs to implement this
     * function, so the LauncherBase class can call this function to have the
     * (the user class) object destroyed again.
     */
    virtual void destroy( void ) = 0;

  public:
    enum class ModuleType {
      UNKNOWN,
      STATIC,
      DELAYED,
      DYNAMIC,
      DYNAMIC_THREAD
    };

    /**
     * @brief Function to retrieve info about the user object containd
     *
     * This funciton can be used to find the type module the user module
     * contained in this launcher has
     *
     * @return The ModuleType of the contained object
     */
    virtual ModuleType getType( void ) = 0;

    /**
     * @brief Function that starts the user class
     *
     * This function starts the class, and ensures that the right callbacks
     * are made to it.
     */
    void startup( void );

    /**
     * @brief Function that shuts the user class down
     *
     * This function informs the class that it is being shut down, and shuts
     * it down.
     */
    void shutdown( void );

    /**
     * @brief Boolean function telling if the module is currently running
     *
     * @return true if the module is up and running, false if not
     */
    inline bool isRunning( void );

    /**
     * @brief Calls the msgGroupReady function on the user class
     */
    void sendMsgGroupReady( void );

    /**
     * @brief Calls the msgSystemReady function on the user class
     */
    void sendMsgSystemReady( void );

    /**
     * @brief Calls the msgGoingDown function on the user class
     */
    void sendMsgGoingDown( void );

    /**
     * @brief LauncherBase destructor
     *
     * Ensures the LauncherBase has a virtual destructor
     */
    virtual ~LauncherBase( void ) {}
  };

  /**
   * @brief The Launcher class is used for dynamically launching the user clases
   *
   *
   */
  template <class TypeToLaunch,
            LauncherBase::ModuleType mt=LauncherBase::ModuleType::STATIC>
    class Launcher : public LauncherBase
  {

  private:
    void create( void ) override
    {
      // Ensure the user object is null
      assert(nullptr == userObject);

      // Instantiate the user object
      userObject = new TypeToLaunch;
    }

    void destroy( void ) override
    {
      // Delete the user object
      delete userObject;

      // Make sure the user object is null
      userObject = nullptr;
    }

    ModuleType moduleType = {mt};
  public:
    /**
     * @brief Function to retrieve info about the user object containd
     *
     * This funciton can be used to find the type module the user module
     * contained in this launcher has
     *
     * @return The ModuleType of the contained object
     */
    ModuleType getType( void ) override
    {
      return moduleType;
    }

  };

  /**
   * @brief Dummy launcher class to mark the end of the launcher list
   *
   *
   */
  class LauncherEnd : public LauncherBase
  {

  private:
    void create( void ) override {};

    void destroy( void ) override {};

  public:
    ModuleType getType( void ) override
    {
      return LauncherBase::ModuleType::UNKNOWN;
    }

  };

}

}
#endif

#!/bin/sh
#
# Small shell script used to extract the version number from
# git to Doxygen
#
# AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015
# LICENSE:  Apache 2.0
#

git log -1 --pretty="format:This file was last changed: %ci, by %aN" "$1" 2>/dev/null || echo No version information available at this time

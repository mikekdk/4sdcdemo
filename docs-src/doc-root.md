
Introduction     {#mainpage}
============

\if 0
******************************************************************************
***                                                                        ***
***     This file contains the first (and main) page of the 4SDC           ***
***     documentation, with the introduction and documentation overview.   ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


The 4S Device Communication module collection (4SDC) is a cross
platform library for applications interacting with personal health
devices (like blood pressure monitors, oximeters, thermometers, weight
scales and so on).

4SDC comprises a cross-platform library written in C++ which is able
to communicate through a Platform Abstraction Layer (PAL) with
platform-specific modules.


\if full

This manual is aimed at contributors to the 4SDC module collection,
and contains the full and hairy implementation details. 

\else 

This manual is aimed at developers who are either going to use modules
from the 4SDC module collection or need a general technical overview
of the 4SDC architecture. The content of this manual will therefore
not cover implementation details.

\endif


\htmlonly
This manual is also available as a PDF file.
\endhtmlonly
\latexonly
This manual is also available as interactive web pages.
\endlatexonly
Please refer to [www.4s-online.dk/4SDC/Documentation]
(http://www.4s-online.dk/4SDC/Documentation)
for the relevant documentation.


\if full

\note 
If you are not intending to modify any of the 4SDC modules, you are
encouraged instead to look at the *Interface Documentation*, which is
aimed at developers who are only going to use the modules but have no
need for the internal implementation details (as well as other people,
who just need the essential overview). The *Interface Documentation*
is also the recommended place to start for new contributors, who wish
to get an overview of the 4SDC module collection without too much
"noise".

\note
The latest version of the *Interface Documentation* can be found here:

\note
[www.4s-online.dk/4SDC/Documentation/latest/interface/]
(http://www.4s-online.dk/4SDC/Documentation/latest/interface/)

\else

\note
Developers already familiar with the architecture, who are planning to
contribute to the 4SDC modules, are encouraged to head over to the
*Full Documentation*, which contains all the hairy implementation
details.

\note
The latest version of the *Full Documentation* can be found here:

\note
[www.4s-online.dk/4SDC/Documentation/latest/full/]
(http://www.4s-online.dk/4SDC/Documentation/latest/full/)

\endif


Documentation Overview     {#sec_overview}
======================

\htmlonly
This manual is organised with a number of introductory pages followed
by detailed information about each of the modules, classes and
files. The pages can be navigated by using the tree-view on the left
of your screen (and of course by following the hyperlinks). If you
wish to print a hardcopy of the documentation instead, we suggest
printing from the PDF file instead, as it is optimised for printing.
\endhtmlonly
\latexonly
This manual is organised with a number of introductory chapters
followed by chapters with detailed information about each of the
modules, classes and files. The detailed information in the last
(huge) chapters may be easier to navigate on the interactive web pages
version of the manual.
\endlatexonly

If you are new to the world of 4SDC, we recommend that you read the
\ref page_start "Getting Started"
\htmlonly
page, followed by the pages
\endhtmlonly
\latexonly
chapter on page \pageref{page_start}, and continue through the
following chapters
\endlatexonly
about the 4SDC architecture.


Other Sources of Information     {#sec_external_docs}
============================

The documentation found
\htmlonly
on these pages
\endhtmlonly
\latexonly
in this document
\endlatexonly
is generated from the source code of a certain version of the 4SDC
library, and is therefore rather static in nature. However, there are
a few other sources of 4SDC information, you should be aware of -- all
of them are more dynamic, and of course you are invited to contribute
as well:
\addindex Wiki
<dl><dt>Wiki</dt>
<dd>The 4SDC Wiki page at
[4s-online.dk/wiki/doku.php?id=4sdc:]
(http://4s-online.dk/wiki/doku.php?id=4sdc:) is used first of
all as a general overview over the 4SDC module collection and secondly
as the tutorial on how to get the demo application up and running -- on
various platforms.</dd>
\addindex Issuetracker
\addindex Jira
<dt>Issuetracker</dt>
<dd>The issuetracker at [issuetracker4s.atlassian.net/projects/SDC/]
(https://issuetracker4s.atlassian.net/projects/SDC/) is used to report
bugs and feature requests, and to coordinate the work on the 4SDC
modules.</dd>
\addindex Forum
<dt>Forum</dt>
<dd>The forum at [4s-online.dk/forum/] (http://4s-online.dk/forum/) is
used for Q/A and discussions.</dd></dl>

\if 0
******************************************************************************
***                                                                        ***
***    Below the order of the remaining chapters of the documentation      ***
***    is defined                                                          ***
***                                                                        ***
******************************************************************************
\endif


\page      page_start          Getting Started
\page      page_arch           Architecture
\if full
\page      page_conformance    Conformance Information
\endif
\page      page_license        License
\page      page_contributors   Contributors


\defgroup  group_presentation  Presentation Layer
\defgroup  group_session       Session Layer
\defgroup  group_pal           Platform Abstraction Layer
\defgroup  group_system        System Layer

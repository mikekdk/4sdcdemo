
Architecture     {#page_arch}
============

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/sections with the architecture introduction.                  ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


[TOC]

\addindex OpenTele3
\addindex Medical Devices Directive
\addindex IEC 62304
The 4SDC module collection is born out of -- and closely linked to --
the
\htmlonly
<a href="http://4s-online.dk/wiki/doku.php?id=opentele3:">OpenTele3
architectural design efforts</a>. The <a
href="http://4s-online.dk/wiki/doku.php?id=opentele3:ot3_to_be_architecture#general_principles">General
Principles</a>
\endhtmlonly
\latexonly
OpenTele3 architectural design efforts, which you can read more about
here: \href{http://4s-online.dk/wiki/doku.php?id=opentele3:}
{\nolinkurl{4s-online.dk/wiki/doku.php?id=opentele3:}}. The
\href{http://4s-online.dk/wiki/doku.php?id=opentele3:ot3_to_be_architecture#general_principles}
{General Principles}
\endlatexonly
of the OpenTele3 architecture are all fundamental in the architecture
of 4SDC as well. Furthermore, the 4SDC project is currently used by
the 4S organisation to develop the governance models needed to support
certification according to the
\htmlonly
<a
href="http://www.wikipedia.org/wiki/Medical_Devices_Directive">Medical
Devices Directive</a>
\endhtmlonly
\latexonly
\href{http://www.wikipedia.org/wiki/Medical_Devices_Directive}
{Medical Devices Directive}
\endlatexonly
-- specifically the
\htmlonly
<a href="http://www.wikipedia.org/wiki/IEC_62304">IEC 62304</a>
\endhtmlonly
\latexonly
\href{http://www.wikipedia.org/wiki/IEC_62304} {IEC 62304}
\endlatexonly
\cite iec62304 software life cycle processes.

\addindex OpenTele3
\addindex Danish Reference Architecture
\addindex Reference Architecture for Collecting Health Data From Citizens
\addindex Continua Design Guidelines
The OpenTele3 architectural design relates to the Danish _Reference
Architecture for Collecting Health Data From Citizens_ (\cite refarkEN
and \cite refarkDK). This reference architecture in turn points toward
the _Continua Design Guidelines_ (CDG) \cite cdg as a technical
framework for collecting the data from personal health devices. As
this is the core purpose of 4SDC, a basic understanding of the CDG
would be a good place to begin. A brief introduction to the CDG can be
found in this
\htmlonly
<a
href="https://cw.continuaalliance.org/document/dl/13473"><i>Fundamentals
of Data Exchange</i></a>
\endhtmlonly
\latexonly
\href{https://cw.continuaalliance.org/document/dl/13473}{\em{Fundamentals
of Data Exchange}}
\endlatexonly
white paper \cite FDEwhitepaper.

The purpose of
\htmlonly
this and the following pages
\endhtmlonly
\latexonly
this chapter
\endlatexonly
is to describe the 4SDC overall architecture, as well as the reasoning
behind it. To set the stage, the following sections will present the
scope of this module collection and explain some of the key terms and
concepts.
\htmlonly
On the following <a href="page_arch_background.html">Background and
Basics</a> page
\endhtmlonly
\latexonly
In section \ref{page_arch_background} on page
\pageref{page_arch_background}
\endlatexonly
the basic requirements and their implications on the overall
architecture are presented. This will then lead to the presentation of
the (goal) architecture for the 4SDC module collection
\htmlonly
on <a href="page_arch_details.html">The 4SDC Architecture</a>
page. Finally, the <a href="page_arch_status.html">Architecture Status
and Road-maps</a> page
\endhtmlonly
\latexonly
in section \ref{page_arch_details} on page
\pageref{page_arch_details}. Finally, section \ref{page_arch_status}
on page \pageref{page_arch_status}
\endlatexonly
will outline the status of the current 4SDC library, and how we plan
to complete the implementation of this architecture.


Scope     {#sec_scope}
=====

The 4SDC library is not intended to be just another implementation of
communication standards, rather it is designed to be a framework
adapting to the users' needs, drawing on the extensive experience from
OpenTele. To better understand what the 4SDC is designed to accomplish,
\htmlonly
consider the following analogy:
\endhtmlonly
\latexonly
consider the analogy from Figure \ref{fig:scopestory}.
\endlatexonly

\htmlonly[block]
<div style="border-style:solid;padding:6px;background-color:#d9defa">
\endhtmlonly
\latexonly
\newsavebox{\mybox}
\begin{lrbox}{\mybox}\begin{minipage}{.9\columnwidth}
\endlatexonly

You are about to print a hardcopy of a document on a printer. In order
to do so, your application must communicate with the printer, so there
must be a _communication protocol_ in place (for instance
communication over a local network). Furthermore, your application
must provide the printer with a description of what the printed page
should look like, so the application and printer must share a
_document format_
\htmlonly
(<a href="http://www.wikipedia.org/wiki/PostScript">PostScript</a>
\endhtmlonly
\latexonly
(\href{http://www.wikipedia.org/wiki/PostScript}{PostScript}
\endlatexonly
is a commonly used format for this). This is all you will need to get
the document hardcopy -- if all goes well, that is. For what will
happen if your printer is out of paper or toner, or perhaps
experiences a paper jam? Or what if you want to print on both sides of
the paper or want to use the built-in stapler? You would want your
application to help you with that, right?

Now, if you have 10 different applications and 4 types of printers,
and each application should be able to handle all printers, you would
need 40 integration efforts to make that possible!

Introducing the operating system's _printing system_ with _printer
drivers_: rather than having each application communicating directly
with the printer, the application will deliver its document to the OS
printing system which will use one of the just 4 different printer
drivers to interact with your printer. The printer drivers will offer
access to the advanced settings of the printer (such as duplex
printing or stapling), and help you troubleshoot problems -- typically
with images or even animated guides instructing you to fix paper jams
or change the toner cartridge.
\htmlonly[block]
</div>
\endhtmlonly
\latexonly
\end{minipage}\end{lrbox}
\begin{figure}\centering
\colorbox[rgb]{0.85,0.87,0.98}{\fbox{\usebox{\mybox}}}
\caption{\label{fig:scopestory}A story with an analogy from the world
of printing}
\end{figure}
\endlatexonly

\addindex OpenTele
To complete this analogy, think of OpenTele -- or any other
application using 4SDC -- as the "application" and a personal health
device as the "printer". The standards mandated by the CDG (such as
Bluetooth HDP \cite bluetoothHDP and IEEE 11073 PHD \cite phd20601) or
any proprietary device communication will play the role as
"communication protocol" and "document format", and finally 4SDC
itself will be the "printing system". As this analogy illustrates, the
purpose of the "printer drivers" of 4SDC is not merely to provide
communication between the application and the device, but their
purpose is also to focus on the users' needs, providing in-context
user instructions and help/guides.

So while the CDG have a significant influence on the architecture of
4SDC, many important features in 4SDC are completely out of their
scope. Furthermore, 4SDC must support any type of device, and cannot
be limited to only Continua compliant devices. Focus is on the needs
of OpenTele, and in OpenTele a number of non-Continua-compliant
devices are currently in use. Most likely, this will always be the
case, as there will always be a need for using new and experimental
(or proprietary) devices (at least in research projects). Furthermore,
as a new device type is introduced to the market, it will take a
couple of years before a standard way of communicating with this
device is ratified, and in the meantime only non-standard
communication protocols can be available.



Terminology     {#sec_terminology}
===========

Before we dig into the architecture details, this section will list
the terms and definitions used in the remainder of this document.

Components     {#sec_term_component}
----------

\addindex Component
Software components are classified and grouped using the following
\htmlonly
terms:
\endhtmlonly
\latexonly
terms (cf. Figure \ref{fig:modulerelation}):
\endlatexonly

\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.6\textwidth]{modulerelation.pdf}
\caption{\label{fig:modulerelation}Example of the relation between
\emph{modules}, \emph{submodules}, and \emph{units}.}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px;">
<img src="modulerelation.svg"
 alt="UML'ish illustration of model relations">
<div class="caption">Example of the relation between <i>modules</i>,
<i>submodules</i>, and <i>units</i>.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex modulerelation.pdf
\image html modulerelation.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly

\addindex Unit
<dl><dt>Unit</dt><dd>
This word is used to indicate the smallest&nbsp;/ atomic software
components -- typically a class or a compilation unit (i.e. a source
file). This is different from a module (defined below) as a unit need
not have any public interfaces, nor does it have to adhere to the
module contract. A (very small) module may also be a unit, however,
this is probably a rare case.
</dd>
\addindex Module
\addindex Module interface
<dt>Module and Module interface</dt><dd>
A _module_ is a software component, which is independently replaceable
(and upgradable). It is fully defined by a set of _module interfaces_
(which are always public), and other modules exposing the same module
interfaces may be used as drop-in replacements (possibly resulting in
different functionality). A module must adhere to the
\htmlonly
<a class="el"
href="page_arch_background.html#sec_module_contract"><i>Module
Contract</i></a>.
\endhtmlonly
\latexonly
\emph{Module Contract} described in section
\ref{page_arch_background_sec_module_contract} on
page \pageref{page_arch_background_sec_module_contract}.
\endlatexonly
</dd>
\addindex Submodule
<dt>Submodule</dt><dd>
Larger modules may be constructed by a number of smaller components,
called _submodules_, which in turn may comprise more submodules and so
on. Submodules are _not_ subject to the
\htmlonly
<a class="el"
href="page_arch_background.html#sec_module_contract"><i>Module
Contract</i></a>,
\endhtmlonly
\latexonly
\emph{Module Contract} described in section
\ref{page_arch_background_sec_module_contract} on
page \pageref{page_arch_background_sec_module_contract},
\endlatexonly
although they may adhere to some or all of the requirements. (Typically
submodules will expose private interfaces.)
\htmlonly
See the figure 
\endhtmlonly
\latexonly
See Figure \ref{fig:modulerelation}
\endlatexonly
for an example of this.
</dd>
\addindex Module collection
<dt>Module collection</dt><dd>
This term is used to signify a group of modules sharing _governance_,
such as the 4SDC _Module Collection_. The modules of the collection
will reside in the same source code repository and share
version/revision numbers etc. A module collection may comprise modules
that are not normally used together (thus, not belonging to the same
_runtime library_). For instance, one subsets of the 4SDC module
collection may be used to build a device communication _library_ for
_Android_ and a different subset will build a library for _iOS_.
</dd>
\addindex Layer
<dt>Layer</dt><dd>
This is another term used to group related modules based on their
function&nbsp;/ purpose. Please read section ?? for more
information. Notice that two modules may belong to different
module collections but be in the same layer or vice versa.

\todo link to architecture.
</dd>
\addindex Part
<dt>Part</dt><dd>
The term _part_ is used in road-map and planning documents to describe
an existing piece of software (component or application) of
unspecified size, which has multiple responsibilities and therefore is
subject to _partitioning_ (see below).
</dd>
\addindex Partition
\addindex Modularisation
<dt>Partitioning and Modularisation</dt><dd>
These terms are used in road-maps and planning documents to describe
the steps toward the goal architecture as a mixture of top-down and
bottom-up approaches. _Partitioning_ is the act of taking an existing
piece of software (the "part") and splitting it up in smaller parts
with public interfaces between them. This is the top-down approach. In
this approach, we move towards the goal architecture by
recursively/repeatedly partitioning the smaller parts until we end up
with suitable modules. The _Modularisation_ approach works bottom-up
by building well-designed modules one by one and working upwards to a
complete application.
</dd>
</dl>


Terms inherited from CDG     {#sec_term_cdg}
------------------------

\addindex Continua Design Guidelines
The following terms are inherited from the Continua Design Guidelines
(CDG)\cite cdg and all the related standards, these guidelines recommends:

\addindex PHD
\addindex Personal Health Device (PHD)
<dl>
<dt>Personal Health Device (PHD)</dt><dd>
This is the general term used for all the kinds of devices that may be
connected to 4SDC. The term comprises medical sensors (such as blood
pressure monitors, thermometers, oximeters etc.), medical actuators
(e.g. insulin pumps), as well as health and fitness devices
(e.g. weight scales, pedometers, cadence sensors, activity monitors).
</dd>
\addindex PAN
\addindex Personal Area Network
<dt>PAN</dt><dd>
A personal area network is typically defined as a network with a short
range of about 10 meters. As the name suggest, it is typically
centered around a _person_ and "moves" with her -- for instance with a
smartphone or laptop as the central hub.  In the CDG three
technologies are recommended:
 - **USB** which is a cabled network with a range of 5 meters.
 - **Bluetooth Classic** which is a wireless network with a range of
   about 10 meters (see below).
 - **Bluetooth LE** which is a wireless network with a range of
   about 10 meters (see below).
</dd>
\addindex LAN (Sensor)
\addindex Sensor-LAN
\addindex Local Area Network (Sensor)
<dt>LAN (Sensor)</dt><dd>
A local area network is typically covering a larger area than the PAN
and in addition to that, it is normally fixed to a geographic location
-- for instance permanently installed in a building. CDG currently
recommends only one LAN technology:
 - **ZigBee** which is a low-power wireless network typically used for
   home-automation, and especially suited for long-time
   battery-operated sensors.
</dd>
\addindex TAN
\addindex Touch Area Network
<dt>TAN</dt><dd>
A touch area network is like a PAN but with a significantly shorter
range -- less than 1 meter, and often in the order of a few cm. TAN
technologies often (but not always) includes power transfer, so that
the sensor device need not have its own power supply, but receives
power from the master device during the communication. CDG currently
recommends only one TAN technology:
 - **NFC** which is a group of standards from the RFID family of
   technologies. It supports wireless communication within a range
   of (typically) less than 10 cm along with wireless power transfer
   to the sensor.
</dd>
\addindex BC
\addindex Bluetooth Classic (BC)
<dt>Bluetooth Classic (BC)</dt><dd>
The original Bluetooth wireless cable-replacement standard, also known
as Bluetooth Basic Rate&nbsp;/ Extended Data Rate (BR/EDR). The
technology behind BC is fundamentally different from \protect{BLE}
(see below) and a BC-only device will not be able to communicate with
a BLE-only device. Even the communication protocols used for personal
health devices are completely different. BC uses the HDP profile (also
described below) along with the IEEE 11073 PHD standards\cite phd20601
for PHD communication.
</dd>
\addindex BLE
\addindex Bluetooth LE (BLE)
<dt>Bluetooth LE (BLE)</dt><dd>
The new Bluetooth wireless cable-replacement standard for low power
(and low data throughput) communication (also branded as "Bluetooth
Smart"). The BLE technology is fundamentally different from BC (see
above) and a BC-only device will not be able to communicate with a
BLE-only device. Even the communication protocols used for personal
health devices are completely different. BLE uses the GATT profile
(see below) along with different specific profiles and
transcoding\cite bletranscoding for PHD communication.
</dd>
\addindex HDP
\addindex Health Device Profile (Bluetooth Classic)
\addindex Bluetooth HDP
<dt>Health Device Profile (HDP)</dt><dd>
The Bluetooth (classic) HDP\cite bluetoothHDP defines how a BC-based
personal health device can be connected to a host and deliver data
using the IEEE 11073 PHD\cite phd20601 protocol.
</dd>
\addindex GATT
\addindex Generic Attribute Profile (GATT)
<dt>Generic Attribute Profile (GATT)</dt><dd>
The parent of all Bluetooth LE profiles. BLE sensors publish one or
more _services_, each exposing a number of _characteristics_,
containing _attributes_ that may be queried using operations from the
Generic Attribute Profile.
</dd>
\addindex PHDC (USB)
\addindex Personal Healthcare Devices Class (USB)
<dt>Personal Healthcare Devices Class (USB-PHDC)</dt><dd>
The USB PHDC\cite USB_PHDC defines how a USB-based personal health
device can be connected to a host and deliver data using the IEEE
11073 PHD\cite phd20601 protocol.
</dd>
\addindex ZHC Profile
\addindex ZigBee Health Care Profile
<dt>ZigBee Health Care Profile (ZHC)</dt><dd>
The ZigBee ZHC\cite ZHC defines how a ZigBee-based personal health
device can be connected to a host and deliver data using the IEEE
11073 PHD\cite phd20601 protocol.
</dd>
\addindex PHDC (NFC)
\addindex NFC-PHDC
\addindex Personal Health Device Communication (NFC)
<dt>Personal Health Device Communication (NFC-PHDC)</dt><dd>
The NFC PHDC\cite NFC_PHDC specification defines how an
\protect{NFC}-based personal health device can be connected to a host
and deliver data using the IEEE 11073 PHD\cite phd20601 protocol.
</dd>
\addindex AHD
\addindex Application Hosting Device
<dt>AHD</dt><dd>
The Application hosting Device, also known as the _Personal Health
Gateway_, is the point of data collection near the citizen. Typically
a smartphone, pc, tablet, or a set-top box. The role of the AHD is to
collect the data from the PHDs and forward these data to the WAN
device (see below) using the PCD-01 data format (see below).
</dd>
\addindex WAN (device / interface)
\addindex Wide Area Network (device / interface)
<dt>WAN (device / interface)</dt><dd>
The WAN (wide area network) device, also known as the _Health and
Fitness Service_, is the central point of data collection hosted by
the service provider. This is where the AHD will deliver the collected
data. The WAN interface (between the AHD and WAN devices) uses the
PCD-01 data format (see below).
</dd>
\addindex PCD-01
<dt>PCD-01</dt><dd>
The data format for WAN interface communication (between the AHD and
WAN devices) is defined by \cite pcd. The CDG recommends using either
SOAP or REST as the exchange protocol with PCD-01 as the message
content.
</dd>
</dl>



\htmlonly
<h3>Proceed to <a href="page_arch_background.html">Background and Basics</a></h3>
\endhtmlonly


\if 0
******************************************************************************
***                                                                        ***
***    Below we insert the sub pages without any (visual) references       ***
***                                                                        ***
******************************************************************************
\endif
\htmlonly
<!--
\endhtmlonly
\latexonly
\iffalse
\endlatexonly
\subpage page_arch_background
\subpage page_arch_details
\subpage page_arch_status
\htmlonly
-->
\endhtmlonly
\latexonly
\fi
\endlatexonly

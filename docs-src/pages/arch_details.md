
The 4SDC Architecture     {#page_arch_details}
=====================

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/sections with the architecture details.                       ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


[TOC]


\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.4\textwidth]{pal_and_system.pdf}
\caption{\label{fig:palandsystem}Conceptual blabla.}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px;">
<img src="pal_and_system.svg" alt="" width="300px">
<div class="caption">Conceptual blabla.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex pal_and_system.pdf
\image html pal_and_system.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly



\latexonly
\begin{figure}\centering
\includegraphics[page=1,width=.6\textwidth]{osi.pdf}
\caption{\label{fig:osilayers}Conceptual blabla.}
\end{figure}
\begin{comment}
% a hack to tell Doxygen to copy the image file
\endlatexonly
\htmlonly
<div class="image" style="float: right; margin-left: 30px;
 margin-bottom: 30px;">
<img src="osi.svg" alt="" width="450px">
<div class="caption">Conceptual blabla.</div>
</div><div style="display:none; visibility:hidden;">
a hack to tell Doxygen to copy the image file
\endhtmlonly
\image latex osi.pdf
\image html osi.svg
\latexonly
\end{comment}
\endlatexonly
\htmlonly
</div>
\endhtmlonly


\if 0

Følgende figur illustrerer den foreslåede arkitektur, hvor de enkelte individuelle moduler er skilt ud i bokse og placeret i forhold til deres relationer. De enkelte bokse kan (i det mindste i princippet) implementeres uafhængigt af hinanden og genbruges enkeltvist i andre sammenhænge. OpenTele client og backend boksene repræsenterer dog ”eksterne” (i forhold til dette notat) større systemer. De cyanblå bokse udgør tilsammen et CHA ”Application Hosting Device”. Alle moduler over ”Platform Abstraction Layer” / ”Connection Control and Security” er uafhængige af kommunikationsinterface og operativsystem, og vil kunne implementeres fuldstændigt system-uafhængigt. De enkelte kommunikations-komponenter (USB, Bluetooth, Zigbee) vil skulle implementeres for hvert operativsystem. 

CHA-certificerede måleapparater, som kommunikerer over USB, Bluetooth ”classic” eller Zigbee, benytter alle IEEE 11073 PHD standarden (se Appendix A) til at oversætte den ”rå” datakommunikation til en objektorienteret model af måleapparatet, kendt som ”Domain Information Model” eller DIM. For Bluetooth Low Energy er billedet en smule anderledes, da disse måleapparater er så begrænsede, at de ikke kan håndtere IEEE 11073 PHD standarden. I stedet benyttes her en simplere model, som dog er aligned med IEEE 11073’s DIM på en sådan måde, at en oversættelse hertil kan ske. Set ”oppefra” er der således ikke forskel på Bluetooth LE måleapparater og de andre apparattyper. 

Ikke-Continua-måleapparater kan man ikke sige noget generelt om. De kan opføre sig vidt forskelligt, og der er ikke nogen garanti for at de kan modelleres i DIM universet, selv om mange sikkert helt eller delvist kan. Dette er illustreret på figuren med en ”omvej” mellem ”Non-CHA compliant devices” boksen og OpenTele klienten. 

CHA foreskriver, at en AHD enhed oversætter målingerne fra DIM til HL7 observationsrapporter, og i denne forbindelse suppleres med en række meta-data omkring målingerne – primært brugerens identitet og kontekst. Disse oplysninger må nødvendigvis indhentes fra OpenTele klienten, som kender sin bruger. Observationsrapporterne afleveres så til en backend server (kaldet ”WAN device” i CHA termer) via et SOAP interface specificeret af IHE (PCD-01).

For at OpenTele klienten kan vise både nye måledata og historiske data fra serveren for brugeren, kan man supplere med endnu en generisk komponent til fremvisning af måledata på ORU^R01 formatet. Denne ”viewer” vil være meget nært relateret til den PHMR viewer, der allerede er under udvikling i 4S regi, og vil sandsynligvis kunne dele såvel arkitektur som kode med denne.





PAL (Platform Abstraction Layer)
PAL er et grænsefladelag, som udelukkende består af en række grænseflader/interfaces - dvs. dette lag indeholder ingen kode. PAL udgøres af følgende interfaces (foreløbige navne):
•	Kommunikations-interface
•	Tråd-interface
•	Ur-interface
•	Konfigurations-interface
•	Management-interface
Kommunikations-interface
Dette interface understøtter kommunikationen mellem IEEE11073PHD comm-laget og de forskellige hardware grænseflader. Interfacet består udelukkende af muligheder for at sende beskeder frem og tilbage samt lukke forbindelser og modtage notifikationer når der kommer indkommende opkald eller forbindelser afbrydes.
Al logik omkring oprettelse af forbindelser, parring af enheder, håndtering af forskellige device typer (Bluetooth, USB, ZigBee mv.) kommunikationssikkerhed mv., hører ikke hjemme i Kommunikations-interfacet. Disse håndtag findes i stedet i Management-interfacet.
Tråd-interface
Alle platform-uafhængige moduler ”over” PAL skrives på en måde så de både vil fungere i en single-thread verden (hvor ingen kald må blokere eller udføre længerevarende opgaver) og vil fungere i en multi-thread verden (hvor der træffes passende foranstaltninger i forhold til race-conditions, re-entrante funktionskald mv.). De nødvendige håndtag til dette (mutex-låse, hoved-event-tråd til afvikling af opgaver, timere mv.) ligger i tråd-interfacet. [Mikes kommentar: Vi har valgt at køre single-trådet i beskedinterfacet til de enkelte moduler. I princippet er en timer, der leverer beskeder over beskedinterfacet tilstrækkeligt. Resten må være future-work]
Ur-interface
Dette interface udstiller platformens muligheder for at måle og angive tid – enten som et realtids ur (UTC/lokaltid, incl. tidszone, sommer/vintertid) eller det mest stabile relative ur, der kan leveres. Ur-interfacet skal også angive meta-informationer om det pågældende ur: Er det fx synkroniseret med en tidsserver eller indstillet manuelt af brugeren. (Se PCD TF-2 kapitel B.7.1-B.7.2)
Konfigurations-interface (evt. Future Work)
For hvert tilsluttet måleapparat, har IEEE11073PHD laget behov for at gemme nogle konfigurationsoplysninger på det lokale system. Konfigurations-interfacet stiller et simpelt ”dictionary” interface til rådighed, hvor sådanne informationer kan gemmes og senere hentes. Det vil give god mening at dette sker i operativsystemets eget system til håndtering af konfigurationer – og gerne på tværs af applikationer, som benytter 4SDC, således at et måleapparat, der er parret med et device opfører sig ens med alle applikationer på dette device, der benytter 4SDC. På Windows vil konfigurationsdata typisk skulle gemmes i registreringsdatabasen, på *nix systemer typisk et sted under /run eller /var.
Management-interface (Future Work)
Dette interface indeholder forskellige håndtag til håndtering af tilslutning af måleapparater: Metoder til at enumerere USB bussen, scanne for lokale kompatible Bluetooth eller ZigBee enheder og håndtere opsætning af forbindelse (parring, nøgleudveksling etc.). 

\endif

System Layer and Module Interactions     {#sec_sys_module}
====================================

\htmlonly
<h3>Proceed to <a href="page_arch_status.html">Architecture Status and Road-maps</a></h3>
\endhtmlonly

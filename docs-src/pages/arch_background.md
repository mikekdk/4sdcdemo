
Background and Basics     {#page_arch_background}
=====================

\if 0
******************************************************************************
***                                                                        ***
***     This file is part of the 4SDC documentation, and contains the      ***
***     page/sections with the architecture background.                    ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


[TOC]



\if 0


Fra gammelt notat:

Følgende tiltag kan anbefales:
Den nuværende måleapparat-integration i OpenTele skrottes fuldstændigt og erstattes af en række nye moduler, som indbyrdes følger arkitekturen og anbefalingerne fra Continua Health Alliance (CHA) tæt, og hver især internt er opbygget efter en arkitektur, som følger de enkelte standarder tæt (i særdeleshed IEEE 11073 PHD, se Appendix A). Dette vil have følgende effekter:
1.	Udvidelser med nye CHA-certificerede måleapparater bliver ’trivielle’ (en IEEE 11073-104xx standarder implementeres som et plug-in modul uden at ændre i eksisterende kode).
2.	Udvidelser med andre CHA-understøttede kommunikationsinterfaces (Zigbee, Bluetooth Low Energy) kan ligeledes hegnes ind i afgrænsede plug-in moduler.
3.	Fremtidige revisioner af standarderne og anbefalingerne fra Continua Health Alliance kan følges op med (bagudkompatible) revisioner af 4S modulerne.
4.	Når udviklere fra andre organisationer vil skulle bidrage til 4S kodebasen (med ændringer eller tilføjelser) vil det være en stor hjælp både for dem og for en auditør/code reviewer, at man uden større besvær kan gennemskue og verificere hvorledes standarden følges gennem eksplicitte krydsreferencer mellem krav i standarddokumenterne, kildekode dokumentationen og governance værktøjerne (issue tracker mv.).
5.	Skulle 4S eller nogle af firmaerne, der anvender 4S kodebasen, i fremtiden ønske en CHA-certificering og/eller en Medical Device Directive certificering må det formodes, at det også vil være et krav (eller i det mindste en stor fordel) at standarderne følges – og at dette kan dokumenteres/sandsynliggøres igennem en eksplicit dokumentation for opfyldelsen af hver detalje.
6.	Standardiserede snitflader vil gøre modulerne anvendelige i mange andre sammenhænge end OpenTele.




Fra design noter:

 - Unix Philosophy
 - Isoler specialistviden i moduler, så alle ikke skal vide alt om alt
 - Single Responsibility/loose coupling - modulafgrænsning efter kilder til opdateringer (standarder ændrer sig over tid og uafhængigt af hinanden - fx Continua pre '15 kun PCD-01 SOAP, fra 2015 også REST og i fremtiden sandsynligvis FHIR).
 - SDC-4 Define environment req. for 4SDC
 - Andre Jira spor?

 - input fra reference ark.
 - input fra CDG
 




Design af moduler
1.	Skal modulet kun bruges i forbindelse med brugerinteraktion og kan det implementeres i ren JS, så er dette førstevalget



I modsætning til server moduler, skal klientmodulerne kunne køre i et særdeles heterogent miljø, hvilket stiller særlige krav til måderne de designes på.
•	Rene Web-applikationer
•	Mobil-OS Apps (Android, iOS, Windows [Phone])
•	Tynde webklienter (ChromeOS, FirefoxOS)
•	Hovedløse set-top bokse (fx ISP router)
•	Set-top bokse til TV (fx kontrolleret af TVets fjernbetjening)
•	PC applikationer
•	Kombineret web-appikation + browser plugin

Bemærk: På servere kan man slippe af sted med mange ting med virtualisering mv. til at indkapsle legacy funktionalitet. Det kan vi ikke på klienter! Derfor skrappere krav nødvendige for at sikre kontinuitet...


\endif

Run-time Environment Requirements     {#sec_rte_req}
=================================

\if 0

I de ”klassiske” desktop operativsystemer (Windows, OS X og Linux) er man vant til, at en installeret applikation kan være skrevet i et vilkårligt programmeringssprog og kan have rettigheder til at gøre stort set hvad som helst. Dette billede har ændret sig væsentligt på de nyere operativsystemer til mobile enheder (Android, iOS, Windows Phone, Blackberry etc.). Her er der meget strengere justits i forhold til, hvad en app kan få lov til at gøre. 
Dertil kommer, at de forskellige operativsystemer har forskellige regler og forskellige modeller for, hvordan reglerne håndteres.

Helt overordnet ønsker vi, at alle moduler over ”Hardware Abstraction Layer” / ”Connection Control and Security” skal være uafhængige af operativsystem. Fra hardware-abstraktionslaget og nedefter (dvs. alle de forbindelses-specifikke moduler: USB, Bluetooth, Zigbee) vil det næppe være muligt at genbruge moduler på tværs af operativsystemerne, da de simpelthen er for forskellige på dette punkt. Derfor må man forvente, at disse moduler skal gen-implementeres for hvert nyt operativsystem, der skal understøttes.

På de mobile operativsystemer vil der på grund af de førnævnte restriktioner være nogle udfordringer i forbindelse med at kommunikere med måleapparaterne såfremt appen ikke er synlig på skærmen. Det betyder, at de moduler, der håndterer kommunikationen med måleapparaterne (hvilket er alle modulerne under DIM laget) på de mobile operativsystemer skal tildeles særlige rettigheder eller på anden vis have særbehandling, for at få lov til at køre uforstyrret. Det kan dermed også komme på tale, at enkelte delkomponenter af modulerne under DIM laget vil skulle håndteres specielt / re-implementeres på visse mobile operativsystemer.
\endif 


Language     {#sec_language}
========

\if 0
Fra første dokument:
Programmeringssprog og køretidsmiljøer
I forhold til valg af programmeringssprog og køretidsmiljø, så vil det ideelle være, om alle moduler over hardware-abstraktionslaget kunne implementeres i et enkelt miljø, som vil fungere ens på tværs af operativsystemer og platforme – ”Write-once-run-everywhere”. Dermed kan man undgå at re-implementere den samme programlogik i forskellige programmeringssprog.

En effektiv –  men desværre også dyr – løsning kunne være udviklingsmiljøet Xamarin. Her kan man udvikle apps til både Android, iOS og Windows Phone i et enkelt udviklingsmiljø. Der er dog mindst to store argumenter imod at vælge Xamarin: Det er proprietært og det er dyrt. Så hvis man bruger det, vil man for det første være afhængig af et enkelt firma og deres forretningsplaner, og for det andet vil licensomkostningerne beløbe sig til mellem US$ 1000-3000 pr. år pr. udvikler. Denne omkostning ville også virke særdeles hæmmende på tilgangen af nye udviklere, som vil bidrage til disse moduler i 4S regi, og dermed stride mod den overordnede open source filosofi. Alt i alt kan Xamarin (og lignende løsninger) derfor ikke anbefales.

En mere generelt anvendelig løsning ville være at bygge modulerne over hardware-abstraktionslaget i JavaScript sammen med et open source køretidsmiljø, der er designet til at fungere på tværs af platforme. Dette ville passe godt sammen med en overordnet migrering fra den nuværende Android/Java app til en HTML5/CSS/JavaScript-baseret OpenTele klient. Et sådant køretidsmiljø findes fx i Apache Cordova (a.k.a. PhoneGap). Ulempen ved en JavaScript-baseret løsning kunne være, at den kan være langsommere end en ”rigtig” (native) app. Men da OpenTele klienten ikke håndterer store eller intensive mængder data eller tung grafik, vil det næppe kunne mærkes på brugeroplevelsen.




Fra design noter:
C++ er valgt til de platformuafhængige moduler (hvad med brugervejledningsmoduler - skal de ikke køre JS??), da:
•	Sproget er stabilt og meget udbredt (og har bevist sit værd gennem mange år, og derfor må forventes at leve videre i mange år fremover).
•	Kan afvikles native på alle de listede platforme pånær Firefox OS (se dog næste punkt). Bemærk at native afvikling er nødvendig for at undgå uhensigtsmæssige afbrydelser.
•	Kan kompileres (vha. Emscripten http://emscripten.org) til en delmængde af JavaScript (asm.js), som derefter kan afvikles på alle browser-platforme – og dette er tilmed effektivt. Dermed får man samtidigt alle fordelene fra JavaScript med.
•	I de tilfælde, hvor man både kører en del af 4SDC i native kode og en applikation i en browser, vil man kunne vælge, hvilke komponenter, der skal køre native og hvilke, der skal afvikles i browserens JavaScript motor. Således kan komponenter, der ofte ændres, loades dynamisk fra webserveren (fx 20601 kører i C++ mens specialiseringer kører i JS, så de kan loades dynamisk fra webserveren - et noget konstrueret eksempel...).

 - SDC-1

 - SDC-37

 - System Layer komponenter håndterer sprogbarriere

 - Presentation layer blanding af C++ og JS

\endif


Module Contract     {#sec_module_contract}
===============

\if 0
==Platform-uafhængighed af runtime komponenter
For at sikre platform-uafhængighed af alle komponenter over PAL (som beskrives senere), stilles følgende krav til disse:
•	Al platform-afhængig funktionalitet skilles ud i små, simple og genbrugelige komponenter under PAL. Komponenter i PAL skal designes efter UNIX-filosofien: Implementer én simpel funktion godt.
•	Komponenter over PAL skal laves, så de kan fungere i alle mulige forskellige tråd-miljøer: Multi-trådede miljøer med thread-safe funktionskald, enkelt-trådede miljøer, hvor alle funktionskald skal være korte og ikke-blokerende (fx fordi al eksekvering kører på en GUI eventtråd): Vi har valgt at køre med enkelttrådede modul-interfaces som skal være ikke-blokerende.
Eksterne platform-uafhængige komponenter
Så vidt muligt undgås afhængigheder af eksterne komponenter med mindre særlige grunde taler for afhængigheden, og den eksterne komponent  i øvrigt overholder licenskravene listet i et tidligere afsnit.

Generelt skal der være så få eksterne afhængigheder som overhovedet muligt, og de afhængigheder, der er, skal være store/udbredte og stabile/gamle, således at de ikke pludselig forsvinder fra markedet eller ændrer sig radikalt.

Såfremt en ekstern afhængighed tages ind i 4SDC runtime, skal den understøtte alle platforme – inklusive fremtidige, og være neutral i forhold til udviklingsmiljøer / IDE / toolchain.[Mikes kommentar: uklar ("alle - incl. fremtidige", "neutral")]

Compiletime/test afhængigheder bør understøtte de almindelige desktop platforme, som må formodes at blive brugt til udvikling (for tiden Win, Linux og OS X). De må ikke være låst til bestemte valg af IDE eller udviklingsmiljøer / toolchains, men skal fungere på tværs af disse (fx Gnu Make/Gcc, Visual Studio, Eclipse, Xcode). Et demo eller tutorial projekt må dog godt stille krav om et bestemt IDE / udviklingsmiljø.

==Platform-afhængige komponenter
Platform-afhængige komponenter må kun forefindes i lagene under PAL (se senere). Her kan der undtagelsesvis være behov for at fravige fra ovenstående retningslinjer omkring licens, sprog og eksterne komponenter – og dette kan så ske, såfremt gode argumenter taler for dette. Komponenterne i lagene under PAL har i øvrigt også lov til at stille særlige krav til udviklingsmiljø/toolchain. Fx kan iOS komponenter forventes at kræve, at Xcode benyttes til at bygge applikationen, mens Windows Phone komponenter kan forventes at kræve Visual Studio.




Its only interaction with other modules is through the
message system. It is created, handles and generates messages, and is
eventually destroyed on one single thread. (A platform-dependent
module may internally use multiple threads, but the public interface
must adhere to this single-thread requirement.)
\endif


External Dependencies     {#sec_external_deps}
=====================

\if 0
= Brug af eksterne biblioteker =
•	4SDC runtime moduler skal kunne frigives under Apache 2 licens
•	4SDC runtime må kun afhænge af et minimalt antal eksterne komponenter, som alle skal benytte licenser kompatible med Apache 2. 
•	4SDC compiletime/test/demo komponenter kan afhænge af eksterne komponenter med mere restriktive licensbestemmelser (fx GPL)
\endif


\htmlonly
<h3>Proceed to <a href="page_arch_details.html">The 4SDC Architecture</a></h3>
\endhtmlonly

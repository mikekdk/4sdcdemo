#ifndef TST_MSGADDR_H
#define TST_MSGADDR_H

#include <QString>
#include <QtTest>
#include <QObject>

class tst_msgaddr : public QObject
{
  Q_OBJECT
public:
  explicit tst_msgaddr( void );
  ~tst_msgaddr( void );

private Q_SLOTS:

  void compare( void );
};

#endif // TST_MSGADDR_H

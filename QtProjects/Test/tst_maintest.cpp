#include <iostream>
#include <QString>
#include <QtTest>

#include "tst_handle.h"
#include "tst_module.h"
#include "tst_msgaddr.h"
#include "tst_msgsinglethread.h"
#include "tst_msgmultithread.h"


class MainTest : public QObject
{
  Q_OBJECT

public:
  MainTest();

private Q_SLOTS:
  void testHandle();
};

MainTest::MainTest()
{
}

void MainTest::testHandle()
{
//  tst_handle obj;
//  QVERIFY2(QTest::qExec(&obj), "Test of Handle module failed");

}

//TEST_APPLESS_MAIN(MainTest)

#include "tst_maintest.moc"


#define TEST(cLASS) {cLASS obj;returnValue+=QTest::qExec(&obj, argc, argv);}

// Dummy function needed by 4SDC (until it starts using the event interface
void sendEvent(std::string) {};

int main(int argc , char *argv [])
{
  std::cout << "Running all tests" << std::endl;

  int returnValue = 0;

//////////////////// LIST OF CLASSES THAT SHOULD BE TESTED /////////////////////

  TEST(MainTest);
  TEST(tst_handle);
  TEST(tst_msgaddr);
  TEST(tst_msgSingleThread);
  TEST(tst_msgMultiThread);
  TEST(tst_module);

////////////////////////////////////////////////////////////////////////////////


  if(0 == returnValue)
  {
    std::cout << "HAPPY!!!!" << std::endl;
  }
  else
  {
    std::cout << "                                               " << std::endl;
    std::cout << "     FFFFFF  AAA   IIII  LL     !!  !!  !!     " << std::endl;
    std::cout << "     FF    AA   AA  II   LL     !!  !!  !!     " << std::endl;
    std::cout << "     FFFF  AAAAAAA  II   LL     !!  !!  !!     " << std::endl;
    std::cout << "     FF    AA   AA  II   LL                    " << std::endl;
    std::cout << "     FF    AA   AA IIII  LLLLLL !!  !!  !!     " << std::endl;
    std::cout << "                                               " << std::endl;
  }

  return returnValue;
}

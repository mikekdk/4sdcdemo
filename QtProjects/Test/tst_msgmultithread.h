#ifndef TST_MSGMULTITHREAD_H
#define TST_MSGMULTITHREAD_H

#include <QString>
#include <QtTest>
#include <QObject>

class tst_msgMultiThread : public QObject
{
  Q_OBJECT
public:
  tst_msgMultiThread( void );
  ~tst_msgMultiThread( void );

private Q_SLOTS:
  void twoThreads( void );
};

#endif // TST_MSGMULTITHREAD_H

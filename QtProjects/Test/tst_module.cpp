#include "tst_module.h"
#include "moduledeclaration.h"

#include "FSYS/moduledeclare.h"
#include "FSYS/modulerunner.h"
#include "FSYS/modulethreader.h"
#include "FSYS/msgsender.h"

#include <map>
#include <thread>

std::map<State, int> stateList;
bool stateSwitchHappy;


tst_module::tst_module(QObject *parent) : QObject(parent)
{

}

tst_module::~tst_module()
{

}

void tst_module::init( void )
{
  // Make the state switch engine happy
  stateSwitchHappy = true;

  // Reset all counters
  stateList[State::INITIAL] = 0;
  stateList[State::READY] = 0;
  stateList[State::GROUP_READY] = 0;
  stateList[State::SYSTEM_READY] = 0;
  stateList[State::GOING_DOWN] = 0;
  stateList[State::DESTROYED] = 0;
}

void tst_module::cleanup( void )
{

}


void tst_module::checkGroupInThread( void )
{


}

void tst_module::checkRunnerInit( void )
{
  // Define modules to test on
  FSYS::ModulePrivate::Runner runner("Test checkRunnerInit",
                                     {new FSYS::ModulePrivate::Launcher<ModuleA>,
                                      new FSYS::ModulePrivate::Launcher<ModuleA>,
                                      new FSYS::ModulePrivate::Launcher<ModuleB>});
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
  runner.startStaticModules();
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
  // Check we have two ModuleA's in the group ready state
  QVERIFY2(stateList[State::GROUP_READY] == 2, "Static modules didn't switch "
                                               "state as expected");
  // Fake some messages
  FSYS::MsgSystemReady ready;
  runner.receive(ready);
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
  QVERIFY2(stateList[State::SYSTEM_READY] == 2, "Static modules didn't switch "
                                                "state as expected");
}

void tst_module::checkRunnerMsgLoop( void )
{
  // Dummy class for broadcasting
  class Sender : public FSYS::MsgSender<Sender> {} sender;

  // Define modules to test on
  FSYS::ModulePrivate::Runner runner("Test checkRunnerInit",
                                     {new FSYS::ModulePrivate::Launcher<ModuleA>,
                                      new FSYS::ModulePrivate::Launcher<ModuleA>,
                                      new FSYS::ModulePrivate::Launcher<ModuleB>,
                                      new FSYS::ModulePrivate::Launcher<ModuleA>});
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
  // Start the static modules
  runner.startStaticModules();

  // Simulate the rest of the system, by sending messages
  FSYS::MsgSystemReady systemReady;
  FSYS::MsgGoingDown goingDown;
  FSYS::MsgDestroy destroy;
  sender.broadcast(systemReady);
  sender.broadcast(goingDown);
  sender.broadcast(destroy);

  // Now run the message loop, it should exit again, as we have posted a
  // destroy message on the queue
  runner.runMsgLoop();

  // Check that we now have three modules in the terminated state
  QVERIFY2(stateList[State::DESTROYED] == 3, "One or more modules didn't "
                                             "reach the destroyed state");
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
}

void tst_module::checkThreadSystem( void )
{
  FSYS::ModulePrivate::ModuleThreader threadSystem;

  class WaitForSystemReady : public FSYS::MsgReceiver<WaitForSystemReady,
                                     FSYS::MsgSystemReady>
  {
  private:
    bool waitComplete = {false};
  public:
    bool wait( void )
    {
      int maxRun=100;
      while(!waitComplete && 0 < maxRun--)
      {
        // Empty message queue
        FSYS::MsgQueue::emptyMsgQueue(10);
        // Let other threads run
        std::this_thread::yield();
      }

      return waitComplete;
    }

    void receive(FSYS::MsgSystemReady)
    {
      waitComplete = true;
    }

  } waitForSystemReady;

  // Bring the thread system to a known state
  threadSystem.terminateAll();
  FSYS::MsgQueue::emptyMsgQueue();

  // Start all threads
  threadSystem.spawnAll();

  // Wait for them to start
  QVERIFY2(waitForSystemReady.wait(), "Modules didn't start in due time");

  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
  // Check we have two ModuleA's in the group ready state
  QVERIFY2(stateList[State::SYSTEM_READY] == 1, "Static modules didn't switch "
                                                "state as expected");

  threadSystem.terminateAll();

  QVERIFY2(stateList[State::DESTROYED] == 1, "One or more modules didn't "
                                             "reach the destroyed state");
  QVERIFY2(stateSwitchHappy, "Some illegal stateswitch happend");
}

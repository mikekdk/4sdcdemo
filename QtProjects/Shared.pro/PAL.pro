################################################################################
#                                                                              #
#   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)                   #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
#                                                                              #
################################################################################
#                                                                              #
#   Contributer list:                                                          #
#                                                                              #
#     Jacob Andersen, The Alexandra Institute <jacob.andersen@alexandra.dk>    #
#     Mike Kristoffersen, The Alexandra Institute <4sdc@mikek.dk>              #
#                                                                              #
################################################################################

################################################################################
# Files for PAL layer
################################################################################

#-------------------------------------------------------------------------------
# Public interface
#-------------------------------------------------------------------------------
HEADERS += \
    ../../Interface/PAL/personalhealthdevice.h \
    ../../Interface/PAL/bluetooth.h


#-------------------------------------------------------------------------------
# Private implementation
#-------------------------------------------------------------------------------
SOURCES += \
    ../../Implementation/PAL/Common/personalhealthdevice.cpp \


# Android specifics for the PAL layer
#_______________________________________________________________________________
android {
QT += androidextras

HEADERS += \
    ../../Interface/PAL/android.h \

SOURCES += \
    ../../Implementation/PAL/Android/android.cpp \
    ../../Implementation/PAL/Android/bluedroid.cpp

OTHER_FILES += \
    ../../Implementation/PAL/Android/java/dk/_4s_online/device_communication/PAL/Android/Bluedroid.java \
    ../../Implementation/PAL/Android/java/dk/_4s_online/device_communication/PAL/Android/GetClassLoader.java

}

# Linux specifics for the PAL layer (not including Android and iOS)
#_______________________________________________________________________________
linux:!android:!iOS {
QT += dbus

HEADERS += \
    ../../Implementation/PAL/Linux/bluez.h

SOURCES += \
    ../../Implementation/PAL/Linux/bluez.cpp
}


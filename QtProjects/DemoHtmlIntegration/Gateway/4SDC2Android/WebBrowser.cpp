/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the %Android webbrowser
 *
 * /see Gateway::WebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "Gateway/webbrowser.h"
#include "Gateway/jsongateway.h"
#include "FSYS/log.h"
#include "platformwebbrowser.h"
#include <mutex>
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtGlobal>

DECLARE_LOG_MODULE("4SDC2Android")

/*
 * JNI Data type table
 *
 * Native Type     Java Language Type  Description        Type signature
 * unsigned char   jboolean            unsigned 8 bits    Z
 * signed char     jbyte               signed 8 bits      B
 * unsigned short  jchar               unsigned 16 bits   C
 * short           jshort              signed 16 bits     S
 * long            jint                signed 32 bits     I
 * long long       jlong               signed 64 bits     J
 * float           jfloat              32 bits            F
 * double          jdouble             64 bits            D
 * void                                                   V
*/


Gateway::WebBrowser::WebBrowser( void )
{
  // Create the platfrom browser
  realBrowser = new PlatformWebBrowser;

  // Tell the JSON gateway that there is a browser available
  JSONGateway::setBrowser(this);
}

Gateway::WebBrowser::~WebBrowser()
{
  // Tell the JSON gateway that the browser has gone
  JSONGateway::setBrowser(nullptr);

  // Delete the platform browser
  delete realBrowser;
  realBrowser = NULL;
}

void Gateway::WebBrowser::launch(std::string const URL)
{
  Q_ASSERT(realBrowser);
  realBrowser->createBrowserWindow(URL);
}

bool Gateway::WebBrowser::runJSInBrowser(std::string const jsString)
{
  Q_ASSERT(realBrowser);
  return realBrowser->runJSInBrowser(jsString);
}

bool Gateway::WebBrowser::platformSupportsSendingLoadFails( void )
{
  return false;
}

void Gateway::WebBrowser::terminate()
{

}

bool Gateway::WebBrowser::platformSupportsTerminateFunction( void )
{
  return false;
}


/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the PlatformWebBrowser class for Qt Webkit
 *
 * The Qt Webkit is the prefered way to open a browser, we only use something
 * else on platforms that don't support Qt Webkit (like Android)
 *
 * /see PlatformWebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef PLATFORMWEBBROWSER_H
#define PLATFORMWEBBROWSER_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <string>

//class QQuickView;
class WebViewGateway;
class QWebView;
class QWebPage;
class QWebFrame;


/**
 * @brief This PlatformWebBrowser class is the Qt Webkit specific browser abstraction
 *
 */
class PlatformWebBrowser : public QObject
{
  Q_OBJECT

private:
  //QQuickView *view;
  /**
   * @brief The URL currently being displayed
   */
  QString url;
  /**
   * @brief Pointer to the WebViewGateway
   *
   * The WebViewGateway class is the class that the JS in the browser interacts
   * with (The fsdc object).
   */
  WebViewGateway *webViewGateway;

  /**
   * @brief The last created QWebView
   */
  QWebView *view;
  /**
   * @brief The last created QWebPage
   */
  QWebPage *myWebPage;
  /**
   * @brief The last created QWebFrame
   */
  QWebFrame *frame;

signals:
  /**
   * @brief Not implemented
   */
  void urlChanged(void);

  // Signal to evaluate Java script
  QVariant evaluateJavaScript(const QString & scriptSource);

public slots:
  /**
   * @brief Called by the QWebFrame when new content is loaded
   *
   * This slot is used for re-initializing the framework for the next page, the
   * default behavior for QT WebKit is to reset the browser context when a new
   * page is loaded.
   */
  void javaScriptWindowObjectCleared(void);

public:
  Q_PROPERTY( QString url MEMBER url NOTIFY urlChanged );

  explicit PlatformWebBrowser(QObject *parent = 0);

  /**
   * @brief Function to call to open a browser window
   *
   * Calling this function will open a new webkit window, and load the URL given
   * in the call in the browser.
   *
   * @warning It is untested behavior to call this function more than once on
   * the same object.
   *
   * @param url The URL of the page that should be loaded.
   */
  void createBrowserWindow(QString url);

  /**
   * @brief Run JS code in the current browser window
   *
   * Calling this function will atempt to run the java code given in the
   * jsString argument in the browser.
   *
   * @param jsString The java code that should be run
   *
   * @return true If the code was attempted run in the browser
   *
   * @return false If the code was not attempted run in the browser
   *
   */
  bool runJSInBrowser(std::string const jsString);

  /**
   * @brief Not implemented
   */
  Q_INVOKABLE void launchBrowser( void );

};

#endif // PLATFORMWEBBROWSER_H


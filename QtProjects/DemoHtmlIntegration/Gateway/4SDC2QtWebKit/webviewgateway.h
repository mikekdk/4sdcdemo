/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the WebViewGateway class for Qt Webkit
 *
 * The Qt Webkit is the prefered way to open a browser, we only use something
 * else on platforms that don't support Qt Webkit (like Android)
 *
 * The WebViewGateway is the class that is exposed in the JS running in the
 * Webkit browser.
 *
 * /see PlatformWebBrowser
 * /see WebViewGateway
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef WEBVIEWGATEWAY_H
#define WEBVIEWGATEWAY_H

#include <QObject>

class WebViewGateway : public QObject
{
  Q_OBJECT
public:
  explicit WebViewGateway(QObject *parent = 0);

  Q_INVOKABLE QString copyright(void);
  Q_INVOKABLE QString version(void);
  Q_INVOKABLE void requestMeasurement(QString reqType, QString jsCallBackName);

signals:

public slots:

};

#endif // WEBVIEWGATEWAY_H

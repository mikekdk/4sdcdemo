/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Header file for the Gateway::JSONGateway interface
 *
 * /see Gateway::WebBrowser
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef JSONGATEWAY_H
#define JSONGATEWAY_H

#include<string>

namespace Gateway
{

class WebBrowser;
class PlatformJSONGateway;

/**
 * @brief Class to do the actuall mapping between the JS/JSON world and 4SDC
 *
 * This class uses the web browser class to communicate with the JS code that
 * runs in the browser.  An instance of JSONGateway is created for each request
 * type that is made from the JS run in the browser.
 *
 * The object will be destroyed, when the callback is cleared from JS or when
 * the page is reloaded
 */
class JSONGateway
{
private:

public:
  static PlatformJSONGateway *getGateway(void);

  /**
   * @brief Set browser
   *
   * The JSON Gateway is what can send JSON back into a browser
   *
   * @param browser The browser that should receive the JSON calls
   */  
  static void setBrowser(WebBrowser *browser);

  /**
   * @brief Function that returns the current copyrigth string
   *
   * Use this function to retrieve the current copyright string for 4SDC.
   *
   * @return The current copyright string
   */
  static std::string copyright(void);

  /**
   * @brief Function that returns the current version string
   *
   * Use this function to retrieve the current version string for 4SDC.
   *
   * @return The current version string
   */
  static std::string version(void);

  /**
   * @brief Function that handles a requestMeasurement call from JS
   *
   * The allowed types of measurments understood by this function are:
   *
   * "BloodPressure" - will attempt to take the blood pressure
   *
   * @param MeasurementType The type of measurement that should be taken
   *
   * @param callBackFunction  The JS function that should be called with a
   *                          JSON object with the result
   */
  static void requestMeasurement(std::string measurementType, std::string callBackFunction);
};

}
#endif // JSONGATEWAY_H

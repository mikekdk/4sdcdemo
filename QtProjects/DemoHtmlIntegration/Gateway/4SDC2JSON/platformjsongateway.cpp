/*
 *   Copyright 2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the Gateway::PlatformJSONGateway class
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "platformjsongateway.h"
#include "assert.h"
#include "4SDC/core.h"
#include "FSYS/log.h"

#include <cstdint>
#include <map>

DECLARE_LOG_MODULE("PlatformJSONGateway");

uint16_t Gateway::PlatformJSONGateway::idFromTypeString(std::string typeString)
{
  // Define the mapping between strings and values
  static const std::map<std::string, uint16_t> map = {
      /* JS name             , value */
      {"BloodPressure"       , 0x1007},
      {"Weight"              , 0x100F}};

  // Check if the typeString is found in the map
  auto it = map.find(typeString);

  // If it is not found, return 0
  if(map.end() == it)
  {
    return 0;
  }

  // If it was found, return the number
  return it->second;
}

void Gateway::PlatformJSONGateway::setBrowser(Gateway::WebBrowser *browser)
{
  this->browser = browser;
}

Gateway::WebBrowser *Gateway::PlatformJSONGateway::getBrowser(void)
{
  // Check that the browser object isn't null
  assert(browser);
  return browser;
}

void Gateway::PlatformJSONGateway::requestMeasurement(
                                                  std::string measurementType,
                                                  std::string callBackFunction)
{
  // Check if we can map the type string to a value
  auto type = idFromTypeString(measurementType);

  if(0 == type)
  {
    WARN("Measurement requested with illegal/unknown measurement type name")

    // If we have a valid callback func
    auto browser = getBrowser();
    assert(browser);      // Ensure that there is a browser (who called us)

    // If not the empty string
    if(callBackFunction.compare(""))
    {
      browser->runJSInBrowser(callBackFunction + "(\"" + measurementType
                              + " is not supported\")");
    }
    else
    {
      WARN("Measurement requested with illegal callback function name")
    }
    return;
  }

  // We now have a valid type

  // Fetch the old callback
  auto oldCallBackString = callBackMap[type];

  // If we didn't have a subscriber, but now have
  if(!oldCallBackString.compare("") && callBackFunction.compare(""))
  {
    // Send the subscribe message
    fsdc::Core::RegisterDataType registerDataType;
    registerDataType.dataType = type;
    broadcast(registerDataType);
  } else

  // If we had a subscriber, but now don't
  if(oldCallBackString.compare("") && !callBackFunction.compare(""))
  {
    // Send the unsubscribe message
    fsdc::Core::UnRegisterDataType unRegisterDataType;
    unRegisterDataType.dataType = type;
    broadcast(unRegisterDataType);
  }

  // In any case we can store the call back parameter in the entry
  callBackMap[type] = callBackFunction;
}

void Gateway::PlatformJSONGateway::reset( void )
{
  // Loop through all registrations and unregister
  for(auto element: callBackMap)
  {
    if(element.second.compare(""))
    {
      fsdc::Core::UnRegisterDataType unRegisterDataType;
      unRegisterDataType.dataType = element.first;
      broadcast(unRegisterDataType);
      callBackMap[element.first] = "";
    }
  }
}

void Gateway::PlatformJSONGateway::receive(fsdc::Core::DataAvailable data)
{
  // Check if we have a callback function to the data
  std::string callBackFunction = callBackMap[data.dataType];

  if(callBackFunction.compare(""))
  {
    getBrowser()->runJSInBrowser(callBackFunction + "(" + data.data + ")");
  }
}

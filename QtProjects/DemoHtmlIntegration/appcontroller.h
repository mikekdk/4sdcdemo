/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Include file for the AppController class
 *
 * Contains definitions for the AppController class
 *
 * /see AppController
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include "FSYS/modulethreader.h"

#include <QObject>
#include <QString>

// Forward declare some classes so we don't need to include their include files
// in every file using this class
class QApplication;
class QQmlApplicationEngine;
class QQmlContext;
//class State;

/**
 * @brief The AppController class controls the QT application
 *
 * You can either inherit from this class and extend it, or you can use it
 * directly by using the static functions on it.
 *
 * If you choose to extend it with your own class then it is VERY important
 * that you DO NOT instantiate it as a global (as in program global or
 * translation unit global), it is fine to have it as a static member of a
 * function or class as long as it isn't instantiated before main is entered.
 *
 * A simple app can be launched as follows:
 *
 * @code
 * #include <appcontroller.h>
 *
 * int main(int argc, char *argv[])
 * {
 *   return AppController::LaunchApp(arc, argv, "qrc:/main.qml");
 * }
 * @endcode
 */
class AppController : public QObject
{
private:
  Q_OBJECT

  /**
   * @brief Object that controls the setup and execution of the thread system
   *
   * This module takes care of all the C++ threads
   */
  static FSYS::ModulePrivate::ModuleThreader moduleThreader;

  /**
   * @brief Storing the QApplication object
   */
  QApplication *app;

  /**
   * @brief Storing the QQmlApplicationEngine
   */
  QQmlApplicationEngine *engine;

  /**
   * @brief Contains a pointer to the global instance of the AppController object
   */
  static AppController *me;

  /**
   * @brief Function to retrieve a reference to the global AppController object
   *
   * @return The value "exit()" was called with from the application
   */
  static AppController& get( void );


  /**
   * @brief Function to run event loop(s)
   *
   * Calling this function will run the Qt and 4S event loops, until exit is
   * called.
   *
   * It will return the value returned by the Qt event loop.
   */
  static int runEventLoop( void );

protected:

  /**
   * @brief Virtual function allowing child classes to register their own classes
   *
   * Overwrite this function to register objects that should be accessible from
   * QML.
   *
   */
  virtual void registerObjects(void);

public:

  /**
   * @brief AppController constructor
   *
   * The AppController constructor
   *
   * @param parent The QObject parent of this object
   */
  explicit AppController(QObject *parent = 0);


  ~AppController();

  /**
   * @brief Call this function to launch the app in the Qt system
   *
   * Calling this function will launch the Qt application by loading the
   * resource file given in the argument to the function.
   *
   * @warning It is a fatal error to call this function recursively.
   *
   * @todo Write how new qml files are loaded dynamically.
   *
   * @param argc The number of arguments transfered in argv
   *
   * @param argv The argument list from launching the application
   *
   * @param urlToLoad The URL that should be loaded (the name of the qml file)
   *
   * @return The value exit was called with to terminate the program
   */
  static int launchApp( int argc, char *argv[], QUrl urlToLoad );

  /**
   * @brief Convinience function for launching the app in the Qt system
   *
   * This function is a convinience function allowing to specify the url for
   * the main qml file to be specified as a QString or char array
   *
   * @sa launchApp(int, char*[], QUrl)
   */
  static int launchApp( int argc, char *argv[], QString urlToLoad );

  /**
   * @brief Function to retrieve the qml context of the main window
   *
   * Call this function after launchApp has been called to retrieve the
   * qmlContext.  This function is made static, so you don't need to have
   * a referenfce to the object in order to use it.
   *
   * @return The main window's qmlContext, or null if there is no main window
   */
  static QQmlContext * qmlContext( void );

signals:

public slots:
  /**
   * @brief Timeout function to drive the message loop
   *
   * This function is used to drive the 4S event loop
   */
  void loop4SEventLoop( void );

};

#endif // APPCONTROLLER_H

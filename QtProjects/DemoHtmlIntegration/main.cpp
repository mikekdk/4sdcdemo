/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief File that contains the main function of the Demo application
 *
 * This file contains the main function (like in C/C++ int main(int argc, char
 * *argv) ) of the program, and the declaration of the AppController derived
 * class.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
//#include <QGuiApplication>
//#include <QQmlApplicationEngine>

#include "appcontroller.h"
#include "state.h"
#include "FSYS/log.h"

DECLARE_LOG_MODULE("MainModule");

class DemoApp : public AppController
{
private:
  State state;

public:

  void registerObjects(void)
  {
    // Register the state object, so the qml code can call back into the C++ code
    state.registerInContext(qmlContext());
  }
};

int main(int argc, char *argv[])
{
  // Write to log
  INFO("Launching qt app");

  // Load the main.qml file as the main page for the application
  DemoApp demoApp;
  return demoApp.launchApp(argc, argv, "qrc:/main.qml");
}

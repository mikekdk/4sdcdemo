#ifndef MODULECONFIG_H
#define MODULECONFIG_H

#include "4SDC/simpledemowrapper.h"
#include "PAL/bluetooth.h"

#endif // MODULECONFIG_H

MODULE_GROUP("SessionLayer")
  MODULE_STATIC(SimpleDemoWrapper2)

MODULE_GROUP_END

MODULE_GROUP("PALLayer")
  MODULE_STATIC(PAL::BluetoothModule)
MODULE_GROUP_END

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief QML file containing the settingsWindow view of the Demo application
 *
 * This file contains the settings view of the Demo application described in QML
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.1

Rectangle {
    id: window1

    color: "#FFFFFF" //"#000000"
    anchors.fill: parent
    visible: !settingsWindow.visible

    Image {
        id: image4sLogo
        //width: (5*parent.width)/100
        width: parent.width/4
        anchors.rightMargin: 0
        //anchors.bottomMargin: (5*parent.width)/100
        //anchors.leftMargin: (5*parent.width)/100
        anchors.topMargin: 0
        anchors.top: parent.top
        anchors.right: parent.right
        //anchors.left: parent.left
        fillMode: Image.PreserveAspectFit
        source: "///gfx/gfx/4S-grafik-artwork-stor.png"
    }

    Image {
        id: alexandraLogo
        height: 40
        //width: (5*parent.width)/100
        // width: parent.width/2
        // anchors.rightMargin: 0
        //anchors.bottomMargin: (5*parent.width)/100
        //anchors.leftMargin: (5*parent.width)/100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 0
        anchors.bottom: textBuildBy.top
        //anchors.top: parent.top
        //anchors.right: parent.right
        //anchors.left: parent.left
        fillMode: Image.PreserveAspectFit
        source: "///gfx/gfx/AI_logo_ORANGE_DK.png"
        transform: Rotation {
            id: logoAnimation
            origin.x: alexandraLogo.width/2
            origin.y: 30
            axis {
                x: 0
                y: 1
                z: 0
            }
            angle: 0

        }

    }

    Text {
        id: textBuildBy
        //y: 43
        text: qsTr("Application build by the Alexandra Institute")

        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 10
        //anchors.right: alexandraLogo.left
        //anchors.left: alexandraLogo.left;
        //anchors.top: parent.top;
        //anchors.leftMargin: 8
        anchors.bottom: textCopyrightBy.top
        anchors.horizontalCenter: parent.horizontalCenter
        //anchors.bottom: parent.bottom
        //font.pixelSize: 12
        color: "#000000" //"#11e305"
    }


    SequentialAnimation {
        loops: Animation.Infinite
        running: true

        PauseAnimation { duration: 2000 }

        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: 0
                to: 90
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#000000"
                to: "#FFFFFF"
                duration: 2000
            }

        }

        ScriptAction {
            script: {
                alexandraLogo.source = "///gfx/gfx/4SDC_icon.png"
                textBuildBy.text = "Device communications powered by 4SDC"
            }
        }

        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: -90
                to: 0
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#FFFFFF"
                to: "#000000"
                duration: 2000
            }
        }
        PauseAnimation { duration: 2000 }
        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: 0
                to: 90
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#000000"
                to: "#FFFFFF"
                duration: 2000
            }

        }

        ScriptAction {
            script: {
                alexandraLogo.source = "///gfx/gfx/4s-illustration.png"
                textBuildBy.text = "4SDC Copyright (c) 4S 2014"
            }
        }

        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: -90
                to: 0
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#FFFFFF"
                to: "#000000"
                duration: 2000
            }
        }
        PauseAnimation { duration: 2000 }
        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: 0
                to: 90
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#000000"
                to: "#FFFFFF"
                duration: 2000
            }

        }

        ScriptAction {
            script: {
                alexandraLogo.source = "///gfx/gfx/AI_logo_ORANGE_DK.png"
                textBuildBy.text = "Application build by the Alexandra Institute"
            }
        }

        ParallelAnimation {
            NumberAnimation {
                target: logoAnimation
                property: "angle"
                from: -90
                to: 0
                duration: 2000
            }
            ColorAnimation {
                target: textBuildBy
                property: "color"
                from: "#FFFFFF"
                to: "#000000"
                duration: 2000
            }
        }
    }

    Text {
        id: textCopyrightBy
        //y: 43
        text: qsTr("Copyright (C) Alexandra Institutet Ltd. 2014\n")

        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 10
        //anchors.right: alexandraLogo.left
        //anchors.left: alexandraLogo.left;
        //anchors.top: parent.top;
        //anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        //anchors.bottom: parent.bottom
        //font.pixelSize: 12
        color: "#000000" //"#11e305"
    }


    Text {
        id: text1
        y: 43
        text: qsTr("Enter URL to load:")
        anchors.top: image4sLogo.bottom
        anchors.left: parent.left
        anchors.leftMargin: 8
        //font.pixelSize: 12
        color: "#000000" //"#11e305"
    }

    FocusScope {
        id: focusScope;
        width: parent.width;
        anchors.top: text1.bottom
        height: textEdit1.font.pixelSize * 2
        anchors.leftMargin: 8
        anchors.rightMargin: 8

        Rectangle {
            id: background;
            anchors.fill: parent;
            color: "#AAAAAA";
            radius: 5;
            antialiasing: true;
            border {
                width: 3;
                color: (focusScope.activeFocus ? "red" : "steelblue");
            }
        }
        TextInput {
            y: 64
            id: textEdit1

            //anchors.fill: parent
            text: cpp_state.url
            //font.pixelSize: 12
            color: "#000000" //"#11e305"
            //focus: true
            width: parent.width-16
            selectByMouse: true

            anchors.centerIn: parent
            //anchors.fill: parent;

            Binding {
                    target: cpp_state
                    property: "url"
                    value: textEdit1.text
                }
        }
    }


    Button {
        id: button1
        y: 325
        text: qsTr("Accept")
        isDefault: true
        anchors.top: focusScope.bottom
        //anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.right: parent.right
        anchors.rightMargin: 8

        onClicked: {
            window1.visible = false;
        }
    }



}



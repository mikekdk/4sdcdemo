/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Java adaptation for the Android PAL bluetooth implementation
 *        using the Bluedroid stack.
 *
 * This file implements a thin adaptation layer between the
 * bluedroid.cpp Android PAL bluetooth implementation and the Android
 * bluetooth stack.
 * 
 * Currently, only Bluetooth classic is supported, but in the future,
 * also Bluetooth LE (a.k.a. SMART) support should be covered by this
 * class.
 *
 * @see bluedroid.cpp
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob Andersen</a>,
 *         The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

package dk._4s_online.device_communication.PAL.Android;

import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.os.ParcelFileDescriptor;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHealth;
import android.bluetooth.BluetoothHealthAppConfiguration;
import android.bluetooth.BluetoothHealthCallback;
import android.bluetooth.BluetoothProfile;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;



/**
 * @brief Bluedroid bluetooth stack adaptation for the Android PAL.
 *
 * This class facilitates the communication between the C++ based
 * Android PAL module and the Java based Bluedroid bluetooth stack in
 * Android via JNI.
 */
public class Bluedroid {


    // FIXME: Errorcodes used in this module:
    private final int ERROR_UNREGISTERED_DATATYPE_DURING_OPEN_CONNECTION = 42;
    private final int ERROR_BLUETOOTH_TURNED_OFF_DURING_OPEN_CONNECTION = 24;
    private final int ERROR_SENDING_DATA_TO_BLUETOOTH_FAILED = 57;
    private final int ERROR_RECEIVING_DATA_FROM_BLUETOOTH_FAILED = 47;



    // android.util.Log identification of messages from this class
    private static final String TAG = "4SDC-Bluedroid";

    // The Android Context which will offer us Bluetooth access privileges
    private Context context;

    // The currently active HealthApp objects created by this Bluedroid instance
    // Access to this map is guarded by bluetoothHealthLock
    private HashMap<Integer,HealthApp> healthAppMap =
                                                new HashMap<Integer,HealthApp>();



    // We only want one instance of this, so we keep it static
    private static BluetoothHealth bluetoothHealth = null;
    // Have we registrered the serviceListener managing bluetoothHealth?
    private static boolean serviceListenerRegistered = false;
    // All currently active HealthApp objects (by all Bluedroid instances)
    private static HashSet<HealthApp> healthAppSet = new HashSet<HealthApp>();
    // Lock for multi-threaded (and multi Bluedroid-object) access to
    // bluetoothHealth, healthAppSet, and serviceListenerRegistered
    private static final Object bluetoothHealthLock = new Object();


    public Bluedroid(Context context) {
        Log.d(TAG, "Bluedroid object created");

        this.context = context;
    }

    @Override
    protected void finalize() {
        Log.d(TAG, "Bluedroid object destroyed");
    }

    /**
     * Run the subsystem dispatcher loop.
     *
     * This will execute the dispatcher loop. This method is invoked
     * on the same thread that created this object.
     */
    public void run() {
        try {
            // Check for context
            if (context == null) {
                Log.w(TAG,"No active Android Context");
                return;
            }

            // Check for Bluetooth availability on the Android platform.
            BluetoothAdapter bluetoothAdapter =
                                            BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter == null) {
                Log.w(TAG,"Bluetooth not available on this platform");
                return;
            }

            try {
                if (!bluetoothAdapter.isEnabled()) {
                    // FIXME: We should have some kind of global flag
                    // to tell us whether we should ask the user to
                    // turn on Bluetooth or just ignore the fact that
                    // Bluetooth is disabled (in the latter case just
                    // log+skip)
                    if (true) {
                        // Start an activity that will use an Android
                        // OS provided popup to ask the user to turn
                        // on the Bluetooth adapter
                        context.startActivity(new Intent(
                                        BluetoothAdapter.ACTION_REQUEST_ENABLE));
                    } else {
                        Log.i(TAG,"Bluetooth adapter not enabled");
                    }
                }
                synchronized (bluetoothHealthLock) {
                    if (!serviceListenerRegistered) {
                        if (bluetoothAdapter.getProfileProxy(context, serviceListener,
                                                             BluetoothProfile.HEALTH)) {
                            serviceListenerRegistered = true;
                        } else {
                            Log.w(TAG, "Bluetooth HEALTH profile not available");
                            return;
                        }
                    }
                }
            } catch (SecurityException e) {
                Log.w(TAG,"The application has no BLUETOOTH permissions");
                return;
            }

            // We are successfully up and running, listening for
            // incoming connections

            Log.d(TAG,"Subsystem started");

            // Run dispatcher
            dispatchUpstream();

            Log.d(TAG, "Subsystem stopped");

        } catch (RuntimeException e) {
            // Should never happen...
            // In case of unexpected exceptions print the stacktrace for debugging.
            e.printStackTrace();
            throw e;
        }
    }


    private static final BluetoothProfile.ServiceListener serviceListener
        = new BluetoothProfile.ServiceListener() {
            public void onServiceConnected(int profile, BluetoothProfile proxy) {
                if (profile == BluetoothProfile.HEALTH) {
                    Log.d(TAG, "onServiceConnected to profile HEALTH");
                    synchronized (bluetoothHealthLock) {
                        bluetoothHealth = (BluetoothHealth) proxy;
                        for (HealthApp ha : healthAppSet) {
                            ha.onServiceConnected();
                        }
                    }
                }
            }

            public void onServiceDisconnected(int profile) {
                if (profile == BluetoothProfile.HEALTH) {
                    Log.d(TAG, "onServiceDisconnected from profile HEALTH");
                    synchronized (bluetoothHealthLock) {
                        bluetoothHealth = null;
                        for (HealthApp ha : healthAppSet) {
                            ha.onServiceDisconnected();
                        }
                    }
                }
            }
        };



    public void interruptRequest() {
        // The C++ state machine should ensure that no datatypes are
        // registered at this point, but we do a double-check just to
        // be sure.
        synchronized (bluetoothHealthLock) {
            for (HealthApp ha : healthAppMap.values()) {
                ha.onUnregisterDatatype();
            }
            healthAppMap.clear();
        }

        Log.d(TAG, "Subsystem interrupt requested");

        signalUpstream(new UpstreamInterruptEvent());
    }

    public void datatypeRequest(char datatype, boolean added) {
        synchronized (bluetoothHealthLock) {
            if (added) {
                if (!healthAppMap.containsKey((int)datatype)) {
                    Log.d(TAG, "Datatype registered: " +
                          (new Character(datatype)).hashCode());
                    HealthApp ha = new HealthApp(datatype);
                    healthAppMap.put((int)datatype, ha);
                    healthAppSet.add(ha);
                    if (bluetoothHealth != null)
                        ha.onServiceConnected();
                }
            } else {
                HealthApp ha = healthAppMap.get((int)datatype);
                if (ha != null) {
                    Log.d(TAG, "Datatype unregistered: " +
                          (new Character(datatype)).hashCode());
                    healthAppMap.remove((int)datatype);
                    ha.onUnregisterDatatype();
                }
            }
        }
    }

    public void disconnectRequest(String address, char datatype) {
        HealthApp ha;
        synchronized (bluetoothHealthLock) {
            ha = healthAppMap.get((int)datatype);
            if (ha == null) return;
            synchronized (ha) {
                VirtualPHD phd = ha.getVirtualPHD(address);
                if (phd == null) return;
                phd.disconnectRequest();
            }
        }
    }

    public void sendApduRequest(String address, char datatype,
                                byte apdu[], boolean primary) {
        HealthApp ha;
        synchronized (bluetoothHealthLock) {
            ha = healthAppMap.get((int)datatype);
        }
        if (ha == null) return;
        synchronized (ha) {
            VirtualPHD phd = ha.getVirtualPHD(address);
            if (phd == null) return;
            phd.sendApduRequest(apdu, primary);
        }
    }

    // Upstream messages for the native layer.

    private class UpstreamEvent {}
    private class UpstreamInterruptEvent extends UpstreamEvent {}
    private class UpstreamConnectEvent extends UpstreamEvent {
        private String address;
        private char datatype;
        private String displayName;
        private String serviceName;
        private String mdepDescription;
        private UpstreamConnectEvent(String address, char datatype, String displayName,
                                     String serviceName, String mdepDescription) {
            this.address = address;
            this.datatype = datatype;
            this.displayName = displayName;
            this.serviceName = serviceName;
            this.mdepDescription = mdepDescription;
        }
    }
    private class UpstreamDisconnectEvent extends UpstreamEvent {
        private String address;
        private char datatype;
        private int error;
        private UpstreamDisconnectEvent(String address, char datatype, int error) {
            this.address = address;
            this.datatype = datatype;
            this.error = error;
        }
    }
    private class UpstreamMessageEvent extends UpstreamEvent {
        private String address;
        private char datatype;
        private byte[] apdu;
        private boolean reliable;
        private UpstreamMessageEvent(String address, char datatype,
                                     byte[] apdu, boolean reliable) {
            this.address = address;
            this.datatype = datatype;
            this.apdu = apdu;
            this.reliable = reliable;
        }
    }

    LinkedList<UpstreamEvent> eventQueue = new LinkedList<UpstreamEvent>();

    private void signalUpstream(UpstreamEvent event) {
        synchronized (eventQueue) {
            eventQueue.add(event);
            if (eventQueue.size() == 1) {
                eventQueue.notifyAll();
            }
        }
    }

    private void dispatchUpstream() {
        UpstreamEvent event;
        do {

            // Get the next event

            synchronized (eventQueue) {
                while (eventQueue.size() == 0) {
                    try {
                        eventQueue.wait();
                    } catch (InterruptedException e) {}
                }
                event = eventQueue.remove();
            }

            // Dispatch the event

            if (event instanceof UpstreamConnectEvent) {
                UpstreamConnectEvent uce = (UpstreamConnectEvent)event;
                connectIndication(uce.address, uce.datatype, uce.displayName,
                                  uce.serviceName, uce.mdepDescription);
            } else if (event instanceof UpstreamDisconnectEvent) {
                UpstreamDisconnectEvent ude = (UpstreamDisconnectEvent)event;
                disconnectIndication(ude.address, ude.datatype, ude.error);
            } else if (event instanceof UpstreamMessageEvent) {
                UpstreamMessageEvent ume = (UpstreamMessageEvent)event;
                messageReceived(ume.address, ume.datatype, ume.apdu, ume.reliable);
            }
        } while (!(event instanceof UpstreamInterruptEvent));
    }

    // Upstream messages for the native layer.

    private native void connectIndication(String address, char datatype,
                String displayName, String serviceName, String mdepDescription);

    private native void disconnectIndication(String address, char datatype,
                                             int error);

    private native void messageReceived(String address, char datatype,
                                        byte[] apdu, boolean reliable);






    // The HealthApp State
    private enum State {
        UNREGISTERED,
        CONFIG_PENDING,
        ACTIVE,
        RECONFIG_PENDING,
        CONFIG_CANCELED,
        DELETE_PENDING,
        DELETED
    };

    private class HealthApp extends BluetoothHealthCallback {
        
        private final String name;
        private final char datatype;

        private State state;
        private BluetoothHealthAppConfiguration appConfig = null;

        private HashMap<String, VirtualPHD> virtualPHDs =
            new HashMap<String,VirtualPHD>();

        private HealthApp(char datatype) {
            this.datatype = datatype;
            // FIXME: Name should be made a variable property of some kind
            this.name = "4SDC";
            state = State.UNREGISTERED;
            Log.d(TAG,"HealthApp State: UNREGISTERED");
        }

        // The caller must hold the bluetoothHealthLock and ensure
        // that the datatype was registered in the healthAppSet
        // (i.e. this is the first - and only - time
        // onUnregisterDatatype is invoked).
        private synchronized void onUnregisterDatatype() {
            if (state == State.CONFIG_PENDING) {
                state = State.CONFIG_CANCELED;
                Log.d(TAG,"HealthApp State: CONFIG_CANCELED");
                return;
            } else if (state == State.RECONFIG_PENDING) {
                state = State.DELETE_PENDING;
                Log.d(TAG,"HealthApp State: DELETE_PENDING");
                return;
            } else if (state == State.ACTIVE) {
                killPHDs(ERROR_UNREGISTERED_DATATYPE_DURING_OPEN_CONNECTION);
            }
            // else: State.UNREGISTERED or State.ACTIVE since we cannot
            // be in State.DELETING or State.DELETED
            state = State.DELETED;
            Log.d(TAG,"HealthApp State: DELETED");

            if (appConfig != null) {
                bluetoothHealth.unregisterAppConfiguration(appConfig);
                appConfig = null;
            }
            healthAppSet.remove(this);
        }

        // The caller must hold the bluetoothHealthLock and ensure
        // the bluetoothHealth != null
        private synchronized void onServiceConnected() {
            if (state == State.RECONFIG_PENDING) {
                state = State.UNREGISTERED;
                Log.d(TAG,"HealthApp State: UNREGISTERED");
                bluetoothHealth.unregisterAppConfiguration(appConfig);
                appConfig = null;
            }
            if (state == State.UNREGISTERED) {

                // In case the service is not ready yet, make a busy
                // wait. This is an ugly hack due to inconsistent
                // behaviour in the bluetooth stack: In some rare
                // cases, the onServiceConnected event may be
                // signalled *before* the bluetooth adapter turns on
                // instead of after (the normal case). In case the
                // signal arrives early, we engage in a busy wait,
                // blocking the calling thread (and the monitors) for
                // a long time (about 2 seconds on one test platform)!
                // This seems to be an acceptable solution with no bad
                // side-effects *at the moment*. But one never knows
                // what will happen in future versions of the stack.
                // On one test platform, this inconsistency may be
                // provoked by disabling the bluetooth adapter in the
                // system settings before starting the application
                // using this subsystem. The very first time (and only
                // the first time) onServiceConnected is signalled,
                // the signal arrives (2 seconds) before the adapter
                // is turned on!
                
                while (BluetoothAdapter.getDefaultAdapter().getState() == 
                       BluetoothAdapter.STATE_TURNING_ON) {
                    Log.d(TAG,"POTENTIAL RACE-CONDITION (Read source code" 
                          + " comment if this locks up)");
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {}
                }

                state = State.CONFIG_PENDING;
                Log.d(TAG,"HealthApp State: CONFIG_PENDING");
                if (!bluetoothHealth.registerSinkAppConfiguration(name,
                                                          (int)datatype, this)) {
                    Log.e(TAG,"App Config registration failed");
                    if (state == State.CONFIG_PENDING) {
                        state = State.UNREGISTERED;
                        Log.d(TAG,"HealthApp State: UNREGISTERED");
                    }
                }
            } else if (state == State.DELETE_PENDING) {
                state = State.DELETED;
                Log.d(TAG,"HealthApp State: DELETED");
                bluetoothHealth.unregisterAppConfiguration(appConfig);
                appConfig = null;
                healthAppSet.remove(this);
            }
        }
        
        // The caller must hold the bluetoothHealthLock
        private synchronized void onServiceDisconnected() {
            if (state == State.CONFIG_PENDING) {
                state = State.UNREGISTERED;
                Log.d(TAG,"HealthApp State: UNREGISTERED");
            } else if (state == State.ACTIVE) {
                killPHDs(ERROR_BLUETOOTH_TURNED_OFF_DURING_OPEN_CONNECTION);
                state = State.RECONFIG_PENDING;
                Log.d(TAG,"HealthApp State: RECONFIG_PENDING");
            } else if (state == State.CONFIG_CANCELED) {
                state = State.DELETED;
                Log.d(TAG,"HealthApp State: DELETED");
                healthAppSet.remove(this);
            }
        }

        private synchronized VirtualPHD getVirtualPHD(String address) {
            if (state != State.ACTIVE) {
                return null;
            }
            return virtualPHDs.get(address);
        }

        // The caller must hold the bluetoothHealthLock
        private synchronized void killPHDs(int error) {
            for (VirtualPHD phd : virtualPHDs.values()) {
                phd.kill(error);
            }
            virtualPHDs.clear();
        }

        public void onHealthAppConfigurationStatusChange(
                                          BluetoothHealthAppConfiguration config,
                                          int status) {
            switch (status) {
            case BluetoothHealth.APP_CONFIG_REGISTRATION_FAILURE:
                Log.e(TAG,"App Config registration failed");
                break;
            case BluetoothHealth.APP_CONFIG_REGISTRATION_SUCCESS:
                Log.d(TAG,"App Config registration success");
                break;
            case BluetoothHealth.APP_CONFIG_UNREGISTRATION_FAILURE:
                Log.d(TAG,"App Config unregistration failure");
                break;
            case BluetoothHealth.APP_CONFIG_UNREGISTRATION_SUCCESS:
                Log.d(TAG,"App Config unregistration success");
                break;
            }
            synchronized (bluetoothHealthLock) {
                synchronized (this) {
                    if (state == State.CONFIG_PENDING) {
                        if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_SUCCESS) {
                            state = State.ACTIVE;
                            Log.d(TAG,"HealthApp State: ACTIVE");
                            appConfig = config;
                        } else {
                            state = State.UNREGISTERED;
                            Log.d(TAG,"HealthApp State: UNREGISTERED");
                            // FIXME: Perhaps we should send a
                            // notification about this to the error
                            // handling subsystem
                        }
                    } else if (state == State.ACTIVE) {
                        if (status == BluetoothHealth.APP_CONFIG_UNREGISTRATION_SUCCESS) {
                            killPHDs(ERROR_BLUETOOTH_TURNED_OFF_DURING_OPEN_CONNECTION);
                            state = State.UNREGISTERED;
                            Log.d(TAG,"HealthApp State: UNREGISTERED");
                            appConfig = null;
                        }
                    } else if (state == State.CONFIG_CANCELED) {
                        state = State.DELETED;
                        Log.d(TAG,"HealthApp State: DELETED");
                        if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_SUCCESS) {
                            bluetoothHealth.unregisterAppConfiguration(config);
                        }
                        healthAppSet.remove(this);
                    }
                }
            }
        }
                
        public synchronized void onHealthChannelStateChange(
                                          BluetoothHealthAppConfiguration config,
                                          BluetoothDevice device, int prevState,
                                          int newState, ParcelFileDescriptor fd,
                                          int channelId) {
            
            if (state != State.ACTIVE) {
                return;
            }
            VirtualPHD phd = virtualPHDs.get(device.getAddress());
            if (phd == null && newState == BluetoothHealth.STATE_CHANNEL_CONNECTED) {
                // Create VirtualPHD
                phd = new VirtualPHD(datatype,device,appConfig);
                virtualPHDs.put(device.getAddress(), phd);
            }
            if (phd != null && phd.channelStateChange(channelId,newState,fd)) {
                // VirtualPHD disconnected
                virtualPHDs.remove(device.getAddress());
            }
        }
    }

    // All method calls on objects of this class must be synchronized
    // on the "parent" HealthApp object
    private class VirtualPHD {
        // Errorcode to be signalled in the disconnect indication
        private int error = 0;
        private final char datatype;
        private final BluetoothDevice device;
        private final HashMap<Integer,Channel> channels = new HashMap<Integer,Channel>();
        private final BluetoothHealthAppConfiguration appConfig;
        private Channel primaryChannel = null;
        private VirtualPHD(char datatype, BluetoothDevice device,
                           BluetoothHealthAppConfiguration appConfig) {
            this.datatype = datatype;
            this.device = device;
            this.appConfig = appConfig;
        }

        // The caller must hold the bluetoothHealthLock as well as the
        // parent HealthApp object lock
        private void disconnectRequest() {
            for (Channel c : channels.values()) {
                c.disconnect();
                Log.d(TAG,"Channel disconnected");
            }
        }

        private void sendApduRequest(byte apdu[], boolean primary) {
            // FIXME: We currently ignore the "primary" argument
            // because the bluedroid stack at the moment does not
            // specify the connection type. This implies that we
            // support multiple incoming channels, but we only ever
            // send on the primary (=first) channel!
            if (primaryChannel != null) {
                primaryChannel.sendApdu(apdu);
            }
        }

        // returns true if the device was disconnected (and will be
        // silent from now on)
        private boolean channelStateChange(int channelId, int newState,
                                           ParcelFileDescriptor fd) {

            Channel channel = channels.get(channelId);
            if (channel == null && newState == BluetoothHealth.STATE_CHANNEL_CONNECTED) {
                if (primaryChannel == null) {
                    signalUpstream(new UpstreamConnectEvent(device.getAddress(), datatype,
                                                            device.getName(),
                                                            device.getName(), ""));
                }
                // Create channel
                channel = new Channel(channelId, fd, this, primaryChannel == null);
                channels.put(channelId, channel);
                if (primaryChannel == null) {
                    Log.d(TAG,"Primary channel opened to device");
                    // Signal connectEvent
                    primaryChannel = channel;
                } else {
                    Log.d(TAG,"Extra channel opened to device");
                }
            } else if (channel == null) {
                return false;
            } else if (newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED || 
                       newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTING) {
                channel.close();
                channels.remove(channel.id);
                if (channel == primaryChannel) {
                    Log.d(TAG,"Primary channel closed");

                    // Primary channel closed - disconnect all other channels
                    primaryChannel = null;

                    // Close all channels
                    for (Channel c : channels.values()) {
                        c.close();
                        Log.d(TAG,"Channel closed");
                    }
                    channels.clear();
                    signalUpstream(new UpstreamDisconnectEvent(device.getAddress(),
                                                               datatype, error));
                    return true;
                } else {
                    Log.d(TAG,"Extra channel closed");
                }
            }
            return false;
        }
        // This will immediately force-close all channels and signal
        // the disconnectIndication upstream. All upstream events will
        // be signalled before this method returns.
        // The caller must hold the bluetoothHealthLock as well as the
        // parent HealthApp object lock
        private void kill(int error) {
            // Disconnect and close all channels
            for (Channel c : channels.values()) {
                c.disconnect();
                c.close();
                Log.d(TAG,"Channel force-closed");
            }
            channels.clear();
            primaryChannel = null;
            signalUpstream(new UpstreamDisconnectEvent(device.getAddress(), datatype, error));
        }
    }


    private class Channel {
        private final int id;
        private final ParcelFileDescriptor fd;
        private final VirtualPHD phd;
        private final FileOutputStream writer;
        private final ReaderThread reader;

        private Channel(int id, ParcelFileDescriptor fd, VirtualPHD phd, boolean reliable) {
            this.id = id;
            this.fd = fd;
            this.phd = phd;
            writer = new FileOutputStream(fd.getFileDescriptor());
            reader = new ReaderThread(fd, phd, reliable);
            reader.start();
            Log.d(TAG,"CHANNEL NEW");
        }

        // The caller must hold the bluetoothHealthLock as well as the
        // parent HealthApp object lock
        private void disconnect() {
            if (bluetoothHealth != null) {
                bluetoothHealth.disconnectChannel(phd.device, phd.appConfig, id);
            }
        }

        private void close() {
            Log.d(TAG,"CHANNEL CLOSE");
            // Close reader, writer and the file descriptor - ignoring errors
            reader.close();
            try {
                writer.close();
            } catch (IOException e) {}
            try {
                fd.close();
            } catch (IOException e) {}
        }

        private void sendApdu(byte apdu[]) {
            try {
                writer.write(apdu);
            } catch (IOException e) {
                e.printStackTrace();
                reader.abort(ERROR_SENDING_DATA_TO_BLUETOOTH_FAILED);
            }
        }
    }

    private class ReaderThread extends Thread {

        private ParcelFileDescriptor fd;
        private VirtualPHD phd;
        private boolean reliable;

        private ReaderThread(ParcelFileDescriptor fd, VirtualPHD phd, boolean reliable) {
            this.fd = fd;
            this.phd = phd;
            this.reliable = reliable;
        }

        private void close() {
            //FIXME
        }

        private void abort(int error) {
            // FIXME
        }

        @Override
        public void run() {
            FileInputStream is = new FileInputStream(fd.getFileDescriptor());
            final byte[] data = new byte[63*1024];
            try {
                int len;
                while ((len = is.read(data)) > -1) {
                    if (len > 0) {
                        byte[] apdu = new byte[len];
                        System.arraycopy(data, 0, apdu, 0, len);

                        signalUpstream(new UpstreamMessageEvent(phd.device.getAddress(),
                                                                phd.datatype, apdu, reliable));

                    }
                }
            } catch (IOException e) {
                // FIXME: Currently ignoring all problems
            // ERROR_RECEIVING_DATA_FROM_BLUETOOTH_FAILED
            }
        }
    }

    /*


      Map<BluetoothDevice, MapList<Integer,Channel> >
      Channel { int state, ParcelFileDescr., ReaderThread reader, FileOutputStream writer }

     */


/*
    private static int lastDeviceID = 0;

    private static FileOutputStream wr;

*/
/*    private static final BluetoothHealthCallback healthCallback
        = new BluetoothHealthCallback() {
            public void onHealthAppConfigurationStatusChange(BluetoothHealthAppConfiguration config,
                                                             int status) {
                if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_FAILURE) {
                    Log.e(TAG,"App Config registration failed");
                    // FIXME: Report the error?
                } else if (status == BluetoothHealth.APP_CONFIG_REGISTRATION_SUCCESS) {
                    Log.d(TAG,"App Config registration success");
                    // Notice: may be called multiple times...
                    //configs.add(config);
                    //sendMessage(STATUS_HEALTH_APP_REG, RESULT_OK, null);
                } else if (status == BluetoothHealth.APP_CONFIG_UNREGISTRATION_FAILURE) {
                    Log.d(TAG,"App Config unregistration failure");
                } else if (status == BluetoothHealth.APP_CONFIG_UNREGISTRATION_SUCCESS) {
                    Log.d(TAG,"App Config unregistration success");
                }
            }
                
            public void onHealthChannelStateChange(BluetoothHealthAppConfiguration config,
                            BluetoothDevice device, int prevState, int newState, ParcelFileDescriptor fd,
                            int channelId) {
                if (prevState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED &&
                                newState == BluetoothHealth.STATE_CHANNEL_CONNECTED) {
                    Log.d(TAG,"Channel connected");
//                    if (acceptsConfiguration(config)) {
//                        insertDeviceConfiguration(device, config);
//                        insertChannelId(device, channelId);
///                        connectIndication(1);
///                        wr = new FileOutputStream(fd.getFileDescriptor());
//                        insertWriter(device, wr);
///                        (new ReaderThread(1, fd)).start();
//                    } else {
//                        sendMessage(STATUS_CREATE_CHANNEL, RESULT_FAIL, device);
//                    }
                } else if (prevState == BluetoothHealth.STATE_CHANNEL_CONNECTING &&
                            newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED) {
                    Log.d(TAG,"Channel connect aborted");
//                    sendMessage(STATUS_CREATE_CHANNEL, RESULT_FAIL, device);
//                    removeWriter(device);
                } else if (newState == BluetoothHealth.STATE_CHANNEL_DISCONNECTED) {
                    Log.d(TAG,"Channel disconnected");
///                      disconnectIndication(1);
///                      wr = null;
/*                    if (acceptsConfiguration(config)) {
                        sendMessage(STATUS_DESTROY_CHANNEL, RESULT_OK, device);
                        removeWriter(device);
                    } else {
                        sendMessage(STATUS_DESTROY_CHANNEL, RESULT_FAIL, device);
                        removeWriter(device);
                    }*/
/*                }
            }
        };

*/  /*

    private static class ReaderThread extends Thread {
        int device;
        ParcelFileDescriptor fd;
        public ReaderThread(int device, ParcelFileDescriptor fd) {
            super();
            this.device = device;
            this.fd = fd;
        }

        @Override
        public void run() {
            FileInputStream is = new FileInputStream(fd.getFileDescriptor());
            final byte[] data = new byte[63*1024];
            try {
                int len;
                while ((len = is.read(data)) > -1) {
                    if (len > 0) {
                        byte[] buf = new byte[len];
                        System.arraycopy(data, 0, buf, 0, len);
                        messageReceived(device, buf);
                    }
                }
            } catch (IOException e) {
                // FIXME: Currently ignoring all problems
            }
            try {
                if (fd != null) fd.close();
            } catch (IOException e) {}
            wr = null;
        }
    }


*/
    /* C++ -> Java methods */

/*
    public static void registerDatatype(int datatype) {
        Log.d(TAG,"*** registered datatype: " + datatype);
        if (bluetoothHealth != null) {
            bluetoothHealth.registerSinkAppConfiguration("FIXME:GetADecentName", datatype, healthCallback);
        } else {
            // FIXME: Store datatypes for later registration in a map if the
            // bluetoothHealth is not yet connected
        }
    }

    public static void unregisterDatatype(int datatype) {
        Log.d(TAG,"*** unregistered datatype: " + datatype);
// FIXME: Needs a map of registered datatypes...
    }

    public static String getBTAddress(int device) {
        Log.d(TAG,"*** getBTAddress called");
        return "Hello, World! ADDRESS";
    }

    public static String getName(int device) {
        Log.d(TAG,"*** getName called");
        return "Hello, World! NAME";
    }

    public static void disconnect(int device) {
        Log.d(TAG,"*** disconnect called");

    }

    public static void sendMessagePrimary(int device, byte[] message) {
        Log.d(TAG,"*** sendMessagePrimary called");
        // FIXME: Ignoring device number
        if (wr == null) return;
        try {
            wr.write(message);
        } catch (IOException e) {
            Log.e(TAG, "Write exception: " + e.toString());
            // FIXME: Handle exceptions
        }
    }

    public static void sendMessageStreaming(int device, byte[] message) {
        Log.d(TAG,"*** sendMessageStreaming called");
        sendMessagePrimary(device, message);
    }
*/
    /* Java -> C++ methods */
/*
    private static native void connectIndication(int device);
    private static native void disconnectIndication(int device);
    private static native void messageReceived(int device, byte[] message);
*/
}


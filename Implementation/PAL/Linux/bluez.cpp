/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Linux Bluetooth (BlueZ) PAL-layer module implementation.
 *
 *
 * @todo A lot of Exception checking is going on, but not a lot of exception handling...
 * @todo A few QDBusPendingCall::waitForFinished() are used as temp shortcuts. The code ought to use only
 *       asyncronous calls though. This *may* also pose a threat to the assumption that only one
 *       signal is being processed at any time (the consequences of using waitForFinished() is not clear
 *       in the documentation)!
 *
 * @note This module connects to a BlueZ v.4 subsystem, which is the version
 *       currently used on Ubuntu. It will not yet work on the newer BlueZ v.5,
 *       due to some fundamental changes in the API!
 * @todo Support for BlueZ v.5. should be implemented at some point.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute, &copy; 2014
 */


#include "PAL/bluetooth.h"

#include <memory>
#include <vector>


using PAL::BluetoothModule;

using std::shared_ptr;
using std::vector;

/* ************************************************************ */
/*                BluetoothModule implementation                */
/* ************************************************************ */

// The BluetoothModule ctor/dtor creates and deletes the PrivateBluetoothModule
// defined earlier. All methods of the BluetoothModule are then simply forwarded
// to the private counterpart.

BluetoothModule::BluetoothModule() :
    privateParts (nullptr /*new PrivateBluetoothModule(this)*/) {}

BluetoothModule::~BluetoothModule() {
/*    assert(privateParts);
    delete privateParts;
    privateParts = nullptr;*/
}

void BluetoothModule::msgGoingDown() {
/*    assert(privateParts);
    privateParts->terminate();*/
}

void BluetoothModule::registerDatatype(uint16_t datatype) noexcept {
/*    assert(privateParts);
    privateParts->registerDatatype(datatype, hm);*/
}

void BluetoothModule::unregisterDatatype(uint16_t datatype) noexcept {
/*    assert(privateParts);
    privateParts->unregisterDatatype(datatype);*/
}

void BluetoothModule::sendApduPrimary(shared_ptr<VirtualPHD> device,
                                   shared_ptr<vector<uint8_t> > apdu) noexcept {
/*    assert(privateParts);
    privateParts->sendApduPrimary(device, apdu);*/
}

void BluetoothModule::sendApduStreaming(shared_ptr<VirtualPHD> device,
                                   shared_ptr<vector<uint8_t> > apdu) noexcept {
/*    assert(privateParts);
    privateParts->sendApduStreaming(device, apdu);*/
}

void BluetoothModule::disconnect(shared_ptr<VirtualPHD> device) noexcept {
/*    assert(privateParts);
    privateParts->disconnect(device);*/
}


#if 0
#include "bluez.h"
#include "FSYS/log.h"
#include "PAL/personalhealthdevice.h"
#include "FSYS/basemsg.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"
#include "4SDC/core.h"

#include <QObject>
#include <QtDBus>

#include <stdexcept>
#include <iostream>
#include <sys/socket.h>
#include <cerrno>

DECLARE_LOG_MODULE("BlueZ")



using PAL::VirtualPHD;
using std::shared_ptr;
using std::vector;
using std::runtime_error;


// Forward declaration of classes defined later

namespace {
class HealthApplication;
class HealthDevice;
class Adapter;
class HealthChannel;
class HDP;
}

class BluezBluetoothModule::PrivateBluez : public QObject {
    Q_OBJECT

    friend class BluezBluetoothModule;

private:
    PrivateBluez(BluezBluetoothModule *parent);
    ~PrivateBluez();

    BluezBluetoothModule *parent;
    QDBusConnection bus;
    QDBusInterface manager;
    QDBusInterface healthManager;
    QMap<uint16_t,HealthApplication*> healthapps;
    QMap<QString,Adapter*> adapters;
    QMap<QString,HealthDevice*> devices;
    bool deviceCreated(const QString path, Adapter *adapter);
    void deviceRemoved(const QString path);


public slots:
    void adapterAdded(const QDBusObjectPath path);
    void adapterRemoved(const QDBusObjectPath path);

};



namespace {
class HealthApplication : public QObject { // Hvorfor QObject?
    Q_OBJECT
public:
    HealthApplication(unsigned, BluezBluetoothModule*);
    ~HealthApplication();
    BluezBluetoothModule *parent;
    QDBusObjectPath path;
};

class Adapter : public QObject {
    Q_OBJECT
public:
    Adapter(QString,BluezBluetoothModule*);
    ~Adapter();
    QDBusInterface interface;
    QSet<QString> devices;
    BluezBluetoothModule *parent;
public slots:
    void deviceCreated(const QDBusObjectPath path);
    void deviceRemoved(const QDBusObjectPath path);
};


class HealthDevice : public QObject {
    Q_OBJECT
public:
    HealthDevice(QString, Adapter*, BluezBluetoothModule*);
    ~HealthDevice();
    QString getBTAddress();
    QString getName();
    QString getAlias();
    bool isConnected();
    bool isPaired();
    void sendMessagePrimary(std::vector<unsigned char> *message);
    void sendMessageStreaming(std::vector<unsigned char> *message);
    void disconnect(bool force);
    void messageReceived(std::vector<unsigned char> *message);

    QDBusInterface deviceInterface;
    QDBusInterface healthInterface;
    Adapter *adapter;
    BluezBluetoothModule *parent;
    QMap<QString,HealthChannel*> channels;
    QList<HealthChannel*> reliableChannels;
    QList<HealthChannel*> streamingChannels;
    HDP *hdp;
    //std::shared_ptr<PersonalHealthDeviceInterface> phddevice;
    QString btaddress, devname, alias;
    bool connected, paired;
public slots:
    void channelDeleted(const QDBusObjectPath path);
    void channelConnected(const QDBusObjectPath path);
    void propertyChanged(const QString devname, const QDBusVariant value);
};

class HealthChannel : public QObject {
    Q_OBJECT
public:
    HealthChannel(QDBusObjectPath, HealthDevice*, BluezBluetoothModule*);
    ~HealthChannel();
    void sendMessage(std::vector<unsigned char> *message);
    QDBusObjectPath path;
    bool isReliable();
    QDBusInterface channelInterface;
    HealthDevice *device;
    BluezBluetoothModule *parent;
    bool reliable;
    QDBusUnixFileDescriptor fileDescriptor;
    QSocketNotifier *notifier;
public slots:
    void dataReceived(int);
};
}








BluezBluetoothModule::BluezBluetoothModule() {
    // TODO: Should test if BlueZ is available and then test the BlueZ
    //       version and adapt PrivateBlueZ initialization to either v4 or v5
    privateParts = new PrivateBluez(this);
}

BluezBluetoothModule::~BluezBluetoothModule() {
    delete privateParts;
}

void BluezBluetoothModule::receive(Terminate) {

}

void BluezBluetoothModule::registerDatatype(uint16_t datatype) noexcept {

}

void BluezBluetoothModule::BluezBluetoothModule::unregisterDatatype(
                                                   uint16_t datatype) noexcept {

}

void BluezBluetoothModule::sendApduPrimary(shared_ptr<VirtualPHD> device,
                                   shared_ptr<vector<uint8_t> > apdu) noexcept {

}

void sendApduStreaming(shared_ptr<VirtualPHD> device,
                       shared_ptr<vector<uint8_t> > apdu) noexcept {

}

void disconnect(shared_ptr<VirtualPHD> device) noexcept {

}















BluezBluetoothModule::PrivateBluez::PrivateBluez(BluezBluetoothModule *parent) :
            QObject(),
            parent(parent),
            bus(QDBusConnection::systemBus()),
            manager("org.bluez","/","org.bluez.Manager",bus),
            healthManager("org.bluez","/org/bluez","org.bluez.HealthManager",bus),
            healthapps(),
            adapters(),
            devices() {

    DEBUG("Constructor Start");

    // Check system D-Bus
    if (!bus.isConnected()) {
        ERR("The System D-Bus is not available");
        throw runtime_error("The System D-Bus is not available");
    }
    // Check the BlueZ Manager and HealthManager
    if (!(manager.isValid() && healthManager.isValid())) {
        ERR("No BlueZ v.4 subsystem available");
        throw runtime_error("No BlueZ v.4 subsystem available");
    }

    DEBUG("Constructor Done");
}

BluezBluetoothModule::PrivateBluez::~PrivateBluez() {
    // Disconnect AdapterAdded/Removed signals
    if (!bus.disconnect("org.bluez","/","org.bluez.Manager","AdapterAdded",this,SLOT(adapterAdded(const QDBusObjectPath)))) {
        ERR("AdapterAdded connection failed");
    }
    if (!bus.disconnect("org.bluez","/","org.bluez.Manager","AdapterRemoved",this,SLOT(adapterRemoved(const QDBusObjectPath)))) {
        ERR("AdapterRemoved connection failed");
    }
    // Deleting all adapters will recursively take care of all devices and connections as well
    foreach(Adapter *a, adapters.values()) {
        if (a) delete a;
    }
    // Delete/unregister all the health applications
    foreach (HealthApplication *h, healthapps.values()) {
        if (h) delete h;
    }
}



void BluezBluetoothModule::receive(fsdc::Core::Ready)
{
    DEBUG("HDPConnector::receive(fsdc::Core::Ready)");
    // Connect AdapterAdded/Removed signals
    if (!bus.connect("org.bluez","/","org.bluez.Manager","AdapterAdded",this,SLOT(adapterAdded(const QDBusObjectPath)))) {
        ERR("AdapterAdded connection failed");
        throw ConnectionException("AdapterAdded connection failed");
    }
    if (!bus.connect("org.bluez","/","org.bluez.Manager","AdapterRemoved",this,SLOT(adapterRemoved(const QDBusObjectPath)))) {
        ERR("AdapterRemoved connection failed");
        throw ConnectionException("AdapterRemoved connection failed");
    }

    // Find all currently available Bluetooth adapters
    QDBusPendingReply<QVariantMap> reply = manager.asyncCall(QLatin1String("GetProperties"));
    reply.waitForFinished();
    if (reply.isError()) {
        ERR(reply.error().message().toStdString());
        throw ConnectionException(reply.error().message().toStdString());
    }
    if (reply.count() != 1) {
        ERR("No manager properties");
        throw ConnectionException("No manager properties");
    }
    QList<QVariant> values = reply.value().values("Adapters");
    if (values.count() != 1 || values[0].value<QDBusArgument>().currentType() != QDBusArgument::ArrayType) {
        ERR("Confused by manager properties");
        throw ConnectionException("Confused by manager properties");
    }
    QDBusArgument value = values[0].value<QDBusArgument>();
    value.beginArray();
    while (!value.atEnd()) {
        QDBusObjectPath adapter;
        value >> adapter;
        adapterAdded(adapter);
    }
}

void BluezBluetoothModule::receive(HDPWrapper::RegisterDataType &signal)
{
    DEBUG("HDPConnector::receive(HDPWrapper::RegisterDataType &signal)");
    if (healthapps.keys().contains(signal.dataType))
    {
        // Already registered - bail out
        return;
    }
    healthapps.insert(signal.dataType,new HealthApplication(signal.dataType,this));
}

void BluezBluetoothModule::receive(HDPWrapper::UnregisterDataType &signal)
{
    DEBUG("HDPConnector::receive(HDPWrapper::UnregisterDataType &signal)");
    HealthApplication *h = healthapps.take(signal.dataType);
    if (h) delete h;
}




/* ********** HealthApplication implementation ********** */

/*
 * Responisble for registering/unregistering the application as
 * handler for an HDP datatype. Registration takes place in the
 * constructor and unregistration in the destructor.
 */

HealthApplication::HealthApplication(unsigned dataType, BluezBluetoothModule *parent) :
        parent(parent) {
    // Register the Health Application
    QVariantMap args;
    // QVariant apparently does not preserve the ushort type, so we need this hack to make an explicit cast, courtesy of
    // http://stackoverflow.com/questions/5008982/connecting-to-the-new-bluez-hdp-plugin-using-dbus-from-qt-c
    QVariant dt(dataType);
    dt.convert((QVariant::Type)(qDBusRegisterMetaType<ushort>()));
    args.insert("DataType",dt);
    args.insert("Role","Sink");
    QDBusPendingReply<QDBusObjectPath> reply = parent->healthManager.asyncCall(QLatin1String("CreateApplication"),args);
    reply.waitForFinished();
    if (reply.isError()) {
        ERR(reply.error().message().toStdString());
        throw ConnectionException(reply.error().message().toStdString());
    }
    if (reply.count() != 1) {
        ERR("No path returned when registering health application");
        throw ConnectionException("No path returned when registering health application");
    }
    path = reply.value();
    DEBUG((QString("Registered Health application for type: ") + QString::number(dataType) + " with path: " + path.path()).toStdString());
}

HealthApplication::~HealthApplication() {
    // Unregister the Health Application
    QDBusPendingReply<> reply = parent->healthManager.asyncCall(QLatin1String("DestroyApplication"),QVariant::fromValue(path));
    reply.waitForFinished();
    if (reply.isError()) {
        ERR(reply.error().message().toStdString());
        throw ConnectionException(reply.error().message().toStdString());
    }
    if (reply.count() != 0) {
        ERR("Confused by return value when attempting to destroy health application");
    } else {
        DEBUG((QString("Unregistered HealthApplication: ") + path.path()).toStdString());
    }
}




/* ********** AdapterAdded / Removed signal handlers ********** */

/*
 * Responsible for creating / disposing the Adapter objects whenever
 * a physical bluetooth adapter is connected / disconnected.
 */

void BluezBluetoothModule::adapterAdded(const QDBusObjectPath path) {
    DEBUG((QString("Adapter added: ") + path.path()).toStdString());
    if (adapters.keys().contains(path.path())) {
        DEBUG("Existing adapter - bail out");
        return;
    }

    adapters.insert(path.path(), new Adapter(path.path(),this));
}

void BluezBluetoothModule::adapterRemoved(const QDBusObjectPath path) {
    DEBUG((QString("Adapter removed: ") + path.path()).toStdString());
    Adapter *adapter = adapters.take(path.path());
    if (adapter) delete adapter;
}





/* ********** Adapter Class implementation ********** */

/*
 * Adapters are new'ed either during BluezHDP::start() or when a
 * new adapter is physically connected. Adapters are deleted
 * when the physical adapter is disconnected.
 *
 * An object of this class is responsible for managing the bluetooth
 * devices connected to its adapter, invoking the BluezHDP parent's
 * deviceCreated() and deviceRemoved() methods when appropriate.
 */

Adapter::Adapter(QString path, BluezBluetoothModule *parent) :
        interface("org.bluez",path,"org.bluez.Adapter",parent->bus),
        parent(parent) {

    if (!interface.isValid()) {
        ERR("Adapter proxy creation failed");
        throw ConnectionException("Adapter proxy creation failed");
    }

    if (!parent->bus.connect("org.bluez",path,"org.bluez.Adapter","DeviceCreated",this,SLOT(deviceCreated(const QDBusObjectPath)))) {
        ERR("Device Created connection failed");
        throw ConnectionException("AdapterAdded connection failed");
    }

    if (!parent->bus.connect("org.bluez",path,"org.bluez.Adapter","DeviceRemoved",this,SLOT(deviceRemoved(const QDBusObjectPath)))) {
        ERR("Device Removed connection failed");
        throw ConnectionException("AdapterRemoved connection failed");
    }


    // Find all currently available Bluetooth devices
    QDBusPendingReply<QVariantMap> reply = interface.asyncCall(QLatin1String("GetProperties"));
    reply.waitForFinished();
    if (reply.isError()) {
        ERR(reply.error().message().toStdString());
        throw ConnectionException(reply.error().message().toStdString());
    }
    if (reply.count() != 1) {
        ERR("No adapter properties");
        throw ConnectionException("No adapter properties");
    }
    QList<QVariant> values = reply.value().values("Devices");
    if (values.count() != 1 || values[0].value<QDBusArgument>().currentType() != QDBusArgument::ArrayType) {
        ERR("Confused by adapter properties");
        throw ConnectionException("Confused by adapter properties");
    }
    QDBusArgument value = values[0].value<QDBusArgument>();
    value.beginArray();
    while (!value.atEnd()) {
        QDBusObjectPath device;
        value >> device;
        deviceCreated(device);
    }


}

Adapter::~Adapter() {
    if (!parent->bus.disconnect("org.bluez",interface.path(),"org.bluez.Adapter","DeviceCreated",this,SLOT(deviceCreated(const QDBusObjectPath)))) {
        ERR("Device Created connection removal failed");
    }
    if (!parent->bus.disconnect("org.bluez",interface.path(),"org.bluez.Adapter","DeviceRemoved",this,SLOT(deviceRemoved(const QDBusObjectPath)))) {
        ERR("Device Removed connection removal failed");
    }
    foreach (QString device, devices) {
        parent->deviceRemoved(device);
    }
}

void Adapter::deviceCreated(const QDBusObjectPath path) {
    if (devices.contains(path.path())) {
        DEBUG("Existing device - bail out");
        return;
    }
    if (parent->deviceCreated(path.path(),this)) {
        devices.insert(path.path());
    }
}

void Adapter::deviceRemoved(const QDBusObjectPath path) {
    if (devices.remove(path.path())) {
        parent->deviceRemoved(path.path());
    }
}





/* ********** DeviceCreated / Removed signal handlers ********** */

/*
 * Responsible for creating / disposing the Device objects whenever
 * a bluetooth device connection is created / destroyed.
 */


bool BluezBluetoothModule::deviceCreated(const QString path, Adapter *adapter) {
    DEBUG((QString("Device created ") + path).toStdString());
    if (devices.keys().contains(path)) {
        WARN("Existing device created again - this should NOT be possible!");
        return false;
    }

    devices.insert(path, new HealthDevice(path,adapter,this));
    return true;
}


void BluezBluetoothModule::deviceRemoved(const QString path) {
    DEBUG((QString("Device removed: ") + path).toStdString());

    HealthDevice *device = devices.take(path);
    if (device) delete device;
}





/* ********** HDP private Class implementation ********** */

class HDP : public HDPDevice {
private:
    bool connected;
    HealthDevice *parent;
public:
    bool isConnected() {return connected;}

    void sendMessagePrimary(std::vector<unsigned char> *message) {
        if (parent) parent->sendMessagePrimary(message);
    }

    void sendMessageStreaming(std::vector<unsigned char> *message) {
        if (parent) parent->sendMessageStreaming(message);
    }

    std::string getBTAddress() {
        if (parent) return parent->getBTAddress().toStdString();
        return "";
    }

    std::string getName() {
        if (parent) return parent->getName().toStdString();
        return "";
    }

    void disconnect() {
        // FIXME: Dette vil med det samme sende et
        // PersonalHealthDeviceListener::disconnectIndication() signal,
        // hvilket bryder med den sekventielle ordning af beskeder...
        // Problemet vil måske blive løst af kø-systemet.
        if (parent) parent->disconnect(true);
    }

    HDP() : connected(false), parent(NULL) {}
    HDP(HealthDevice *parent) : connected(true), parent(parent) {}

    void kill() { connected = false; parent = NULL; }

};


/* ********** Device Class implementation ********** */

/*
 * Represents a device associated with a bluetooth adapter. The device may or may not be
 * a health (HDP-compliant) device, but only health devices will open channels.
 */

HealthDevice::HealthDevice(QString path, Adapter *adapter, BluezBluetoothModule *parent) :
    deviceInterface("org.bluez",path,"org.bluez.Device",parent->bus),
    healthInterface("org.bluez",path,"org.bluez.HealthDevice",parent->bus),
    adapter(adapter),
    parent(parent),
    channels(),
    reliableChannels(),
    streamingChannels(),
    hdp(NULL),
    phddevice(new HDP()) {

    // Check the proxy
    if (!(healthInterface.isValid() && deviceInterface.isValid())) {
        DEBUG("(Health)Device proxy creation failed");
        throw ConnectionException("(Health)Device proxy creation failed");
    }

    if (!parent->bus.connect("org.bluez",path,"org.bluez.HealthDevice","ChannelConnected",this,SLOT(channelConnected(const QDBusObjectPath)))) {
        ERR("Channel Connected connection failed");
        throw ConnectionException("Channel Connected connection failed");
    }

    if (!parent->bus.connect("org.bluez",path,"org.bluez.HealthDevice","ChannelDeleted",this,SLOT(channelDeleted(const QDBusObjectPath)))) {
        ERR("Channel Deleted connection failed");
        throw ConnectionException("Channel Deleted connection failed");
    }

    if (!parent->bus.connect("org.bluez",path,"org.bluez.Device","PropertyChanged",this,SLOT(propertyChanged(const QString, const QDBusVariant)))) {
        ERR("Property Changed connection failed");
        throw ConnectionException("Property Changed connection failed");
    }

    // Load initial property values
    QDBusPendingReply<QVariantMap> reply = deviceInterface.asyncCall(QLatin1String("GetProperties"));
    reply.waitForFinished();
    if (reply.isError()) {
        ERR(reply.error().message().toStdString());
        throw ConnectionException(reply.error().message().toStdString());
    }
    if (reply.count() != 1) {
        ERR("No device properties");
        throw ConnectionException("No device properties");
    }
    QVariantMap vm = reply.value();

    btaddress = vm["Address"].toString();
    devname = vm["Name"].toString();
    alias = vm["Alias"].toString();
    connected = vm["Connected"].toBool();
    paired = vm["Paired"].toBool();

    DEBUG((QString("Device created: ")+devname+" with address: "+btaddress+", is paired: "+(paired?"yes":"no")+", is connected: "+(connected?"yes":"no")).toStdString());
}

HealthDevice::~HealthDevice() {
    disconnect(true);
    if (!parent->bus.disconnect("org.bluez",deviceInterface.path(),"org.bluez.HealthDevice","ChannelConnected",this,SLOT(channelConnected(const QDBusObjectPath)))) {
        ERR("Channel Connected disconnection failed");
    }

    if (!parent->bus.disconnect("org.bluez",deviceInterface.path(),"org.bluez.HealthDevice","ChannelDeleted",this,SLOT(channelDeleted(const QDBusObjectPath)))) {
        ERR("Channel Deleted disconnection failed");
    }

    if (!parent->bus.disconnect("org.bluez",deviceInterface.path(),"org.bluez.Device","PropertyChanged",this,SLOT(propertyChanged(const QString, const QDBusVariant)))) {
        ERR("Property Changed disconnection failed");
    }
}

QString HealthDevice::getBTAddress() { return btaddress; }
QString HealthDevice::getName()      { return devname;   }
QString HealthDevice::getAlias()     { return alias;     }
bool HealthDevice::isConnected()     { return connected; }
bool HealthDevice::isPaired()        { return paired;    }

void HealthDevice::sendMessagePrimary(std::vector<unsigned char> *message) {
    // Send message using the first reliable channel, if available
    if (!reliableChannels.empty()) {
        reliableChannels.first()->sendMessage(message);
    }
}

void HealthDevice::sendMessageStreaming(std::vector<unsigned char> *message) {
    // Send message using the first streaming channel, if available
    if (!streamingChannels.empty()) {
        streamingChannels.first()->sendMessage(message);
    } else {
        // If no streaming channels, use the primary
        sendMessagePrimary(message);
    }
}

void HealthDevice::channelConnected(const QDBusObjectPath path) {
    DEBUG((QString("Channel connect request from: ") + path.path()).toStdString());

    if (channels.keys().contains(path.path())) {
        DEBUG("Existing channel - bail out");
        return;
    }

    // Create channel
    HealthChannel *channel = new HealthChannel(path,this,parent);
    channels.insert(path.path(),channel);
    if (channel->isReliable()) {
        reliableChannels.append(channel);
        DEBUG("Add reliable channel");
    } else {
        streamingChannels.append(channel);
        DEBUG("Add streaming channel");
    }

    // Was this a new connection to the device?
    if (!hdp && !reliableChannels.empty()) {
        // New connection
        hdp = new HDP(this);
        phddevice = std::shared_ptr<VirtualPHD>(hdp);
        parent->listener.connectIndication(phddevice);
    }
}

void HealthDevice::channelDeleted(const QDBusObjectPath path) {
    DEBUG((QString("Channel disconnect: ") + path.path()).toStdString());

    // Note: In Antidote's BlueZ stack, whenever one channel to a device is
    //       closed remotely, all channels to that device will be closed and
    //       the device is disconnected. Perhaps this happens for a reason?
    //       For the time being, though, we only disconnects the device and
    //       close all channels when all **reliable** channels are closed
    //       remotely...

    // Close the channel
    HealthChannel *channel = channels.take(path.path());
    if (channel) {
        reliableChannels.removeAll(channel);
        streamingChannels.removeAll(channel);
        delete channel;
    }
    // If no more **reliable** channels, disconnect the device
    if (reliableChannels.empty()) disconnect(false);
}

void HealthDevice::disconnect(bool force) {
    // Connection to the device is lost
    streamingChannels.clear();
    reliableChannels.clear();
    foreach (HealthChannel *channel, channels.values()) {
        if (force) {
            // Call the DestroyChannel method
            QDBusPendingReply<> reply = healthInterface.asyncCall(QLatin1String("DestroyChannel"),QVariant::fromValue(channel->path));
            reply.waitForFinished();
            if (reply.isError()) {
                ERR(reply.error().message().toStdString());
            }
            if (reply.count() != 0) {
                ERR("Confused by return value when attempting to destroy channel");
            } else {
                DEBUG((QString("Channel destroyed: ") + channel->path.path()).toStdString());
            }
        }
        delete channel;
    }
    channels.clear();
    if (hdp) {
        hdp->kill();
        hdp = NULL;
        parent->listener.disconnectIndication(phddevice);
    }
}

void HealthDevice::messageReceived(std::vector<unsigned char> *message) {
    if (hdp) {
        parent->listener.messageReceived(phddevice,message);
    }
}


void HealthDevice::propertyChanged(const QString name, const QDBusVariant value) {
    DEBUG("Property Changed")
    if (name == "Address") {
        btaddress = value.variant().toString();
    } else if (name == "Name") {
        devname = value.variant().toString();
    } else if (name == "Alias") {
        alias = value.variant().toString();
    } else if (name == "Connected") {
        // If true->false transition, disconnect
        if (connected && !value.variant().toBool()) disconnect(false);
        connected = value.variant().toBool();
    } else if (name == "Paired") {
        paired = value.variant().toBool();
    } else {
        return; // The following debug message is irrelevant
    }
    DEBUG((QString("Device property changed: ")+devname+" with address: "+btaddress+", is paired: "+(paired?"yes":"no")+", is connected: "+(connected?"yes":"no")).toStdString());
}


HealthChannel::HealthChannel(QDBusObjectPath path, HealthDevice *device, BluezBluetoothModule *parent) :
    path(path),
    channelInterface("org.bluez",path.path(),"org.bluez.HealthChannel",parent->bus),
    device(device),
    parent(parent),
    fileDescriptor() {

    // Read the reliable property from the channel interface
    QDBusPendingReply<QVariantMap> properties = channelInterface.asyncCall(QLatin1String("GetProperties"));
    properties.waitForFinished();
    if (properties.isError()) {
        ERR(properties.error().message().toStdString());
        throw ConnectionException(properties.error().message().toStdString());
    }
    if (properties.count() != 1) {
        ERR("No device properties");
        throw ConnectionException("No device properties");
    }
    QVariantMap vm = properties.value();

    // Type should be either "Reliable" or "Streaming"
    reliable = (vm["Type"].toString() == "Reliable");

    // Open the communication line
    QDBusPendingReply<QDBusUnixFileDescriptor> fileDescr = channelInterface.asyncCall(QLatin1String("Acquire"));
    fileDescr.waitForFinished();
    if (fileDescr.isError()) {
        ERR(fileDescr.error().message().toStdString());
        throw ConnectionException(fileDescr.error().message().toStdString());
    }
    if (fileDescr.count() != 1 || !fileDescr.value().isValid()) {
        ERR("No file descriptor");
        throw ConnectionException("No file descriptor");
    }
    fileDescriptor = fileDescr.value();
    notifier = new QSocketNotifier(fileDescriptor.fileDescriptor(),QSocketNotifier::Read);
    notifier->setEnabled(true);
    connect(notifier,SIGNAL(activated(int)),this,SLOT(dataReceived(int)));
}

HealthChannel::~HealthChannel() {

    // Close communication
    notifier->setEnabled(false);
    disconnect(notifier,SIGNAL(activated(int)),this,SLOT(dataReceived(int)));
    delete notifier;
}

bool HealthChannel::isReliable() {
    return reliable;
}

void HealthChannel::sendMessage(std::vector<unsigned char> *message) {
    int sentCount = send(fileDescriptor.fileDescriptor(),&(*message)[0],message->size(),0);
    if (sentCount < 0) {
        throw ConnectionException(strerror(errno));
    } else if (sentCount != (int)message->size()) {
        throw ConnectionException("Partial message sent");
    }
}

// Max size, cf. 11073-20601 §8.3.2.c
const size_t INBUF_SIZE = 63*1024;
// Only one active call at any point in time, so we will use
// shared memory for the input buffer, at it is not used in-between
// dataReceived events.
unsigned char inbuf[INBUF_SIZE];

void HealthChannel::dataReceived(int fd) {
    int len = recv(fd, inbuf, INBUF_SIZE, 0);
    if (len < 0) {
        if (errno == EAGAIN) {
            // Just a race condition
            DEBUG("EAGAIN received from recv")
            return;
        } else if (errno == ETIMEDOUT) {
            // A timeout resulted in disconnection - handle as "channel closed"
            len = 0;
        } else {
            throw ConnectionException(strerror(errno));
        }
    }
    if (len == 0) {
        // Channel was closed by the peer. Now close it here as well
        device->channelDeleted(path);
        return;
    }
    std::vector<unsigned char> msg(inbuf,inbuf+len);
    device->messageReceived(&msg);
}




HDPWrapper::HDPWrapper(PAL::PersonalHealthDeviceListener &listener) :
    connector(new BluezBluetoothModule(listener)) {}

HDPWrapper::~HDPWrapper() {
    delete connector;
}

#endif

/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation file for the 4SDC stacks main thread class
 *
 * The class contained in this file has the responsibility of controlling the
 * 4SDC stack's main thread, initialising the stack, and running its message
 * loop.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/log.h"
#include "FSYS/msgreceiver.h"
#include "FSYS/msgsender.h"
#include "PAL/personalhealthdevice.h"
#include "4SDC/simpledemowrapper.h"
#include "4SDC/core.h"

/*** HACK START ****/
#include "Gateway/jsongateway.h"
#include "Gateway/4SDC2JSON/platformjsongateway.h"
/*** HACK END ****/

#include <csignal>
#include <map>

#include <QtCore/QCoreApplication>
#include <QObject>
#include <QString>
#include <QVector>
#include <QDateTime>

#include <iostream>
#include <sstream>

DECLARE_LOG_MODULE("InitialDemoLib")

using std::cout;
using std::endl;
using std::string;

using PAL::PersonalHealthDeviceHandler;
using PAL::VirtualPHD;





char hexNibble (unsigned char c) {
    if (c > 9) {
        return 'A' + c - 10;
    } else {
        return '0' + c;
    }
}

// Convert a byte to hex-string
string hexByte(unsigned char c) {
    char o[2];
    o[0] = hexNibble((c >> 4) & 0xf);
    o[1] = hexNibble(c & 0xf);
    return string(o,2);
}

// Convert an AbsoluteTime message section to string
string timestamp(std::shared_ptr<std::vector<uint8_t> > message, int start) {
    string retval;
    retval += hexByte((*message)[start]);
    retval += hexByte((*message)[start+1]);
    retval += "-";
    retval += hexByte((*message)[start+2]);
    retval += "-";
    retval += hexByte((*message)[start+3]);
    retval += " ";

    retval += hexByte((*message)[start+4]);
    retval += ":";
    retval += hexByte((*message)[start+5]);
    retval += ":";
    retval += hexByte((*message)[start+6]);
    retval += ".";
    retval += hexByte((*message)[start+7]);

    return retval;
}

// Convert a SFLOAT message type to string
string sfloat(u_int16_t val) {
    if (val >= 0x07FE && val <= 0x0802) {
        return "null"; // +/-Inf, NaN etc.
    }
    int16_t exponent = val & 0xF000;
    exponent = exponent / 4096;
    int16_t mantissa = val << 4;
    mantissa = mantissa / 16;
    string rval = QString::number(mantissa).toStdString();
    if (exponent) {
        rval += "e";
        rval += QString::number(exponent).toStdString();
    }
    return rval;
}

string ffloat(u_int32_t val) {
    if (val >= 0x007FFFFE && val <= 0x00800002) {
        return "null"; // +/-Inf, NaN etc.
    }
    int32_t exponent = val & 0xFF000000;
    exponent = exponent / 0x01000000;
    int32_t mantissa = val << 8;
    mantissa = mantissa / 256;
    string rval = QString::number(mantissa).toStdString();
    if (exponent) {
        rval += "e";
        rval += QString::number(exponent).toStdString();
    }
    return rval;
}

// Hex-print message on stdout
void printMsg(std::shared_ptr<std::vector<uint8_t> > message) {
    std::ostringstream oss;
    oss << message->size() <<" bytes:"<<endl;
    for (std::vector<unsigned char>::iterator it = message->begin(); it != message->end(); ) {
        for (int i = 0; i<16 && it != message->end(); i++,it++) {
            if (i != 0)  oss << ", ";
            oss << hexByte(*it);
        }
        oss << endl;
    }
    DEBUG(oss.str());
}

bool validTimestamp(std::shared_ptr<std::vector<uint8_t> > message, int start) {
    QDateTime deviceTime = QDateTime::fromString((timestamp(message,start)+"0").c_str(),"yyyy-MM-dd hh:mm:ss.zzz");
    qint64 timeDiff = deviceTime.secsTo(QDateTime::currentDateTime());
    if (timeDiff < -60 || timeDiff > 60) {
        // More than 60 seconds off
        return false;
    }
    return true;
}


struct ConnectedDevice;
class PHDListener : public FSYS::MsgSender<PHDListener>,
                    public FSYS::MsgReceiver<PHDListener, fsdc::Core::RegisterDataType>,
                    public FSYS::MsgReceiver<PHDListener, fsdc::Core::UnRegisterDataType>,
                    protected PersonalHealthDeviceHandler
{
public:
    void sendMessage(std::shared_ptr<VirtualPHD> device,
                     std::shared_ptr<std::vector<uint8_t> > message) noexcept;
    void receive(fsdc::Core::RegisterDataType regDataType);
    void receive(fsdc::Core::UnRegisterDataType regDataType);

protected:
    void connectIndication(
                         std::shared_ptr<VirtualPHD> device) noexcept;

    void disconnectIndication(
                         std::shared_ptr<VirtualPHD> device) noexcept;

    void disconnectIndication(std::shared_ptr<VirtualPHD> device,
                                errortype error) noexcept;

    void apduReceived(std::shared_ptr<VirtualPHD> device,
                       std::shared_ptr<std::vector<uint8_t> > message, bool) noexcept;
private:
    ConnectedDevice *lookupDevice(std::shared_ptr<VirtualPHD> device) noexcept;
    ConnectedDevice &lookupOrCreateDevice(std::shared_ptr<VirtualPHD> device) noexcept;
    void deleteDevice(std::shared_ptr<VirtualPHD> device) noexcept;

    std::map<FSYS::Handle,ConnectedDevice*> connectedDevices;
};

struct ConnectedDevice : public FSYS::MsgSender<ConnectedDevice>
{
    char state;
    string guid, manufacturer, model, serialno, oldMeasurementB, oldMeasurementP;
    u_int64_t guidval;
    PHDListener *parent;
    std::shared_ptr<VirtualPHD> phdevice; // FIXME: Should be initialized in constructor
    std::vector<string> measurementQueue;
    qint64 timeDiff;

    ConnectedDevice(PHDListener *parent, std::shared_ptr<VirtualPHD> phdevice) :
        state(0), manufacturer("null"), model("null"), serialno("null"),
        oldMeasurementB("null"), oldMeasurementP("null"), parent(parent), phdevice(phdevice) {}

    void wrapMDS(string);
    void sendNoMeasurement();
    void weAreConnected();
    void disassociate();

    /** HACK HACK HACK Function**/
    void broadcast(fsdc::Core::DataAvailable data)
    {
      if(Gateway::JSONGateway::getGateway())
      {
        Gateway::JSONGateway::getGateway()->receive(data);
      }
    }


};

void ConnectedDevice::wrapMDS(string measurement) {
  if (state == 2 && measurement != "null") {
    if (measurement.find("18948") != string::npos) {
        oldMeasurementB = measurement;
    } else {
        oldMeasurementP = measurement;
    }
    return;
  }
  string dataType = QString::number(phdevice->getDatatype()).toStdString();
  if (state == 3 && oldMeasurementB != "null") {
    fsdc::Core::DataAvailable data;

    data.dataType = phdevice->getDatatype();
    data.data =
            "{ \"type\" : "                + dataType +
            ", \"id\" : "                  + guid +
            ", \"manufacturer\" : "        + manufacturer +
            ", \"model\" : "               + model +
            ", \"serialNumber\" : "        + serialno +
            ", \"state\" : "               + "2" +
            ", \"measurement\" : "         + oldMeasurementB + " }";
    broadcast(data);
    DEBUG(data.data);
    oldMeasurementB = "null";
  }
  if (state == 3 && oldMeasurementP != "null") {
    fsdc::Core::DataAvailable data;

    data.dataType = phdevice->getDatatype();
    data.data =
              "{ \"type\" : "                + dataType +
              ", \"id\" : "                  + guid +
              ", \"manufacturer\" : "        + manufacturer +
              ", \"model\" : "               + model +
              ", \"serialNumber\" : "        + serialno +
              ", \"state\" : "               + "2" +
              ", \"measurement\" : "         + oldMeasurementP + " }";
    broadcast(data);
    DEBUG(data.data);
    oldMeasurementP = "null";
  }
  fsdc::Core::DataAvailable data;

  data.dataType = phdevice->getDatatype();
  data.data =
            "{ \"type\" : "                + dataType +
            ", \"id\" : "                  + guid +
            ", \"manufacturer\" : "        + manufacturer +
            ", \"model\" : "               + model +
            ", \"serialNumber\" : "        + serialno +
            ", \"state\" : "               + QString::number(state).toStdString() +
            ", \"measurement\" : "         + measurement + " }";
  broadcast(data);
  DEBUG(data.data);
}

void ConnectedDevice::sendNoMeasurement() {
    wrapMDS("null");
}

void ConnectedDevice::weAreConnected() {
    state = 2;
    sendNoMeasurement();
    if (!measurementQueue.empty()) {
        for (unsigned int i = 0; i < measurementQueue.size(); i++) {
            wrapMDS(measurementQueue[i]);
        }
        measurementQueue.clear();
    }
}

void ConnectedDevice::disassociate() {
    if (state == 2) { // Connected
        state = 3;
        sendNoMeasurement();
    }
    unsigned char x[] = {

        0xE4, 0x00,
        0x00, 0x02,
        0x00, 0x00

    };
    std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
    parent->sendMessage(phdevice,m);
}


//    std::map<FSYS::Handle,ConnectedDevice*> connectedDevices;

ConnectedDevice *PHDListener::lookupDevice(std::shared_ptr<VirtualPHD> device) noexcept {
    if (connectedDevices.count(*device) == 0) return nullptr;
    return connectedDevices[*device];
}
ConnectedDevice &PHDListener::lookupOrCreateDevice(std::shared_ptr<VirtualPHD> device) noexcept {
    if (connectedDevices.count(*device) != 0) {
        return *connectedDevices[*device];
    } else {
        ConnectedDevice *cd = new ConnectedDevice(this,device);
        connectedDevices.insert(std::pair<FSYS::Handle,ConnectedDevice*>(*device,cd));
        return *cd;
    }
}
void PHDListener::deleteDevice(std::shared_ptr<VirtualPHD> device) noexcept {
    if (connectedDevices.count(*device) == 0) return;
    ConnectedDevice *cd = connectedDevices[*device];
    connectedDevices.erase(*device);
    delete cd;
}


void PHDListener::sendMessage(std::shared_ptr<VirtualPHD> device,
                 std::shared_ptr<std::vector<uint8_t> > message) noexcept {
    sendApduPrimary(device,message);
}

void PHDListener::connectIndication(std::shared_ptr<VirtualPHD> device) noexcept {
    string dataType = QString::number(device->getDatatype()).toStdString();
    DEBUG("Connect Indication from VirtualPHD named '" + device->getDisplayName() +
          "', type: " + dataType + "; the PhysicalPHD is named '" +
          device->getPhysicalDevice().getDisplayName() + "'");
    PAL::PHDBluetoothDevice *bluetoothDevice =
        dynamic_cast<PAL::PHDBluetoothDevice *>(&device->getPhysicalDevice());
    if (bluetoothDevice) {
      DEBUG("This is a Bluetooth device with Bluetooth name: '" +
            bluetoothDevice->getBTName() + "' and address: " +
            bluetoothDevice->getBTAddress());
    }
    lookupOrCreateDevice(device); // Lookup and create new if no existing
}

void PHDListener::disconnectIndication(std::shared_ptr<VirtualPHD> device) noexcept {
    DEBUG("Disconnect indication");
    // Lookup and return if not found
    ConnectedDevice *d = lookupDevice(device);
    if (!d) return;

    if (d->state == 2) { // Connected
        INFO("DIM SIGNAL: BPM disconnecting");
        d->state = 3;
        d->sendNoMeasurement();
    }
    if (d->state == 3 || d->state == 1) {
        INFO("DIM SIGNAL: BPM disconnected");
        d->state = 0;
        d->sendNoMeasurement();
    }
    d = nullptr;
    deleteDevice(device);
}

void PHDListener::disconnectIndication(std::shared_ptr<VirtualPHD> device,
                                       errortype) noexcept {
    disconnectIndication(device);
}

void PHDListener::apduReceived(std::shared_ptr<VirtualPHD> device,
                                 std::shared_ptr<std::vector<uint8_t> > message,
                                 bool) noexcept {
    DEBUG("Message received");
    printMsg(message);
    // Lookup and return if not found
    ConnectedDevice *d = lookupDevice(device);
    if (!d) return;
    ConnectedDevice &connDev = *d;

    if ((*message)[0] == 0xE2 && (*message)[1] == 0x00 && connDev.state == 0) {
        DEBUG("Association Request");
        if (message->size() == 54 && (*message)[44] == 0x02 && (*message)[45] == 0xBC) {
            DEBUG("from BPM");
        } else if (message->size() == 54 && (*message)[44] == 0x05 && (*message)[45] == 0xDC) {
            DEBUG("from Scale");
        } else {
            return;
        }
        unsigned char x[] = {

            0xE3, 0x00,
            0x00, 0x2C,
            0x00, 0x00,
            0x50, 0x79,
            0x00, 0x26,
            0x80, 0x00, 0x00, 0x00,
            0x80, 0x00,
            0x80, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x80, 0x00, 0x00, 0x00,
            0x00, 0x08,
            0x81, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
            0x00, 0x00,
            0x00, 0x00,
            0x00, 0x00,
            0x00, 0x00, 0x00, 0x00

        };
        std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
        sendApduPrimary(device,m);
        unsigned char y[] = {

            0xE7, 0x00,
            0x00, 0x0E,
            0x00, 0x0C,
            0x00, 0x24,
            0x01, 0x03,
            0x00, 0x06,
            0x00, 0x00,
            0x00, 0x00,
            0x00, 0x00

        };
        std::shared_ptr<std::vector<uint8_t> > n(new std::vector<uint8_t>(y,y+sizeof(y)));
        sendApduPrimary(device,n);
        INFO("DIM SIGNAL: Device connected");
        connDev.guid = "\"";
        connDev.guidval = 0;
        connDev.measurementQueue.clear();
        for (int i=36; i<44; i++) {
            connDev.guidval <<= 8;
            connDev.guidval |= (*message)[i];
            connDev.guid += hexByte((*message)[i]);
        }
        connDev.guid += "\"";
        connDev.state = 1;
        connDev.sendNoMeasurement();
    } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && (message->size() == 172 || message->size() == 184) && connDev.state == 1) {
        // MDS attributes for BPM
        // Check guid value
        uint64_t gval = 0;
        for (int i=74; i<82; i++) {
            gval <<= 8;
            gval |= (*message)[i];
        }
        if (connDev.guidval != gval) {
            DEBUG("MDS Properties with GUID mismatch");
            return;
        }
        DEBUG("MDS properties");
        connDev.manufacturer = "\"";
        for (int i=42; i<54; i++) {
            connDev.manufacturer += ((*message)[i]);
        }
        connDev.manufacturer = QString(connDev.manufacturer.c_str()).trimmed().toStdString();
        connDev.manufacturer += "\"";
        connDev.model = "\"";
        for (int i=56; i<68; i++) {
            connDev.model += ((*message)[i]);
        }
        connDev.model = QString(connDev.model.c_str()).trimmed().toStdString();
        connDev.model += "\"";
        connDev.serialno = "\"";
        for (int i=114; i<126; i++) {
            connDev.serialno += ((*message)[i]);
        }
        connDev.serialno = QString(connDev.serialno.c_str()).trimmed().toStdString();
        connDev.serialno += "\"";

        connDev.phdevice = device;

        QDateTime deviceTime = QDateTime::fromString((timestamp(message,(message->size() == 172 ? 92 : 130))+"0").c_str(),"yyyy-MM-dd hh:mm:ss.zzz");
        cout << "Current device time: " << deviceTime.toString("yyyy-MM-dd hh:mm:ss").toStdString() << endl;
        connDev.timeDiff = deviceTime.secsTo(QDateTime::currentDateTime());
        if (connDev.timeDiff < -30 || connDev.timeDiff > 30) {
            // We will set the time
            unsigned char x[] = {

                0xE7, 0x00,
                0x00, 0x1A,
                0x00, 0x18,
                0x00, 0x77, // invoke id
                0x01, 0x07,
                0x00, 0x12,
                0x00, 0x00,
                0x0C, 0x17,
                0x00, 0x0C,
                0,0,0,0,  // Date
                0,0,0,0,  // Time
                0x00, 0x00, 0x00, 0x00

            };
            QString now = QDateTime::currentDateTime().toString("yyyyMMddhhmmsszzz");
            for (int i = 0; i < 8; i++) {
                x[i+18] = (now[2*i].toLatin1() - '0') << 4 | (now[2*i+1].toLatin1() - '0');
            }
            std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
            cout << "Difference is: " << connDev.timeDiff << ", Setting time:" << endl;
            printMsg(m);
            sendApduPrimary(device,m);

        } else {
            // Proceed to the connected state
            connDev.weAreConnected();
        }
    } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && (*message)[7] == 0x77 && message->size() == 18 && connDev.state == 1) {
        // Time set
        DEBUG("Time adjust completed");
        if (connDev.timeDiff < -300 || connDev.timeDiff > 300) {
            // If the time change exceeded 5 minutes, we will ignore any old measurements
            connDev.measurementQueue.clear();
        }
        // Proceed to the connected state
        connDev.weAreConnected();
    } else if ((*message)[0] == 0xE4 && (*message)[1] == 0x00 && message->size() == 6) {
        // Association release
        if (connDev.state == 2) { // Connected
            INFO("DIM SIGNAL: BPM disconnecting");
            connDev.state = 3;
            connDev.sendNoMeasurement();
        }
        unsigned char x[] = {

            0xE5, 0x00,
            0x00, 0x02,
            0x00, 0x00

        };
        std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
        sendApduPrimary(device,m);
        disconnect(device);
    } else if ((*message)[0] == 0xE6 && (*message)[1] == 0x00 && message->size() == 6) {
        // Abort connection
        if (connDev.state == 2) { // Connected
            INFO("DIM SIGNAL: BPM disconnecting");
            connDev.state = 3;
            connDev.sendNoMeasurement();
        }
        disconnect(device);
    } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && message->size() > 30
               && (*message)[18] == 0x0D && (*message)[19] == 0x1D
               && (u_int16_t)((*message)[28] * 256 + (*message)[29] + 30) == message->size()) {
        // Report from BPM
        u_int16_t invokeid = (*message)[6] * 256 + (*message)[7];
        u_int16_t reportCount = (*message)[26] * 256 + (*message)[27];
        int pointer = 30;
        for (int i = 0; i < reportCount; i++) {
            u_int16_t handle = (*message)[pointer + 0] * 256 + (*message)[pointer + 1];
            string json = "{ ";
            u_int16_t sys,dia,map,puls;
            switch (handle) {
            case 1:
                if (!validTimestamp(message,pointer+14)) {
                    DEBUG("Old measurement dropped");
                    pointer += 22;
                    continue;
                }
                sys = (*message)[pointer + 8] * 256 + (*message)[pointer + 9];
                dia = (*message)[pointer + 10] * 256 + (*message)[pointer + 11];
                map = (*message)[pointer + 12] * 256 + (*message)[pointer + 13];
                json += "\"type\" : 18948, \"datetime\" : \"" + timestamp(message, pointer+14);
                json += "\", \"unit\" : 3872, \"values\" : [ { ";
                json += "\"physio\" : 18949, \"value\" : " + sfloat(sys);
                json += " }, { \"physio\" : 18950, \"value\" : " + sfloat(dia);
                json += " }, { \"physio\" : 18951, \"value\" : " + sfloat(map);
                pointer += 22;
                break;
            case 2:
                if (!validTimestamp(message,pointer+6)) {
                    DEBUG("Old measurement dropped");
                    pointer += 14;
                    continue;
                }
                puls = (*message)[pointer + 4] * 256 + (*message)[pointer + 5];
                json += "\"type\" : 18474, \"datetime\" : \"" + timestamp(message, pointer+6);
                json += "\", \"unit\" : 2720, \"values\" : [ { ";
                json += "\"physio\" : null, \"value\" : " + sfloat(puls);
                pointer += 14;
                break;
            default:
                throw std::runtime_error("Parsing problem");
            }
            json += "} ] }";
            if (connDev.state == 2) {
                connDev.wrapMDS(json);
            } else {
                connDev.measurementQueue.push_back(json);
            }
        }
        unsigned char x[] = {

            0xE7, 0x00,
            0x00, 0x12,
            0x00, 0x10,
            0,0,
            0x02, 0x01,
            0x00, 0x0A,
            0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x0D, 0x1D,
            0x00, 0x00

        };
        x[6] = (invokeid >> 8) & 0xFF;
        x[7] = invokeid & 0xFF;
        std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
        sendApduPrimary(device,m);
    } else if ((*message)[0] == 0xE7 && (*message)[1] == 0x00 && message->size() > 30
               && (*message)[18] == 0x0D && (*message)[19] == 0x1E
               && (u_int16_t)((*message)[28] * 256 + (*message)[29] + 30) == message->size()) {
        // Report from weight scale
        u_int16_t invokeid = (*message)[6] * 256 + (*message)[7];
        u_int16_t reportCount = (*message)[26] * 256 + (*message)[27];
        int pointer = 30;
        for (int i = 0; i < reportCount; i++) {
            if (!validTimestamp(message,pointer+18)) {
                DEBUG("Old measurement dropped");
                pointer += 32;
                continue;
            }
            u_int32_t weight;
            u_int16_t unit;
            weight = (*message)[pointer + 10];
            weight <<= 8;
            weight |= (*message)[pointer + 11];
            weight <<= 8;
            weight |= (*message)[pointer + 12];
            weight <<= 8;
            weight |= (*message)[pointer + 13];
            unit = (*message)[pointer + 30];
            unit <<= 8;
            unit |= (*message)[pointer + 31];
            string json = "{ ";
            json += "\"type\" : 57664, \"datetime\" : \"" + timestamp(message, pointer+18);
            json += "\", \"unit\" : "  +QString::number(unit).toStdString() + ", \"values\" : [ { ";
            json += "\"physio\" : null, \"value\" : " + ffloat(weight);
            json += "} ] }";
            pointer += 32;
            if (connDev.state == 2) {
                connDev.wrapMDS(json);
            } else {
                connDev.measurementQueue.push_back(json);
            }
        }
        unsigned char x[] = {

            0xE7, 0x00,
            0x00, 0x12,
            0x00, 0x10,
            0,0,
            0x02, 0x01,
            0x00, 0x0A,
            0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x0D, 0x1E,
            0x00, 0x00

        };
        x[6] = (invokeid >> 8) & 0xFF;
        x[7] = invokeid & 0xFF;
        std::shared_ptr<std::vector<uint8_t> > m(new std::vector<uint8_t>(x,x+sizeof(x)));
        sendApduPrimary(device,m);
    }
}

void PHDListener::receive(fsdc::Core::RegisterDataType regDataType)
{
  registerDatatype(regDataType.dataType);
}

void PHDListener::receive(fsdc::Core::UnRegisterDataType unregDataType)
{
  unregisterDatatype(unregDataType.dataType);
}

class Wrapper : public PHDListener
{

public:
  Wrapper()
  {
    INFO("Wrapper constructing");
  }

  ~Wrapper()
  {
    INFO("Wrapper destruction");
  }
};


void SimpleDemoWrapper2::msgReady( void )
{
  pWrapper = new Wrapper();
}

void SimpleDemoWrapper2::msgGoingDown( void )
{
  delete pWrapper;
}

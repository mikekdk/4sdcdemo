/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the FSYS::Handle class
 *
 * /see FSYS::Handle
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "FSYS/handle.h"

#include <cassert>
#include <mutex>


int64_t FSYS::Handle::getNextHandle( void )
{
  static std::mutex mutex;
  static int64_t counter = 0;
  std::lock_guard<std::mutex> guard(mutex);

  assert(counter >= 0);

  return ++counter;
}

FSYS::Handle &FSYS::Handle::broadCastHandle( void )
{
  // Constructor will initialise handle to have unique value
  static Handle broadCastHandle;

  return broadCastHandle;
}

FSYS::Handle &FSYS::Handle::null( void )
{
  // Constructor will initialise handle to have unique value
  static Handle broadCastHandle;

  return broadCastHandle;
}


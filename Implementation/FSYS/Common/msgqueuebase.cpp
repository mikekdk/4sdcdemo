/*
 *   Copyright 2014-2015 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Contains implementation of the FSYS::MsgQueueBase class
 *
 * /see FSYS::MsgQueueBase
 *
 * @author <a href="mailto:4sdc@mikek.dk">Mike
 *         Kristoffersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2014-2015 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "msgqueuebase.h"

#include "FSYS/basemsg.h"
#include "FSYS/msgcallback.h"

#include <chrono>
#include <ctime>
#include <iostream>
#include <ratio>


std::forward_list<FSYS::MsgQueueBase*> FSYS::MsgQueueBase::listOfQueues;

FSYS::MsgQueueBase::MsgQueueBase()
{
  std::lock_guard<std::recursive_mutex> guard(getMutex());
  // Add us to the list of queues
  listOfQueues.push_front(this);
}

FSYS::MsgQueueBase::~MsgQueueBase()
{
  std::lock_guard<std::recursive_mutex> guard(getMutex());
  // Remove us from the list of queues
  listOfQueues.remove(this);
}

void FSYS::MsgQueueBase::removeListenerFromQueue(MsgCallBack *msgCallBack)
{
  std::lock_guard<std::recursive_mutex> guard(getMutex());
  MsgQueueBase &me = getThreadQueue();
  auto lastItr = me.localListenerList.before_begin();
  auto itr = me.localListenerList.begin();

  while(itr != me.localListenerList.end())
  {
    // Check if it is the right element
    if(itr->msgCallBack == msgCallBack)
    {
      // Delete the element before this one
      me.localListenerList.erase_after(lastItr);
      break;
    }

    // Goto next element
    lastItr = itr;
    itr++;
  }
}

void FSYS::MsgQueueBase::sendMsg(std::shared_ptr<BaseMsg> msg,  // Msg to send
                    const std::type_info &typeOfMsg,  // Type of msg to send                    
                    Handle queue,                  // Msg queue to target
                    Handle object,                 // Msg object to target
                    void *magicKey                    // MagicKey to target
                   )
{
  std::lock_guard<std::recursive_mutex> guard(getMutex());

  // Loop through the queues
  for(auto currentQueue: listOfQueues)
  {
    // Check if the queue is the right one
    if(*currentQueue == queue || getBroadCastHandle() == queue)
    {
      // Loop through the listeners on the queue
      for(auto currentListener: currentQueue->localListenerList)
      {
        // Check if the listener is one that we should talk to
//        std::cout << currentListener.typeOfMsg.name() << " compared to " << typeOfMsg.name() << std::endl;
        if(currentListener.typeOfMsg == typeOfMsg
        && (*(currentListener.msgCallBack) == object || getBroadCastHandle() == object)
        && (currentListener.magicKey == magicKey || nullptr == magicKey))
        {
          // Add the item to the queue
          currentQueue->localMsgQueue.emplace(msg, typeOfMsg, *(currentListener.msgCallBack), currentListener.magicKey);
          // Tell the queue that it has data (if it was blocking
          currentQueue->localQueueConditionVariable.notify_all();
        }
      }
    }
  }
}

void FSYS::MsgQueueBase::waitUntilQueueHasData( void )
{
  // Make sure we are the only one who are in the queue at this point in time
  getMutex().lock();

  // As long as the queue is empty
  while(getThreadQueue().localMsgQueue.empty())
  {
    // Wait for something to happen
    getThreadQueue().localQueueConditionVariable.wait(getMutex());
  }

  getMutex().unlock();
}

void FSYS::MsgQueueBase::emptyMsgQueue( int maxTimeMs )
{
  std::queue<MsgQueueItem> &queue = getThreadQueue().localMsgQueue;
  getTerminateLoopFlag() = false;


  // Get the current length of the queue
  getMutex().lock();
  int startQueueSize = queue.size();
  getMutex().unlock();
  int numberOfElementsProcessed = 0;

  // Get the now/start time
  std::chrono::high_resolution_clock::time_point startTime =
                                      std::chrono::high_resolution_clock::now();

  // Loop until we break out when the queue is empty, or the terminate flag is
  // set
  while(!getTerminateLoopFlag()
     && (maxTimeMs < 0
         || (maxTimeMs == 0 && startQueueSize < numberOfElementsProcessed)
         || (maxTimeMs >= std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::high_resolution_clock::now() - startTime).count())))
  {
    // Make sure we are the only one who are in the queue at this point in time
    getMutex().lock();

    // If the queue is empty, then return
    if(queue.empty())
    {
      getMutex().unlock();
      return;
    }

    // Fetch the first element
    MsgQueueItem msgQueueItem = queue.front();

    // Remove the first element from the queue
    queue.pop();

    // Let others access the queue
    getMutex().unlock();


    // Now check if we have the listener for the msg
    for(auto v: getThreadQueue().localListenerList)
    {
      // If we found a match
      if(msgQueueItem.typeOfMsg == v.typeOfMsg              // Check msg type
      && msgQueueItem.object == *v.msgCallBack              // Check obj handle
      && msgQueueItem.magicKey == v.magicKey)               // Check the pointer
      {
        v.msgCallBack->callBack(msgQueueItem.msg);

        // Break out of the for loop
        break;
      }
    }

    // Increase the count of elements processed so far
    numberOfElementsProcessed++;
  }
}

void FSYS::MsgQueueBase::breakEmptyMsgQueue( void )
{
  getTerminateLoopFlag() = true;
}

void FSYS::MsgQueueBase::addListenerToQueue(void* magicKey,
                                      const std::type_info &typeOfMsg,
                                      MsgCallBack *msgCallBack)
{
  std::lock_guard<std::recursive_mutex> guard(getMutex());
  getThreadQueue().localListenerList.emplace_front(magicKey,
                                                   typeOfMsg,
                                                   msgCallBack);
}

bool FSYS::MsgQueueBase::isEmpty( void )
{
  // Take lock on queue
  std::lock_guard<std::recursive_mutex> guard(getMutex());

  // Return if it is empty or not
  return getThreadQueue().localMsgQueue.empty();
}
